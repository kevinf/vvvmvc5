﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EblaAPI;
#if _USE_MYSQL
using MySql.Data.MySqlClient;
using EblaDataReader = MySql.Data.MySqlClient.MySqlDataReader;
using EblaConnection = MySql.Data.MySqlClient.MySqlConnection;
using EblaCommand = MySql.Data.MySqlClient.MySqlCommand;
using EblaParameter = MySql.Data.MySqlClient.MySqlParameter;
using EblaTransaction = MySql.Data.MySqlClient.MySqlTransaction;
#else
using System.Data.SQLite;
using EblaDataReader = System.Data.SQLite.SQLiteDataReader;
using EblaConnection = System.Data.SQLite.SQLiteConnection;
using EblaCommand = System.Data.SQLite.SQLiteCommand;
using EblaParameter = System.Data.SQLite.SQLiteParameter;
using EblaTransaction = System.Data.SQLite.SQLiteTransaction;
 

#endif


namespace EblaImpl
{
    public class AlignmentSet : OpenableEblaItem, IAlignmentSet
    {

        #region "IAlignmentSet"


        public bool Open(string CorpusName, string VersionName, string Username, string Password)
        {
            if (!base.Open(Username, Password))
                return false;

            OpenWithoutPrivilegeCheck(CorpusName, VersionName, false, null);

            return true;
        }

        internal void OpenWithoutPrivilegeCheck(string CorpusName, string VersionName, bool makeAdmin, EblaConnection cn)
        {
            if (makeAdmin)
                base.MakeAdmin();

            if (cn != null)
                SetConnection(cn);
            // Need the IDs of the two documents for later

            // First get corpus ID
            using (EblaCommand cmd = new EblaCommand("SELECT ID FROM corpora WHERE Name = @name", GetConnection()))
            {
                var nameParm = cmd.Parameters.AddWithValue("name", CorpusName);
                object o = cmd.ExecuteScalar();
                if (o == null)
                    throw new Exception("Corpus not found: " + CorpusName);

                _corpusid = System.Convert.ToInt32(o);


                string sql = "SELECT ID FROM documents WHERE CorpusID = " + _corpusid.ToString();
                cmd.CommandText = sql + " AND Name IS NULL";

                o = cmd.ExecuteScalar();
                if (o == null)
                    throw new Exception("Base text not found for corpus: " + CorpusName);

                _basetextid = System.Convert.ToInt32(o);
                cmd.CommandText = sql + " AND Name = @name";
                nameParm.Value = VersionName;
                o = cmd.ExecuteScalar();
                if (o == null)
                    throw new Exception("Version '" + VersionName + "' not found for corpus: " + CorpusName);

                _versionid = System.Convert.ToInt32(o);

                _corpusname = CorpusName;
                _versionname = VersionName;

            }
            CheckCanRead(_corpusname);
            _open = true;

           
        }

        internal int CreateAlignment(SegmentAlignment NewAlignment, EblaTransaction txn)
        {
            CheckOpen();
            CheckCanWrite(_corpusname);

            CheckAlignment(NewAlignment);


            using (EblaCommand cmd = new EblaCommand("INSERT INTO alignmentdetails (AlignmentType, AlignmentStatus, Notes) VALUES (@alignmenttype, @alignmentstatus, @notes)", GetConnection()))
            {
                cmd.Transaction = txn;

                cmd.Parameters.AddWithValue("alignmenttype", (int)(NewAlignment.AlignmentType));
                cmd.Parameters.AddWithValue("alignmentstatus", (int)(NewAlignment.AlignmentStatus));
                //cmd.Parameters.AddWithValue("basetextsegmentid", NewAlignment.SegmentIDInBaseText);
                //cmd.Parameters.AddWithValue("versionsegmentid", NewAlignment.SegmentIDInVersion);
                cmd.Parameters.AddWithValue("notes", NewAlignment.Notes);

                cmd.ExecuteNonQuery();

                int alignmentID = EblaHelpers.GetLastInsertID(cmd);

                cmd.CommandText = "INSERT INTO segmentalignments (AlignmentID, BaseTextSegmentID, VersionSegmentID) VALUES (" + alignmentID.ToString() + ", @basetextsegmentid, @versionsegmentid)";
                var basetextsegmentidparm = cmd.Parameters.AddWithValue("basetextsegmentid", 0);
                var versionsegmentidparm = cmd.Parameters.AddWithValue("versionsegmentid", 0);
                if (NewAlignment.SegmentIDsInBaseText.Length == 1)
                {
                    basetextsegmentidparm.Value = NewAlignment.SegmentIDsInBaseText[0];
                    foreach (int versionsegmentid in NewAlignment.SegmentIDsInVersion)
                    {
                        versionsegmentidparm.Value = versionsegmentid;
                        cmd.ExecuteNonQuery();
                    }
                }
                else
                {
                    versionsegmentidparm.Value = NewAlignment.SegmentIDsInVersion[0];
                    foreach (int basetextsegmentid in NewAlignment.SegmentIDsInBaseText)
                    {
                        basetextsegmentidparm.Value = basetextsegmentid;
                        cmd.ExecuteNonQuery();
                    }

                }


                return alignmentID;

            }
        }

        public int CreateAlignment(SegmentAlignment NewAlignment)
        {
            

            using (EblaTransaction txn = GetConnection().BeginTransaction())
            {
                var i = CreateAlignment(NewAlignment, txn);


                txn.Commit();

                return i;

                
            }

        }

        public SegmentAlignment FindAlignment(int segIdFilter, bool baseText)
        {
            CheckOpen();
            CheckCanRead(_corpusname);

            if (baseText && _preloaded)
                if (_preloadCache.ContainsKey(segIdFilter))
                    return _preloadCache[segIdFilter];
                else
                    return null; // TODO - review

            //string filter;
            //if (baseText)
            //    filter = "BaseTextSegmentID = " + segIdFilter.ToString();
            //else
            //    filter = "VersionSegmentID = " + segIdFilter.ToString();
            int? baseTextSegIdFilter = null;
            int? versionSegIdFilter = null;
            if (baseText)
                baseTextSegIdFilter = segIdFilter;
            else
                versionSegIdFilter = segIdFilter;

            SegmentAlignment[] a = ReadAlignments(null, null, null, _basetextid, _versionid, GetConnection(), baseTextSegIdFilter, versionSegIdFilter);

            if (a.Length == 0)
                return null;

            if (a.Length > 1)
            {
                // TODO - review
                //throw new Exception("Too many matching alignments!");
                return null;
            }

            return a[0];

        }

        public SegmentAlignment FindAlignment(int baseTextSegId, int versionSegId)
        {
            CheckOpen();
            CheckCanRead(_corpusname);

            //string filter = "BaseTextSegmentID = " + baseTextSegId.ToString() + " AND VersionSegmentID = " + versionSegId.ToString();

            SegmentAlignment[] a = ReadAlignments(null, null, null, _basetextid, _versionid, GetConnection(), baseTextSegId, versionSegId);

            if (a.Length == 0)
                return null;

            if (a.Length > 1)
                throw new Exception("Too many matching alignments!");

            return a[0];

        }

        public SegmentAlignment GetAlignment(int ID)
        {
            CheckOpen();
            CheckCanRead(_corpusname);

            string filter = "alignmentdetails.ID = " + ID.ToString();

            SegmentAlignment[] a = ReadAlignments(filter, null, null, _basetextid, _versionid, GetConnection(), null, null);

            if (a.Length == 0)
                return null;

            if (a.Length > 1)
                throw new Exception("Too many matching alignments!");

            return a[0];

        }

        public void UpdateAlignment(SegmentAlignment UpdatedAlignment)
        {
            CheckOpen();
            CheckCanWrite(_corpusname);

            CheckAlignment(UpdatedAlignment);

            SegmentAlignment current = GetAlignment(UpdatedAlignment.ID);
            if (current == null)
                throw new Exception("Alignment with ID " + UpdatedAlignment.ID.ToString() + " not found.");

            var baseSegIdsInvolved = new HashSet<int>(current.SegmentIDsInBaseText);
            var versionSegIdsInvolved = new HashSet<int>(current.SegmentIDsInVersion);

            foreach (var id in UpdatedAlignment.SegmentIDsInBaseText)
                if (!baseSegIdsInvolved.Contains(id))
                    baseSegIdsInvolved.Add(id);
            foreach (var id in UpdatedAlignment.SegmentIDsInVersion)
                if (!versionSegIdsInvolved.Contains(id))
                    versionSegIdsInvolved.Add(id);

            Dictionary<string, List<int>> existingPairs = new Dictionary<string, List<int>>();
            if (current.SegmentIDsInBaseText.Length == 1)
            {
                foreach (int id in current.SegmentIDsInVersion)
                {
                    existingPairs.Add(current.SegmentIDsInBaseText[0].ToString() + "-" + id.ToString(), new List<int>() { current.SegmentIDsInBaseText[0], id });
                }
            }
            else
            {
                foreach (int id in current.SegmentIDsInBaseText)
                {
                    existingPairs.Add(id.ToString() + "-" + current.SegmentIDsInVersion[0].ToString(), new List<int>() { id, current.SegmentIDsInVersion[0] });
                }

            }

            bool alignmentChanged = false;

            using (EblaTransaction txn = GetConnection().BeginTransaction())
            {
                using (EblaCommand cmd = new EblaCommand("UPDATE alignmentdetails SET AlignmentType = @alignmenttype, AlignmentStatus = @alignmentstatus, Notes = @notes WHERE ID = " + UpdatedAlignment.ID.ToString(), GetConnection()))
                {
                    cmd.Transaction = txn;
                    cmd.Parameters.AddWithValue("alignmenttype", (int)(UpdatedAlignment.AlignmentType));
                    cmd.Parameters.AddWithValue("alignmentstatus", (int)(UpdatedAlignment.AlignmentStatus));
                    //cmd.Parameters.AddWithValue("basetextsegmentid", UpdatedAlignment.SegmentIDInBaseText);
                    //cmd.Parameters.AddWithValue("versionsegmentid", UpdatedAlignment.SegmentIDInVersion);
                    cmd.Parameters.AddWithValue("notes", UpdatedAlignment.Notes);

                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "INSERT INTO segmentalignments (AlignmentID, BaseTextSegmentID, VersionSegmentID) VALUES (" + UpdatedAlignment.ID.ToString() + ", @basetextsegmentid, @versionsegmentid)";
                    var basetextsegmentidparm = cmd.Parameters.AddWithValue("basetextsegmentid", 0);
                    var versionsegmentidparm = cmd.Parameters.AddWithValue("versionsegmentid", 0);

                    if (UpdatedAlignment.SegmentIDsInBaseText.Length == 1)
                    {
                        basetextsegmentidparm.Value = UpdatedAlignment.SegmentIDsInBaseText[0];
                        foreach (int id in UpdatedAlignment.SegmentIDsInVersion)
                        {
                            string key = UpdatedAlignment.SegmentIDsInBaseText[0].ToString() + "-" + id.ToString();
                            if (existingPairs.ContainsKey(key))
                            {
                                existingPairs.Remove(key);
                            }
                            else
                            {
                                alignmentChanged = true;
                                versionsegmentidparm.Value = id;
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                    else
                    {
                        versionsegmentidparm.Value = UpdatedAlignment.SegmentIDsInVersion[0];
                        foreach (int id in UpdatedAlignment.SegmentIDsInBaseText)
                        {
                            string key = id.ToString() + "-" + UpdatedAlignment.SegmentIDsInVersion[0].ToString();
                            if (existingPairs.ContainsKey(key))
                            {
                                existingPairs.Remove(key);
                            }
                            else
                            {
                                alignmentChanged = true;
                                basetextsegmentidparm.Value = id;
                                cmd.ExecuteNonQuery();
                            }

                        }

                    }

                    // in principle, the AlignmentID filter isn't necessary ...
                    cmd.CommandText = "DELETE FROM segmentalignments WHERE AlignmentID = " + UpdatedAlignment.ID.ToString() + " AND BaseTextSegmentID = @basetextsegmentid AND VersionSegmentID = @versionsegmentid";
                    foreach (List<int> pair in existingPairs.Values)
                    {
                        alignmentChanged = true;
                        basetextsegmentidparm.Value = pair[0];
                        versionsegmentidparm.Value = pair[1];
                        cmd.ExecuteNonQuery();
                    }

                }


                txn.Commit();
            }

            // TODO - enclose in txn
            if (alignmentChanged)
            {
                //foreach (var id in baseSegIdsInvolved)
                //    Document.WipeVariationStatsForBaseTextSegment(id, false, ConfigProvider, GetConnection(), _corpusname);

                //foreach (var id in versionSegIdsInvolved)
                //    Document.WipeVariationStatsForVersionSegment(id, false, ConfigProvider, GetConnection(), _corpusname, _versionname);

            }


        }

        public void DeleteAlignment(int ID)
        {
            CheckOpen();
            CheckCanWrite(_corpusname);

            using (EblaCommand cmd = new EblaCommand("DELETE FROM alignmentdetails  WHERE ID = " + ID.ToString(), GetConnection()))
            {
                cmd.ExecuteNonQuery();

            }
        }

        public void DeleteAlignments(bool DraftOnly, SegmentAttribute[] BaseTextAttributeFilters, SegmentAttribute[] VersionAttributeFilters, int basetextStartPos, int baseTextLength, int versionStartPos, int versionLength)
        {
            // TODO - wipe variatoin

            CheckOpen();
            CheckCanWrite(_corpusname);

            List<EblaParameter> parms = new List<EblaParameter>();

            string filter = string.Empty;

            filter = "(" + Document.SegdefBoundsFilter(basetextStartPos, baseTextLength, true, true, "basesegmentdefinitions.") + ") ";
            filter += " AND (" + Document.SegdefBoundsFilter(versionStartPos, versionLength, true, true, "versionsegmentdefinitions.") + ") ";

            string sql = "DELETE FROM alignmentdetails WHERE ID IN (";


            sql += GetNestedSelect(filter, BaseTextAttributeFilters, VersionAttributeFilters, parms, _basetextid, _versionid);


            //sql += "SELECT AlignmentID ";
            //sql += "FROM segmentalignments  ";
            //sql += "INNER JOIN segmentdefinitions as basesegmentdefinitions ON segmentalignments.BaseTextSegmentID = basesegmentdefinitions.ID ";
            //sql += "INNER JOIN segmentdefinitions as versionsegmentdefinitions ON segmentalignments.VersionSegmentID = versionsegmentdefinitions.ID ";
            //sql += "WHERE basesegmentdefinitions.DocumentID = " + _basetextid.ToString() + " ";
            //sql += "AND versionsegmentdefinitions.DocumentID = " + _versionid.ToString() + " ";
            sql += ")";
            if (DraftOnly)
                sql += " AND AlignmentStatus = " + ((int)(AlignmentStatuses.draft)).ToString();

            using (EblaCommand cmd = new EblaCommand(sql, GetConnection()))
            {
                cmd.Parameters.AddRange(parms.ToArray());
                cmd.ExecuteNonQuery();

            }

        }

        public SegmentAlignment[] FindAlignments(SegmentAttribute[] BaseTextAttributeFilters, SegmentAttribute[] VersionAttributeFilters, int basetextStartPos, int baseTextLength, int versionStartPos, int versionLength)
        {
            CheckOpen();
            CheckCanRead(_corpusname);

            string filter = string.Empty;
            //int basetextEndPosition = basetextStartPos + baseTextLength;
            //int versionEndPosition = versionStartPos + versionLength;

            filter = "(" + Document.SegdefBoundsFilter(basetextStartPos, baseTextLength, true, true, "basesegmentdefinitions.") + ") ";
            filter += " AND (" + Document.SegdefBoundsFilter(versionStartPos, versionLength, true, true, "versionsegmentdefinitions.") + ") ";


            //if (basetextStartPos.HasValue)
            //    filter = AndFilter("basesegmentdefinitions.StartPosition >= " + basetextStartPos.Value.ToString());
            //if (versionStartPos.HasValue)
            //    filter = AndFilter("versionsegmentdefinitions.StartPosition >= " + versionStartPos.Value.ToString());

            return ReadAlignments(filter, BaseTextAttributeFilters, VersionAttributeFilters, _basetextid, _versionid, GetConnection(), null, null);
        }

        public void ConfirmAlignments(SegmentAttribute[] BaseTextAttributeFilters, SegmentAttribute[] VersionAttributeFilters, int basetextStartPos, int baseTextLength, int versionStartPos, int versionLength)
        {
            CheckOpen();
            CheckCanWrite(_corpusname);

            string filter = string.Empty;

            filter = "(" + Document.SegdefBoundsFilter(basetextStartPos, baseTextLength, true, true, "basesegmentdefinitions.") + ") ";
            filter += " AND (" + Document.SegdefBoundsFilter(versionStartPos, versionLength, true, true, "versionsegmentdefinitions.") + ") ";

            List<EblaParameter> parms = new List<EblaParameter>();

            // TODO - refactor to get rid of duplication versus ReadAlignments

            string sql = "UPDATE alignmentdetails SET AlignmentStatus = " + ((int)AlignmentStatuses.confirmed).ToString() + " WHERE AlignmentStatus = " + ((int)AlignmentStatuses.draft).ToString();
            sql += " AND ID IN (";

            sql += GetNestedSelect(filter, BaseTextAttributeFilters, VersionAttributeFilters, parms, _basetextid, _versionid);

            sql += ") ";

            using (EblaCommand cmd = new EblaCommand(sql, GetConnection()))
            {
                cmd.Parameters.AddRange(parms.ToArray());

                cmd.ExecuteNonQuery();
            }

        }

        public bool CreateMooreAlignments(TranslationDocument translationDocument, List<BestAlignment> results,
            out int progress)
        {
            var filteredAlignments = RemoveMultipleAligments(results);

            var isCreated = CreateMooreAlignments(filteredAlignments, out progress);

            return isCreated;
        }

        public void CreateMooreAlignments(TranslationDocument translationDocument, List<BestAlignment> results)
        {
            var filteredAlignments = RemoveMultipleAligments(results);

            CreateMooreAlignments(filteredAlignments);
        }

        private void CreateMooreAlignments(List<BestAlignment> alignments)
        {
            foreach (var bestAlignment in alignments)
            {
                for (var x = 0; x < bestAlignment.BaseSegmentList.Length + 1; x++)
                {
                    for (var y = 0; y < bestAlignment.VersionSegmentList.Length + 1; y++)
                    {
                        if (x >= bestAlignment.BaseSegmentList.Length ||
                            y >= bestAlignment.VersionSegmentList.Length)
                            continue;

                        CreateUpdateMooreAlignment(bestAlignment, x, y);
                    }
                }
            }
        }

        private bool CreateMooreAlignments(List<BestAlignment> results, out int progress)
        {
            // don't start a txn here until more work has been done on the functions it calls, 
            // since these will currently attempt to start transactions of their own, causing exceptions
            //using (var txn = GetConnection().BeginTransaction())
            {
                foreach (var bestAlignment in results)
                {
                    for (var x = 0; x < bestAlignment.BaseSegmentList.Length + 1; x++)
                    {
                        for (var y = 0; y < bestAlignment.VersionSegmentList.Length + 1; y++)
                        {
                            if (x >= bestAlignment.BaseSegmentList.Length ||
                                y >= bestAlignment.VersionSegmentList.Length)
                                continue;

                            CreateUpdateMooreAlignment(bestAlignment, x, y);
                        }
                    }
                }

                //txn.Commit();
            }
            progress = 100;

            return progress != 100;
        }

        
        private void CreateUpdateMooreAlignment(BestAlignment bestAlignment, int x, int y)
        {
            var isNewAlignment = true;

            var baseAlignment = FindAlignment(bestAlignment.BaseSegmentList[x].Id,
                true);
            var versionAlignment = FindAlignment(bestAlignment.VersionSegmentList[y].Id,
                false);

            if (baseAlignment != null)
            {
                isNewAlignment = false;
                if (UpdateBaseMooreAlignment(bestAlignment, y, baseAlignment)) return;
            }
            else
            {
                if (versionAlignment != null)
                {
                    isNewAlignment = false;
                    if (UpdateVersionMooreAlignment(bestAlignment, x, versionAlignment)) return;
                }
            }

            if (isNewAlignment)
                CreateMooreAlignment(bestAlignment, x, y);
        }

        private void CreateMooreAlignment(BestAlignment bestAlignment, int x, int y)
        {
            var newAlignment = new SegmentAlignment
            {
                AlignmentStatus = AlignmentStatuses.draft,
                SegmentIDsInBaseText = new int[] { bestAlignment.BaseSegmentList[x].Id },
                SegmentIDsInVersion = new int[] { bestAlignment.VersionSegmentList[y].Id },
                Scores = new[] { bestAlignment.Score }
            };

            CreateAlignment(newAlignment);
        }

        private bool UpdateVersionMooreAlignment(BestAlignment bestAlignment, int x, SegmentAlignment versionAlignment)
        {
            var existingId = versionAlignment.SegmentIDsInVersion.Where(
                t => t == bestAlignment.BaseSegmentList[x].Id).FirstOrDefault();
            if (existingId != 0) return true;

            var newList = versionAlignment.SegmentIDsInBaseText.ToList();
            newList.Add(bestAlignment.BaseSegmentList[x].Id);
            versionAlignment.SegmentIDsInBaseText = newList.ToArray();

            UpdateAlignment(versionAlignment);
            return false;
        }

        private bool UpdateBaseMooreAlignment(BestAlignment bestAlignment, int y, SegmentAlignment baseAlignment)
        {
            var baseId = baseAlignment.SegmentIDsInVersion.Where(
                t => t == bestAlignment.VersionSegmentList[y].Id).FirstOrDefault();
            if (baseId != 0) return true;

            var newList = baseAlignment.SegmentIDsInVersion.ToList();
            newList.Add(bestAlignment.VersionSegmentList[y].Id);
            baseAlignment.SegmentIDsInVersion = newList.ToArray();

            UpdateAlignment(baseAlignment);
            return false;
        }

        private List<BestAlignment> RemoveMultipleAligments(List<BestAlignment> results)
        {
            var newList = new List<BestAlignment>();
            foreach (var bestAlignment in results)
            {
                BestAlignment newBestAlignment = new BestAlignment();
                List<SegmentTokens> newBaseSegmentList = new List<SegmentTokens>();
                List<SegmentTokens> newVersionSegmentList = new List<SegmentTokens>();
                newBestAlignment.Score = bestAlignment.Score;

                for (var x = 0; x < bestAlignment.BaseSegmentList.Length + 1; x++)
                {
                    for (var y = 0; y < bestAlignment.VersionSegmentList.Length + 1; y++)
                    {
                        if (x >= bestAlignment.BaseSegmentList.Length || 
                            y >= bestAlignment.VersionSegmentList.Length)
                            continue;

                        var xSegment = bestAlignment.BaseSegmentList[x];
                        var ySegment = bestAlignment.VersionSegmentList[y];

                        if (!IsCreatingMultipleAligments(newList, xSegment.Id, ySegment.Id))
                        {
                            if (newBaseSegmentList.Find(e => e.Id == xSegment.Id) == null)
                                newBaseSegmentList.Add(xSegment);
                            if (newVersionSegmentList.Find(e => e.Id == ySegment.Id) == null)
                                newVersionSegmentList.Add(ySegment);
                        }
                    }
                }
                
                if (newBaseSegmentList.Count > 0 && newVersionSegmentList.Count > 0)
                {
                    newBestAlignment.BaseSegmentList = newBaseSegmentList.ToArray();
                    newBestAlignment.VersionSegmentList = newVersionSegmentList.ToArray();
                    newList.Add(newBestAlignment);
                }
            }
            return newList;
        }

        /// <summary>
        /// In order to prevent the previous alignments from being deleted in the EblaAPI.AlignmentSet.UpdateAlignment
        /// we check if the new alignment will cause a NxN relation.
        /// </summary>
        /// <returns>Is creating N x N?</returns>
        private bool IsCreatingMultipleAligments(List<BestAlignment> newList, int xId, int yId)
        {
            if (newList == null || newList?.Count == 0) return false;

            var bestAligmentsWithBaseX =
                newList.FirstOrDefault(p => p.BaseSegmentList.FirstOrDefault(b => b.Id == xId) != null);

            var bestAligmentsWithVersionY =
                newList.FirstOrDefault(p => p.VersionSegmentList.FirstOrDefault(v => v.Id == yId) != null);

            //First verification: When base and version ids already exist.
            if (bestAligmentsWithBaseX != null && bestAligmentsWithVersionY != null) return true;

            //Second verification: When there is an alignment with the base text id, but no version id.
            //As the new alignment will create a (1 X N) relation with the base id to new version plus the existing version alignments, 
            //none of the existing version id alignments could be in a (N X 1) relationship.
            if (bestAligmentsWithBaseX != null)
            {
                foreach (SegmentTokens version in bestAligmentsWithBaseX.VersionSegmentList)
                {
                    var bestAligmentsWithVersion =
                        newList.FindAll(p => p.VersionSegmentList.FirstOrDefault(v => v.Id == version.Id) != null);
                    var baseIds = new List<int>();
                    for (int i = 0; i < bestAligmentsWithVersion.Count; i++)
                    {
                        foreach (var j in bestAligmentsWithVersion[i].BaseSegmentList)
                        {
                            if (!baseIds.Contains(j.Id))
                                baseIds.Add(j.Id);
                        }
                    }
                    if (baseIds.Count > 1) return true;
                }
            }

            //Third verification: Opposite of second verification.
            if (bestAligmentsWithVersionY != null)
            {
                foreach (SegmentTokens baseToken in bestAligmentsWithVersionY.BaseSegmentList)
                {
                    var bestAligmentsWithBase =
                        newList.FindAll(p => p.BaseSegmentList.FirstOrDefault(v => v.Id == baseToken.Id) != null);
                    var versionIds = new List<int>();
                    for (int i = 0; i < bestAligmentsWithBase.Count; i++)
                    {
                        foreach (var j in bestAligmentsWithBase[i].VersionSegmentList)
                        {
                            if (!versionIds.Contains(j.Id))
                                versionIds.Add(j.Id);
                        }
                    }
                    if (versionIds.Count > 1) return true;
                }
            }

            return false;
        }

        private bool CreateMooreAlignments2(TranslationDocument translationDocument, List<BestAlignment> results, out int progress)
        {
            using (EblaTransaction txn = GetConnection().BeginTransaction())
            {
                var newAlignment = new SegmentAlignment { AlignmentStatus = AlignmentStatuses.draft };
                foreach (var bestAlignment in results)
                {
                    for (var x = 0; x < bestAlignment.BaseSegmentList.Length + 1; x++)
                    {
                        for (var y = 0; y < bestAlignment.VersionSegmentList.Length + 1; y++)
                        {
                            if (x >= bestAlignment.BaseSegmentList.Length || y >= bestAlignment.VersionSegmentList.Length)
                                continue;

                            newAlignment.SegmentIDsInBaseText = new int[] { bestAlignment.BaseSegmentList[x].Id };
                            newAlignment.SegmentIDsInVersion = new int[] { bestAlignment.VersionSegmentList[y].Id };
                            newAlignment.Scores = new float[] { bestAlignment.Score };


                            var existingAlignment = FindAlignment(bestAlignment.BaseSegmentList[x].Id,
                                bestAlignment.VersionSegmentList[y].Id);

                            if (existingAlignment == null)
                                CreateAlignment(newAlignment);
                        }
                    }
                }

                txn.Commit();
            }
            progress = 100;

            return progress != 100;
        }
        #endregion


        int _corpusid;
        int _basetextid;
        int _versionid;
        string _versionname;
        string _corpusname;

        bool _preloaded = false;
        Dictionary<int, SegmentAlignment> _preloadCache = new Dictionary<int, SegmentAlignment>();

        public AlignmentSet(System.Collections.Specialized.NameValueCollection configProvider)
            : base(configProvider)
        {

        }

        internal void Preload(SegmentAlignment[] alignments)
        {
            if (alignments == null)
                alignments = ReadAlignments(string.Empty, null, null, _basetextid, _versionid, GetConnection(), null, null);
            foreach (var a in alignments)
            {
                // This simplified preloaded representation only support 1-to-n alignments
                // and not n-to-1
                if (a.SegmentIDsInBaseText.Length == 1)
                {
                    // 1-to-n should usually be represented by a single alignment with 
                    // SegmentIDsInVersion longer than 1, but cater for representation
                    // as two separate alignments since that's how the Moore aligner 
                    // implementation is currently doing it.

                    // This check also deals with n-to-1 alignments, but only by 
                    // skipping all but the first part.
                    if (!_preloadCache.ContainsKey(a.SegmentIDsInBaseText[0]))
                        _preloadCache.Add(a.SegmentIDsInBaseText[0], a);
                }
            }
            _preloaded = true;
        }

        private void CheckAlignment(SegmentAlignment alignment)
        {
            if (alignment.SegmentIDsInBaseText == null || alignment.SegmentIDsInVersion == null || alignment.SegmentIDsInBaseText.Length == 0 || alignment.SegmentIDsInVersion.Length == 0)
                throw new Exception("At least one segment ID must be specified for the base text and the version document.");

            if ((alignment.SegmentIDsInBaseText.Length > 1) && (alignment.SegmentIDsInVersion.Length > 1))
                throw new Exception("Many-to-many alignments are not supported.");

#if DEBUG
            // Check segments refer to correct docs 
            string debugsql = "SELECT DocumentID FROM segmentdefinitions WHERE ID = @id";
            using (EblaCommand cmd = new EblaCommand(debugsql, GetConnection()))
            {
                EblaParameter idparm = cmd.Parameters.AddWithValue("id", 0);

                foreach (int id in alignment.SegmentIDsInBaseText)
                {
                    idparm.Value = id;

                    object o = cmd.ExecuteScalar();
                    if (o == null)
                        throw new Exception("Segment definition not found with ID: " + id.ToString());

                    if (System.Convert.ToInt32(o) != _basetextid)
                        throw new Exception("Segment definition with ID " + id.ToString() + " does not refer to base text");
                }

                foreach (int id in alignment.SegmentIDsInVersion)
                {
                    idparm.Value = id;

                    object o = cmd.ExecuteScalar();
                    if (o == null)
                        throw new Exception("Segment definition not found with ID: " + id.ToString());

                    if (System.Convert.ToInt32(o) != _versionid)
                        throw new Exception("Segment definition with ID " + id.ToString() + " does not refer to version");
                }


            }
#endif


        }

        static internal Dictionary<string, AlignmentSet> BatchPreload(System.Collections.Specialized.NameValueCollection ConfigProvider, EblaConnection cn,string corpusName, int corpusID, List<string> versions)
        {
            var docToIdMap = Corpus.GetDocToIdMap(cn, corpusID);

            string sql = "SELECT alignmentdetails.ID, AlignmentType, AlignmentStatus, BaseTextSegmentID, VersionSegmentID, alignmentdetails.Notes, DocumentID ";
            sql += "FROM alignmentdetails ";
            sql += "INNER JOIN segmentalignments ON alignmentdetails.ID = segmentalignments.AlignmentID ";
            sql += "INNER JOIN segmentdefinitions ON segmentalignments.VersionSegmentID = segmentdefinitions.ID ";
            sql += "WHERE segmentdefinitions.DocumentID IN (";
            var sb = new StringBuilder();
            var alignmentLists = new Dictionary<int, List<SegmentAlignment>>();
            var results = new Dictionary<string, AlignmentSet>();
            if (versions.Count == 0)
                return results;
            //var templist = new List<string>( versions);
            //templist.Add(string.Empty); // base text
            foreach (var v in versions)
            {
                if (sb.Length > 0)
                    sb.Append(", ");
                sb.Append(docToIdMap[v].ToString());
                alignmentLists.Add(docToIdMap[v], new List<SegmentAlignment>());
            }
            sql += sb.ToString();
            sql += ") ORDER BY alignmentdetails.ID ";

            using (EblaCommand cmd = new EblaCommand(sql, cn))
            {
                using (EblaDataReader dr = cmd.ExecuteReader())
                {
                    int alignmenttypeOrdinal = dr.GetOrdinal("AlignmentType");
                    int alignmentstatusOrdinal = dr.GetOrdinal("AlignmentStatus");
                    int basetextsegmentidOrdinal = dr.GetOrdinal("BaseTextSegmentID");
                    int versionsegmentidOrdinal = dr.GetOrdinal("VersionSegmentID");
                    int notesOrdinal = dr.GetOrdinal("Notes");
                    int idOrdinal = dr.GetOrdinal("ID");
                    int docidOrdinal = dr.GetOrdinal("DocumentID");
                    SegmentAlignment lastAlignment = null;

                    while (dr.Read())
                    {
                        int docid = dr.GetInt32(docidOrdinal);

                        int id = dr.GetInt32(idOrdinal);
                        int basetextsegid = dr.GetInt32(basetextsegmentidOrdinal);
                        int versionsegid = dr.GetInt32(versionsegmentidOrdinal);

                        if (lastAlignment != null)
                            if (id == lastAlignment.ID)
                            {
                                var baseTextSegIds = new HashSet<int>(lastAlignment.SegmentIDsInBaseText);
                                var versionSegIds = new HashSet<int>(lastAlignment.SegmentIDsInVersion);
                                if (!baseTextSegIds.Contains(basetextsegid))
                                    baseTextSegIds.Add(basetextsegid);

                                if (!versionSegIds.Contains(versionsegid))
                                    versionSegIds.Add(versionsegid);

                                if (baseTextSegIds.Count > 1 && versionSegIds.Count > 1)
                                    throw new Exception("Segment alignment data corrupted!");


                                var sortedBaseTextSegIds = new List<int>(baseTextSegIds);
                                sortedBaseTextSegIds.Sort();
                                var sortedVersionSegIds = new List<int>(versionSegIds);
                                sortedVersionSegIds.Sort();

                                lastAlignment.SegmentIDsInBaseText = sortedBaseTextSegIds.ToArray();
                                lastAlignment.SegmentIDsInVersion = sortedVersionSegIds.ToArray();

                                continue;
                            }

                        SegmentAlignment a = new SegmentAlignment();

                        a.ID = dr.GetInt32(idOrdinal);
                        a.AlignmentType = (AlignmentTypes)(dr.GetInt32(alignmenttypeOrdinal));
                        a.AlignmentStatus = (AlignmentStatuses)(dr.GetInt32(alignmentstatusOrdinal));
                        a.SegmentIDsInBaseText = new int[] { basetextsegid };
                        a.SegmentIDsInVersion = new int[] { versionsegid };
                        a.Notes = dr.IsDBNull(notesOrdinal) ? string.Empty : dr.GetString(notesOrdinal);

                        lastAlignment = a;
                        alignmentLists[docid].Add(a);

                    }
                }
            }

            
            foreach (var v in versions)
            {
                var alset = new AlignmentSet(ConfigProvider);

                alset.OpenWithoutPrivilegeCheck(corpusName, v, true, cn);
                alset.Preload(alignmentLists[docToIdMap[v]].ToArray());
                results.Add(v, alset);
            }
            return results;
        }


        internal static SegmentAlignment[] ReadAlignments(string filter, SegmentAttribute[] BaseTextAttributeFilters, SegmentAttribute[] VersionAttributeFilters, int baseTextID, int? versionID, EblaConnection cn, /*string attributeSought,*/ int? baseTextSegIdFilter, int? versionSegIdFilter)
        {
            List<EblaParameter> parms = new List<EblaParameter>();

            string sql = "SELECT alignmentdetails.ID, AlignmentType, AlignmentStatus, BaseTextSegmentID, VersionSegmentID, alignmentdetails.Notes ";

            //if (attributeSought != null)
            //    sql += ", DocumentID, Value";

            sql += "FROM alignmentdetails ";
            sql += "INNER JOIN segmentalignments ON alignmentdetails.ID = segmentalignments.AlignmentID ";
            //if (attributeSought != null)
            //    sql += "INNER JOIN segmentattributes ON segmentalignments.VersionSegmentID = segmentattributes.SegmentDefinitionID INNER JOIN segmentdefinitions ON segmentattributes.SegmentDefinitionID = segmentdefinitions.ID ";
            sql += " INNER JOIN segmentdefinitions as basesegmentdefinitions ON segmentalignments.BaseTextSegmentID = basesegmentdefinitions.ID ";
            sql += " INNER JOIN segmentdefinitions as versionsegmentdefinitions ON segmentalignments.VersionSegmentID = versionsegmentdefinitions.ID ";


            if (BaseTextAttributeFilters != null)
            {
                int count = 0;
                foreach (var f in BaseTextAttributeFilters)
                {
                    sql += "INNER JOIN segmentattributes as basetextsegmentattributes" + count.ToString() + " ON basesegmentdefinitions.ID = basetextsegmentattributes" + count.ToString() + ".SegmentDefinitionID ";
                    if (!string.IsNullOrEmpty(filter))
                        filter += " AND ";
                    string nameparmname = "baseattribname" + count.ToString();
                    string valparmname = "baseattribvalue" + count.ToString();
                    EblaParameter p = new EblaParameter(nameparmname, f.Name);
                    parms.Add(p);
                    p = new EblaParameter(valparmname, f.Value);
                    parms.Add(p);
                    filter += " (basetextsegmentattributes" + count.ToString() + ".Name = @" + nameparmname + " AND basetextsegmentattributes" + count.ToString() + ".Value = @" + valparmname + ") ";
                    count++;
                }
            }
            if (VersionAttributeFilters != null)
            {
                int count = 0;
                foreach (var f in VersionAttributeFilters)
                {
                    sql += "INNER JOIN segmentattributes as versionsegmentattributes" + count.ToString() + " ON versionsegmentdefinitions.ID = versionsegmentattributes" + count.ToString() + ".SegmentDefinitionID ";
                    if (!string.IsNullOrEmpty(filter))
                        filter += " AND ";
                    string nameparmname = "versionattribname" + count.ToString();
                    string valparmname = "versionattribvalue" + count.ToString();
                    EblaParameter p = new EblaParameter(nameparmname, f.Name);
                    parms.Add(p);
                    p = new EblaParameter(valparmname, f.Value);
                    parms.Add(p);
                    filter += " (versionsegmentattributes" + count.ToString() + ".Name = @" + nameparmname + " AND versionsegmentattributes" + count.ToString() + ".Value = @" + valparmname + ") ";
                    count++;
                }
            }

            string totalFilter = filter;
            if (!string.IsNullOrEmpty(totalFilter)) totalFilter += " AND ";
            totalFilter += " basesegmentdefinitions.DocumentID = " + baseTextID.ToString();
            totalFilter += " AND versionsegmentdefinitions.DocumentID = " + versionID.ToString();
            //if (!baseTextSegIdFilter.HasValue && !versionSegIdFilter.HasValue)
            //{
            //    sql += " WHERE alignmentdetails.ID IN (";

            //    sql += GetNestedSelect(filter, BaseTextAttributeFilters, VersionAttributeFilters, parms, baseTextID, versionID);

            //    sql += ")";
            //}
            //else
            {
                //sql += "INNER JOIN segmentdefinitions ON segmentalignments.BaseTextSegmentID = segmentdefinitions.ID ";
                //sql += " INNER JOIN segmentdefinitions as basesegmentdefinitions ON segmentalignments.BaseTextSegmentID = basesegmentdefinitions.ID ";
                //sql += " INNER JOIN segmentdefinitions as versionsegmentdefinitions ON segmentalignments.VersionSegmentID = versionsegmentdefinitions.ID ";

                
                //s += "basesegmentdefinitions.DocumentID = " + baseTextID.ToString();
                if (baseTextSegIdFilter.HasValue)
                {
                    if (!string.IsNullOrEmpty(totalFilter)) totalFilter += " AND ";
                    totalFilter += " BaseTextSegmentID = " + baseTextSegIdFilter.Value.ToString();
                }

                //s += " AND versionsegmentdefinitions.DocumentID = " + versionID.ToString();
                if (versionSegIdFilter.HasValue)
                {
                    if (!string.IsNullOrEmpty(totalFilter)) totalFilter += " AND ";
                    totalFilter += " VersionSegmentID = " + versionSegIdFilter.Value.ToString();
                }
                
            }


            sql += " WHERE " + totalFilter;

            sql += " ORDER BY alignmentdetails.ID ";
            
            List<SegmentAlignment> alignments = new List<SegmentAlignment>();
            using (EblaCommand cmd = new EblaCommand(sql, cn))
            {
                cmd.Parameters.AddRange(parms.ToArray());

                using (EblaDataReader dr = cmd.ExecuteReader())
                {

                    if (dr.Read())
                    {
                        int alignmenttypeOrdinal = dr.GetOrdinal("AlignmentType");
                        int alignmentstatusOrdinal = dr.GetOrdinal("AlignmentStatus");
                        int basetextsegmentidOrdinal = dr.GetOrdinal("BaseTextSegmentID");
                        int versionsegmentidOrdinal = dr.GetOrdinal("VersionSegmentID");
                        int notesOrdinal = dr.GetOrdinal("Notes"); 
                        int idOrdinal = dr.GetOrdinal("ID");

                        SegmentAlignment lastAlignment = null;

                        do
                        {
                            int id = dr.GetInt32(idOrdinal);
                            int basetextsegid = dr.GetInt32(basetextsegmentidOrdinal);
                            int versionsegid = dr.GetInt32(versionsegmentidOrdinal);

                            if (lastAlignment != null)
                                if (id == lastAlignment.ID)
                                {
                                    var baseTextSegIds = new HashSet<int>(lastAlignment.SegmentIDsInBaseText);
                                    var versionSegIds = new HashSet<int>(lastAlignment.SegmentIDsInVersion);
                                    if (!baseTextSegIds.Contains(basetextsegid))
                                        baseTextSegIds.Add(basetextsegid);

                                    if (!versionSegIds.Contains(versionsegid))
                                        versionSegIds.Add(versionsegid);

                                    if (baseTextSegIds.Count > 1 && versionSegIds.Count > 1)
                                        throw new Exception("Segment alignment data corrupted!");


                                    var sortedBaseTextSegIds = new List<int>(baseTextSegIds);
                                    sortedBaseTextSegIds.Sort();
                                    var sortedVersionSegIds = new List<int>(versionSegIds);
                                    sortedVersionSegIds.Sort();

                                    lastAlignment.SegmentIDsInBaseText = sortedBaseTextSegIds.ToArray();
                                    lastAlignment.SegmentIDsInVersion = sortedVersionSegIds.ToArray();

                                    continue;
                                }

                            SegmentAlignment a = new SegmentAlignment();

                            a.ID = dr.GetInt32(idOrdinal);
                            a.AlignmentType = (AlignmentTypes)(dr.GetInt32(alignmenttypeOrdinal));
                            a.AlignmentStatus = (AlignmentStatuses)(dr.GetInt32(alignmentstatusOrdinal));
                            a.SegmentIDsInBaseText = new int[] { basetextsegid };
                            a.SegmentIDsInVersion = new int[] {versionsegid} ;
                            a.Notes = dr.IsDBNull(notesOrdinal) ? string.Empty : dr.GetString(notesOrdinal);

                            lastAlignment = a;
                            alignments.Add(a);

                        } while (dr.Read());

                        
                    }
                }


                
            }

            return alignments.ToArray();

            
        }


        internal static string GetNestedSelect(string filter, SegmentAttribute[] BaseTextAttributeFilters, SegmentAttribute[] VersionAttributeFilters, List<EblaParameter> parms, int baseTextID, int? versionID)
        {
            string sql = "SELECT AlignmentID ";
            sql += "FROM segmentalignments  ";
            sql += "INNER JOIN segmentdefinitions as basesegmentdefinitions ON segmentalignments.BaseTextSegmentID = basesegmentdefinitions.ID ";
            sql += "INNER JOIN segmentdefinitions as versionsegmentdefinitions ON segmentalignments.VersionSegmentID = versionsegmentdefinitions.ID ";
            if (BaseTextAttributeFilters != null)
            {
                int count = 0;
                foreach (var f in BaseTextAttributeFilters)
                {
                    sql += "INNER JOIN segmentattributes as basetextsegmentattributes" + count.ToString() + " ON basesegmentdefinitions.ID = basetextsegmentattributes" + count.ToString() + ".SegmentDefinitionID ";
                    if (!string.IsNullOrEmpty(filter))
                        filter += " AND ";
                    string nameparmname = "baseattribname" + count.ToString();
                    string valparmname = "baseattribvalue" + count.ToString();
                    EblaParameter p = new EblaParameter(nameparmname, f.Name);
                    parms.Add(p);
                    p = new EblaParameter(valparmname, f.Value);
                    parms.Add(p);
                    filter += " (basetextsegmentattributes" + count.ToString() + ".Name = @" + nameparmname + " AND basetextsegmentattributes" + count.ToString() + ".Value = @" + valparmname + ") ";
                    count++;
                }
            }
            if (VersionAttributeFilters != null)
            {
                int count = 0;
                foreach (var f in VersionAttributeFilters)
                {
                    sql += "INNER JOIN segmentattributes as versionsegmentattributes" + count.ToString() + " ON versionsegmentdefinitions.ID = versionsegmentattributes" + count.ToString() + ".SegmentDefinitionID ";
                    if (!string.IsNullOrEmpty(filter))
                        filter += " AND ";
                    string nameparmname = "versionattribname" + count.ToString();
                    string valparmname = "versionattribvalue" + count.ToString();
                    EblaParameter p = new EblaParameter(nameparmname, f.Name);
                    parms.Add(p);
                    p = new EblaParameter(valparmname, f.Value);
                    parms.Add(p);
                    filter += " (versionsegmentattributes" + count.ToString() + ".Name = @" + nameparmname + " AND versionsegmentattributes" + count.ToString() + ".Value = @" + valparmname + ") ";
                    count++;
                }
            }


            sql += "WHERE basesegmentdefinitions.DocumentID = " + baseTextID.ToString() + " ";
            if (versionID.HasValue)
                sql += "AND versionsegmentdefinitions.DocumentID = " + versionID.Value.ToString() + " ";


            if (!string.IsNullOrEmpty(filter))
                sql += " AND " + filter + " ";


            return sql;
        }

   

    }
}
