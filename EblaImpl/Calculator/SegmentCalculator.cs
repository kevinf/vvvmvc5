﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.

    This class was based on the following articles and 2006-2015 Jarek Lipski - 
    Maligna (v3.0.1-SNAPSHOT) source code (https://github.com/loomchild/maligna):

    [1] Peter F. Brown, Jennifer C. Lai, Robert L. Mercer (1991), 
    Aligning sentences in parallel corpora, Proceedings of the 29th annual meeting on Association for Computational Linguistics.
    [2] Jassem, K., & Lipski, J. (2008). A new tool for the bilingual text aligning at the sentence level. 
    Intelligent Information Systems, 279-286.
    [3] Moore, R. C. (2002, October). Fast and accurate sentence alignment of bilingual corpora. 
    In Conference of the Association for Machine Translation in the Americas (pp. 135-144). Springer Berlin Heidelberg.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using EblaAPI;
using EblaImpl.Translation;

namespace EblaImpl.Calculator
{
    public class SegmentCalculator : ICalculator
    {
        internal float Mean { get; set; }
        internal TokensProbability BaseProbability { get; set; }
        internal TokensProbability VersionProbability { get; set; }

        public SegmentCalculator()
        {
            if (BaseProbability != null && VersionProbability != null)
                Mean = VersionProbability.Mean / BaseProbability.Mean;
        }

        public SegmentCalculator(SegmentTokens[] baseTokenses, SegmentTokens[] versionTokenses)
        {
            BaseProbability = CalculateProbabilities(baseTokenses, null);
            VersionProbability = CalculateProbabilities(versionTokenses, null);
            if (BaseProbability != null && VersionProbability != null)
                Mean = VersionProbability.Mean / BaseProbability.Mean;
        }

        public float CalculateScore(SegmentTokens[] baseTokenses,
            SegmentTokens[] versionTokenses
        )
        {
            float score;
            if (baseTokenses.Length == 0 && versionTokenses.Length == 0)
            {
                score = 0.0f;
            }
            else if (baseTokenses.Length == 0)
            {
                score = CalculateSegmentLikelihood(versionTokenses, VersionProbability);
            }
            else
            {
                score = CalculateSegmentLikelihood(baseTokenses, BaseProbability);
                if (versionTokenses.Length > 0)
                {
                    score += CalculatePoissonDistributionScore(baseTokenses,
                        versionTokenses);
                }
            }

            return score;
        }

        internal float CalculatePoissonDistributionScore(SegmentTokens[] segmentsBaseDocument,
            SegmentTokens[] segmentsVersionDocument)
        {
            var baseTotalTokens = CalculateSegmentLength(segmentsBaseDocument);
            var versionTotalTokens = CalculateSegmentLength(segmentsVersionDocument);
            var mean = baseTotalTokens * Mean;

            var score = EblaHelpers.PoissonDistribution(mean, versionTotalTokens);
            return score;
        }

        internal int CalculateSegmentLength(SegmentTokens[] segmentList)
        {
            return segmentList.Aggregate(0, (current, segment) => current + segment.TotalTokensValue);
        }

        internal static float CalculateSegmentLikelihood(SegmentTokens[] segmentList, TokensProbability probability)
        {
            return (float)segmentList.Sum(segment => -Math.Log(probability.TokensProbabilityValues[segment.TotalTokensValue]));
        }

        #region Calculates tokens' probabilities

        public TokensProbability CalculateProbabilities(SegmentTokens[] segmentList,
            Vocabulary vocabulary)
        {
            var tokensProb = GetProbabilities(segmentList);

            for (var i = 0; i < tokensProb.TokensProbabilityValues.Length; ++i)
            {
                var probability = tokensProb.TokensProbabilityValues[i] / tokensProb.AttributeValueOccurenceCount;
                tokensProb.TokensProbabilityValues[i] = probability;
            }
            tokensProb.Mean = (float) tokensProb.TotalAttributeValue / tokensProb.AttributeValueOccurenceCount;

            return tokensProb;
        }

        private static TokensProbability GetProbabilities(SegmentTokens[] segmentList)
        {
            var tokensProb = new TokensProbability();

            var maxAttributeValue = segmentList.Max(t => t.TotalTokensValue) + 1;

            tokensProb.TokensProbabilityValues = new float[maxAttributeValue];
            foreach (var segment in segmentList)
            {
                tokensProb.TokensProbabilityValues[segment.TotalTokensValue] += 1;
                tokensProb.AttributeValueOccurenceCount++;
                tokensProb.TotalAttributeValue += segment.TotalTokensValue;
            }

            return tokensProb;
        }

        #endregion Calculates tokens' probabilities
    }
}
