﻿using System.Collections.Generic;

namespace EblaImpl.Translation
{
    /// <summary>
    /// Compare VersionTranslationData objects and choosing the higher probability.
    /// </summary>
    public class VersionTranslationDataComparer : IComparer<VersionTranslationData>
    {
        /// <summary>
        /// Compare two VersionTranslationData objects.
        /// </summary>
        /// <param name="version1"></param>
        /// <param name="version2"></param>
        /// <returns></returns>
        public int Compare(VersionTranslationData version1, VersionTranslationData version2)
        {
            if (version1 == null || version2 == null) return 0;
            var difference = version1.Probability - version2.Probability;
            if (difference > 0) return 1;
            return difference < 0 ? -1 : 0;
        }
    }
}
