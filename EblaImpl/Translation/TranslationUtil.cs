﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.

    This class was based on the following articles and 2006-2015 Jarek Lipski - 
    Maligna (v3.0.1-SNAPSHOT) source code (https://github.com/loomchild/maligna):

    [1] Peter F. Brown, Jennifer C. Lai, Robert L. Mercer (1991), 
    Aligning sentences in parallel corpora, Proceedings of the 29th annual meeting on Association for Computational Linguistics.
    [2] Jassem, K., & Lipski, J. (2008). A new tool for the bilingual text aligning at the sentence level. 
    Intelligent Information Systems, 279-286.
    [3] Moore, R. C. (2002, October). Fast and accurate sentence alignment of bilingual corpora. 
    In Conference of the Association for Machine Translation in the Americas (pp. 135-144). Springer Berlin Heidelberg.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using EblaAPI;
using System.Text;

namespace EblaImpl.Translation
{
    public class TranslationUtil
    {
        private const int VocabularyNullWid = 0;
        private const string VocabularyOtherWord = "{OTHER}";

        public List<List<int>> CreateSegmentWidList(SegmentTokens[] segmentList,
            Dictionary<string, AlignmentVocabulary> vocabulary, bool addNullWid)
        {
            var widsList = new List<List<int>>();
            foreach (var segment in segmentList)
            {
                var wids = (from attribute in segment.Attributes
                    select vocabulary[attribute.Name]
                    into vocabularyToken
                    where vocabularyToken != null
                    select vocabularyToken.Id).ToList();
                if (addNullWid)
                {
                    wids.Add(VocabularyNullWid);
                }
                widsList.Add(wids);
            }
            return widsList;
        }
        public List<List<int>> CreateSegmentWidListWithoutRareWordsForTraining(SegmentTokens[] segmentList,
            Vocabulary vocabulary, bool addNullWid)
        {
            var widsList = new List<List<int>>();
            foreach (var segment in segmentList)
            {
                List<int> wids = new List<int>();
                var otherWordId = vocabulary.list.Find(t => t.Token == VocabularyOtherWord).Id;
                foreach (var attribute in segment.Attributes)
                {
                    var vocabularyToken = vocabulary.list.Find(t => t.Token == attribute.Name);
                    wids.Add(vocabularyToken?.Id ?? otherWordId);
                }
                if (addNullWid)
                {
                    wids.Add(VocabularyNullWid);
                }
                widsList.Add(wids);
            }
            return widsList;
        }

        public List<Int32?> CreateSegmentWidListWithoutRareWords(SegmentTokens[] segmentList,
            Vocabulary vocabulary, bool addNullWid)
        {
            var widsList = new List<Int32?>();
            foreach (var segment in segmentList)
            {
                foreach (var attribute in segment.Attributes)
                {
                    widsList.Add(vocabulary.TokenMap.ContainsKey(attribute.Name)? 
                        vocabulary.TokenMap[attribute.Name][0] : (Int32?)null);
                }
                if (addNullWid)
                {
                    widsList.Add(VocabularyNullWid);
                }
            }
            return widsList;
        }

        public SegmentTokens[] CreateSegmentWidListWithoutRareWords(SegmentTokens[] segmentList,
            Vocabulary vocabulary)
        {
            var widsList = segmentList;
            foreach (var segment in segmentList)
            {
                var sb = new StringBuilder(100);

                foreach (var attribute in segment.Attributes)
                {
                    if (sb.Length > 0) { sb.Append(" "); }
                    sb.Append(attribute.Name);
                    if (!vocabulary.TokenMap.ContainsKey(attribute.Name))
                    { 
                        var widListItem = widsList.FirstOrDefault(t => t.Id == segment.Id);

                        var firstOrDefault = widListItem?.Attributes.FirstOrDefault(
                            t => t.Name == attribute.Name && t.Value == attribute.Value);
                        if (firstOrDefault != null)
                            firstOrDefault.Name= VocabularyOtherWord;
                    }
                }

                segment.Segment = sb.ToString();

            }
            return widsList.ToArray();
        }

        public static List<Int32?> AddNullWidToList(List<Int32?> widList)
        {
            widList.Add(VocabularyNullWid);
            return widList;
        }

        public Dictionary<string, int> CreateVocabulary(string[] attributes)
        {
            var vocabulary = new Dictionary<string, int>();
            foreach (var attribute in attributes)
            {
                var tokensLines = attribute.Split(new string[] {Environment.NewLine},
                    StringSplitOptions.RemoveEmptyEntries);
                foreach (var line in tokensLines)
                {
                    var parts = line.Split(new char[] {'\t'}, StringSplitOptions.RemoveEmptyEntries);
                    if (parts.Length != 2)
                        throw new Exception("Bad tokens attribute for segment.");

                    var count = 0;
                    if (!int.TryParse(parts[1], out count))
                        throw new Exception("Bad tokens attribute for segment.");

                    if (vocabulary.ContainsKey(parts[0]))
                    {
                        count += vocabulary[parts[0]];
                        vocabulary[parts[0]] = count;
                    }
                    else
                    {
                        vocabulary.Add(parts[0], count);
                    }
                }
            }
            return vocabulary;
        }

        public List<AlignmentVocabulary> CreateAlignmentVocabulary(string[] attributes)
        {
            var vocabulary = new List<AlignmentVocabulary>();
            var vocabularyId = 1;
            foreach (var attribute in attributes)
            {
                var tokensLines = attribute.Split(new string[] {Environment.NewLine},
                    StringSplitOptions.RemoveEmptyEntries);
                foreach (var line in tokensLines)
                {
                    var parts = line.Split(new char[] {'\t'}, StringSplitOptions.RemoveEmptyEntries);
                    if (parts.Length != 2)
                        throw new Exception("Bad tokens attribute for segment.");

                    var count = 0;
                    if (!int.TryParse(parts[1], out count))
                        throw new Exception("Bad tokens attribute for segment.");

                    var existingToken = vocabulary.Find(t => t.Token == parts[0]);
                    if (existingToken == null)
                    {
                        var newToken = new AlignmentVocabulary
                        {
                            Id = vocabularyId,
                            Token = parts[0],
                            Count = count
                        };
                        vocabulary.Add(newToken);
                        vocabularyId++;
                    }
                    else
                    {
                        count += existingToken.Count;
                        vocabulary.Find(t => t.Token == parts[0]).Count = count;
                    }
                }
            }
            return vocabulary;
        }

        public Dictionary<string, AlignmentVocabulary> CreateAlignmentVocabulary(SegmentTokens[] segmentList)
        {
            var vocabulary = new Dictionary<string, AlignmentVocabulary>();
            var vocabularyId = 1;

            foreach (var segment in segmentList)
            {
                foreach (var attribute in segment.Attributes)
                {
                    AlignmentVocabulary existingToken = null;
                    vocabulary.TryGetValue(attribute.Name, out existingToken);

                    if (existingToken == null)
                    {
                        var newToken = new AlignmentVocabulary
                        {
                            Id = vocabularyId,
                            Token = attribute.Name,
                            Count = int.Parse(attribute.Value)
                        };
                        vocabulary.Add(attribute.Name, newToken);
                        vocabularyId++;
                    }
                    else
                    {
                        vocabulary[attribute.Name].Count = existingToken.Count +
                                                                                int.Parse(attribute.Value);
                    }
                }
            }

            return vocabulary;
        }

        

        public Vocabulary CreateAlignmentVocabularyWithoutRareWords(SegmentTokens[] segmentList,
            int minOccurrence)
        {
            var vocabulary = CreateAlignmentVocabulary(segmentList);

            int vocabularyId;
            var resultVocabulary = CreateAlignmentVocabularyWithoutRareWords(vocabulary, minOccurrence, out vocabularyId);

            var otherWordCount =
                vocabulary.Values.Where(existingToken => existingToken.Count < minOccurrence)
                    .Sum(token => token.Count);
            if (otherWordCount > 0)
            {
                resultVocabulary.Add(VocabularyOtherWord, new AlignmentVocabulary()
                {
                    Id = vocabularyId,
                    Count = otherWordCount,
                    Token = VocabularyOtherWord
                });
            }

            var v = new Vocabulary();
            v.list = new List<AlignmentVocabulary>();
            v.list.AddRange(resultVocabulary.Values);
            v.TokensList = (from r in resultVocabulary.Values
                            select r.Token).ToList();
            v.TokenMap = new Dictionary<string, int[]>();
            foreach (var item in resultVocabulary.Values)
            {
                v.TokenMap[item.Token] = new int[2] { item.Id, item.Count };
            }
            return v;
        }

        private static Dictionary<string, AlignmentVocabulary> CreateAlignmentVocabularyWithoutRareWords(Dictionary<string, AlignmentVocabulary> vocabulary, 
            int minOccurrence, out int vocabularyId)
        {
            var resultVocabulary = new Dictionary<string, AlignmentVocabulary>();
            vocabularyId = 1;
            foreach (var existingToken in vocabulary.Values)
            {
                if (existingToken.Count < minOccurrence) continue;
                var newToken = new AlignmentVocabulary
                {
                    Id = vocabularyId,
                    Token = existingToken.Token,
                    Count = existingToken.Count
                };
                resultVocabulary.Add(newToken.Token, newToken);
                vocabularyId++;
            }

            return resultVocabulary;
        }

        public List<AlignmentVocabulary> CreateAlignmentVocabularyWithoutRareWordsWithOtherWord(SegmentTokens[] segmentList,
            int minOccurrence)
        {
            var vocabulary = CreateAlignmentVocabulary(segmentList);

            var resultVocabulary = vocabulary.Values.Where(segment => segment.Count >= minOccurrence)
                .ToList();

            return resultVocabulary;
        }

        public SegmentTokens[] CreateTokensListWithoutRareWords(SegmentTokens[] segmentList,
            List<AlignmentVocabulary> vocabulary)
        {
            var widsList = segmentList;
            foreach (var segment in segmentList)
            {
                foreach (var attribute in segment.Attributes)
                {
                    var vocabularyToken = vocabulary.Find(t => t.Token == attribute.Name);
                    if (vocabularyToken == null)
                    {
                        var widListItem = widsList.FirstOrDefault(t => t.Id == segment.Id);

                        var firstOrDefault = widListItem?.Attributes.FirstOrDefault(
                            t => t.Name == attribute.Name && t.Value == attribute.Value);
                        if (firstOrDefault != null)
                            firstOrDefault.Name = VocabularyOtherWord;
                    }
                }

            }
            return widsList.ToArray();
        }

        private static List<AlignmentVocabulary> CreateTokensListWithoutRareWords(List<AlignmentVocabulary> vocabulary, 
            int minOccurrence, out int vocabularyId)
        {
            var resultVocabulary = new List<AlignmentVocabulary>();
            vocabularyId = 1;
            foreach (var existingToken in vocabulary)
            {
                if (existingToken.Count < minOccurrence) continue;
                var newToken = new AlignmentVocabulary
                {
                    Id = vocabularyId,
                    Token = existingToken.Token,
                    Count = existingToken.Count
                };
                resultVocabulary.Add(newToken);
                vocabularyId++;
            }

            return resultVocabulary;
        }


        public List<List<int>> CreateTokensListWithoutRareWords(SegmentTokens[] segmentList,
            List<AlignmentVocabulary> vocabulary, bool addNullWid)
        {
            var widsList = new List<List<int>>();
            foreach (var segment in segmentList)
            {
                var wids = new List<int>();
                var otherWordId = vocabulary.Find(t => t.Token == VocabularyOtherWord).Id;
                foreach (var attribute in segment.Attributes)
                {
                    var vocabularyToken = vocabulary.Find(t => t.Token == attribute.Name);
                    wids.Add(vocabularyToken?.Id ?? otherWordId);
                }
                if (addNullWid)
                {
                    wids.Add(VocabularyNullWid);
                }
                widsList.Add(wids);
            }
            return widsList;
        }

        public static SegmentTokens[] GetSubArray(SegmentTokens[] list, int fromIndex, int toIndex)
        {
            if ( !(fromIndex >= 0 && toIndex >= 0
                    && fromIndex < list.Length
                    && toIndex <= list.Length)
                    )
            {
                return new SegmentTokens[0];
            }

            var subArray = new SegmentTokens[toIndex - fromIndex];

            int index = 0;
            for (var i = fromIndex; i < toIndex; i++)
            {
                subArray[index++] = (list[i]);
            }
            
            return subArray;
        }

        public static List<SegmentTokens> GetSubList(SegmentTokens[] list, int fromIndex, int toIndex)
        {
            var subList = new List<SegmentTokens>();
            try
            {
                if (fromIndex >= 0 && toIndex >= 0
                    && fromIndex < list.Length
                    && toIndex <= list.Length)
                {
                    for (var i = fromIndex; i < toIndex; i++)
                    {
                        subList.Add(list[i]);
                    }
                }
            }
            catch (Exception)
            {
                return subList;
            }
            return subList;
        }

    }
}
