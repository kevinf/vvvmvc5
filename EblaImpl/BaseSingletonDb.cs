﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2017.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Configuration;

#if _USE_MYSQL
using EblaConnection = MySql.Data.MySqlClient.MySqlConnection;
#else
using System.Data.SQLite;
using EblaDataReader = System.Data.SQLite.SQLiteDataReader;
using EblaConnection = System.Data.SQLite.SQLiteConnection;
using EblaCommand = System.Data.SQLite.SQLiteCommand;
using EblaParameter = System.Data.SQLite.SQLiteParameter;
using EblaTransaction = System.Data.SQLite.SQLiteTransaction;
#endif

namespace EblaImpl
{
    class BaseSingletonDb
    {
        public EblaConnection conn = null;
        private string ConnnectionString
        {
            get
            {
                return ConfigurationManager.AppSettings["ConnStr"];
            }
        }

        public BaseSingletonDb()
        {
            conn = SingletonDb.Instance.GetConnection(ConnnectionString);
        }

        public BaseSingletonDb(string connectionString)
        {
            conn = SingletonDb.Instance.GetConnection(connectionString);
        }

    }
}
