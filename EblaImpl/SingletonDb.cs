﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2017.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Data;
using System.Configuration;

#if _USE_MYSQL
using MySql.Data.MySqlClient;
using EblaDataReader = MySql.Data.MySqlClient.MySqlDataReader;
using EblaConnection = MySql.Data.MySqlClient.MySqlConnection;
using EblaCommand = MySql.Data.MySqlClient.MySqlCommand;
#else
using System.Data.SQLite;
using EblaDataReader = System.Data.SQLite.SQLiteDataReader;
using EblaConnection = System.Data.SQLite.SQLiteConnection;
using EblaCommand = System.Data.SQLite.SQLiteCommand;
using EblaParameter = System.Data.SQLite.SQLiteParameter;
using EblaTransaction = System.Data.SQLite.SQLiteTransaction;
#endif

namespace EblaImpl
{
    public sealed class SingletonDb
    {
        private static volatile SingletonDb instance;
        private static object syncRoot = new Object();
        private static NameValueCollection _configProvider;

        static EblaConnection conn = null;

        private string ConnnectionString
        {
            get
            {
                return ConfigurationManager.AppSettings["ConnStr"];
            }
        }

        private SingletonDb()
        {

        }

        public static SingletonDb Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new SingletonDb();
                    }
                }

                return instance;
            }
        }

        public EblaConnection GetConnection()
        {
            if (conn == null)
            {
                conn = new EblaConnection(ConnnectionString);
                conn.Open();
            }
            else
            {
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
            }
            return conn;
        }

        public EblaConnection GetConnection(string connnectionString)
        {
            if (conn == null)
            {
                conn = new EblaConnection(connnectionString);
                conn.Open();
            }
            else
            {
                if (conn.State != ConnectionState.Open) conn.Open();
            }
            return conn;
        }

        public EblaConnection GetConnection(NameValueCollection configProvider)
        {
            if (conn == null)
            {
                conn = new EblaConnection(GetConnnectionString(configProvider));
                conn.Open();
            }
            else
            {
                if (conn.State != ConnectionState.Open) conn.Open();
            }
            return conn;
        }

        private string GetConnnectionString(NameValueCollection configProvider)
        {
            return configProvider["ConnStr"];
        }

        private void Close()
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
    }

}
