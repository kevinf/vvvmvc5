﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.

    This class was based on the following articles and 2006-2015 Jarek Lipski - 
    Maligna (v3.0.1-SNAPSHOT) source code (https://github.com/loomchild/maligna):

    [1] Peter F. Brown, Jennifer C. Lai, Robert L. Mercer (1991), 
    Aligning sentences in parallel corpora, Proceedings of the 29th annual meeting on Association for Computational Linguistics.
    [2] Jassem, K., & Lipski, J. (2008). A new tool for the bilingual text aligning at the sentence level. 
    Intelligent Information Systems, 279-286.
    [3] Moore, R. C. (2002, October). Fast and accurate sentence alignment of bilingual corpora. 
    In Conference of the Association for Machine Translation in the Americas (pp. 135-144). Springer Berlin Heidelberg.
 */

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using EblaAPI;
using EblaImpl.Algorithm;
using EblaImpl.Calculator;
using EblaImpl.Translation;

namespace EblaImpl.Aligner
{
    public class CustomTranslationAligner : OpenableEblaItem, ITranslationAligner
    {
        private const string TotalTokensAttributeName = ":totaltokens";
        private const string TokensAttributeName = ":tokens";
        private const string WidsAttributeName = ":wids";
        private const int DefaultMaxWordCount = 5000;
        private const int DefaultMinOccurrenceCount = 2;

        public CustomTranslationAligner(NameValueCollection configProvider) : base(configProvider)
        {
        }

        private void CheckConnectionStatus()
        {
            _open = true;
        }

        public TranslationDocument GetMooreTranslationDocument(string corpusName, string versionName, string username,
            string password)
        {
            CheckConnectionStatus();

            var corpus = new Corpus(ConfigProvider);
            corpus.OpenWithoutPrivilegeCheck(corpusName, true, GetConnection());

            var translationDocument = new TranslationDocument
            {
                CorpusName = corpusName,
                VersionName = versionName
            };

            var translationUtil = new TranslationUtil();

            translationDocument.BaseDocument = (Document) EblaFactory.GetDocument(ConfigProvider);
            if (!translationDocument.BaseDocument.OpenBaseText(corpusName, username, password))
                throw new Exception("You do not have permission to access the document.");

            var sentenceAlignment = new SegmentBasedAlignment(ConfigProvider);
            translationDocument.BaseDocumentSegmentsTotalTokens =
                sentenceAlignment.GetSegmentTokens(((Document) translationDocument.BaseDocument).ID(),
                    TotalTokensAttributeName);

            translationDocument.VersionDocument = (Document) EblaFactory.GetDocument(ConfigProvider);
            if (!translationDocument.VersionDocument.OpenVersion(corpusName, versionName, username, password))
                throw new Exception("You do not have permission to access the document.");

            translationDocument.VersionDocumentSegmentsTotalTokens =
                sentenceAlignment.GetSegmentTokens(((Document) translationDocument.VersionDocument).ID(),
                    TotalTokensAttributeName);

            translationDocument.BaseDocumentSegmentsTokens =
                sentenceAlignment.GetSegmentTokens(((Document) translationDocument.BaseDocument).ID(),
                    TokensAttributeName);

            translationDocument.BaseVocabulary =
                translationUtil.CreateAlignmentVocabularyWithoutRareWords(
                    translationDocument.BaseDocumentSegmentsTokens, DefaultMinOccurrenceCount);

            var baseDocumentWids = sentenceAlignment.GetSegmentTokens(((Document)translationDocument.BaseDocument).ID(),
                    WidsAttributeName);
            translationDocument.BaseDocumentSegmentsWids =
                translationUtil.CreateSegmentWidListWithoutRareWords(baseDocumentWids,
                    translationDocument.BaseVocabulary);

            translationDocument.VersionDocumentSegmentsTokens =
                sentenceAlignment.GetSegmentTokens(((Document) translationDocument.VersionDocument).ID(),
                    TokensAttributeName);

            translationDocument.VersionVocabulary =
                translationUtil.CreateAlignmentVocabularyWithoutRareWords(
                    translationDocument.VersionDocumentSegmentsTokens, DefaultMinOccurrenceCount);

            var versionDocumentWids = sentenceAlignment.GetSegmentTokens(((Document)translationDocument.VersionDocument).ID(),
                    WidsAttributeName);
            translationDocument.VersionDocumentSegmentsWids =
                translationUtil.CreateSegmentWidListWithoutRareWords(versionDocumentWids,
                    translationDocument.VersionVocabulary);

            return translationDocument;
        }
              
        public bool ApplyMooreAlignment(TranslationDocument translationDocument, string corpusName, string versionName, string userName, string password,
            out int progress)
        {
            CheckConnectionStatus();

            var unifiedAlignments = ApplyMooreAlignment(translationDocument);
            return CreateEblaAlignmentsFromMooreAligner(translationDocument, unifiedAlignments, corpusName, versionName, userName, password, out progress);
        }

        private bool CreateEblaAlignmentsFromMooreAligner(TranslationDocument translationDocument, List<BestAlignment> mooreAlignments, string corpusName, string versionName, string userName, string password, out int progress)
        {
            // Get a specific connection for this operation
            // (per-call connection held in Session state could easily now be null after Session timeout)
            using (var cn = EblaHelpers.GetConnection(ConfigProvider))
            {
                EblaCxnMgr.ThreadConnection = cn;
                var alignmentSet = new AlignmentSet(ConfigProvider);
                if (alignmentSet == null)
                    throw new Exception("It was not possible to instantiate auto-alignment.");

                alignmentSet.Open(corpusName, versionName, userName, password);

                var isMoreToDo = alignmentSet.CreateMooreAlignments(translationDocument, mooreAlignments,
                    out progress);

                return isMoreToDo;
            }
        }

        private List<BestAlignment> ApplyMooreAlignment(TranslationDocument translationDocument)
        {
            var segmentLengthAlignments = ApplyMooreAlignmentFirstStage(translationDocument);
            var bestAlignments = ApplyMooreBestAlignments(segmentLengthAlignments);
            var contentAlignments = ApplyMooreAlignmentSecondStage(translationDocument, bestAlignments);
            var unifiedAlignments = ApplyMooreAlignmentThirdStage(translationDocument, contentAlignments);

            return unifiedAlignments;
        }

        private List<BestAlignment> ApplyMooreBestAlignments(List<BestAlignment> segmentLengthAlignments)
        {
            CheckConnectionStatus();
            var bestScoreAligner = new BestScoreAligner();
            var bestAlignments = bestScoreAligner.SelectBestScoreAlignments(segmentLengthAlignments);

            return bestAlignments;
        }

        private List<BestAlignment> ApplyMooreAlignmentFirstStage(TranslationDocument translationDocument)
        {
            CheckConnectionStatus();

            var fwdbwd = new ForwardBackwardAlgorithm
            {
                Calculator =
                    new SegmentCalculator(translationDocument.BaseDocumentSegmentsTotalTokens,
                        translationDocument.VersionDocumentSegmentsTotalTokens)
            };

            var fwdbwdAlignments = fwdbwd.ForwardBackward(translationDocument.BaseDocumentSegmentsTotalTokens,
                translationDocument.VersionDocumentSegmentsTotalTokens);

            return fwdbwdAlignments;
        }

        private List<BestAlignment> ApplyMooreAlignmentSecondStage(TranslationDocument translationDocument,
            List<BestAlignment> bestAlignments)
        {
            CheckConnectionStatus();

            var ibmAligner = new IbmModelAligner();

            var translationData = ibmAligner.GetTranslationData(translationDocument, bestAlignments);

            var fwdbwd = new ForwardBackwardAlgorithm
            {
                Calculator =
                    new WordCalculator(translationDocument)
                    {
                        WordTranslationData = translationData
                    }
            };
            var bestTrainedAlignments = fwdbwd.ForwardBackward(translationDocument.BaseDocumentSegmentsWids,
                translationDocument.VersionDocumentSegmentsWids);

            return bestTrainedAlignments;
        }

        private List<BestAlignment> ApplyMooreAlignmentThirdStage(TranslationDocument translationDocument,
            List<BestAlignment> secondStageAlignments)
        {
            CheckConnectionStatus();

            var unify = new UnifyAligner();
            var unifiedAlignments = unify.UnifyAlignments(translationDocument, secondStageAlignments);

            return unifiedAlignments;
        }

        public bool ContinueAutoAlignment(TranslationDocument translationDocument, string corpusName, string versionName, string username,
            string password, out int progress)
        {
            CheckConnectionStatus();
            var isMoreToDo = ApplyMooreAlignment(translationDocument, corpusName, versionName, username, password, out progress);

            return isMoreToDo;
        }

        public List<BestAlignment> ApplyAutoAlignmentAndCreateEblaAlignment(string corpusName, string versionName, string userName, string password)
        {
            CheckConnectionStatus();

            var aligner = new CustomTranslationAligner(ConfigProvider);
            if (aligner == null)
                throw new Exception("It was not possible to instantiate aligner.");

            var translationDocument = aligner.GetMooreTranslationDocument(corpusName, versionName, userName, password);
            if (translationDocument == null)
                throw new Exception("It was not possible to create the translation document.");

            var alignments = ApplyMooreAlignment(translationDocument);

            var progress = 0;
            var isMoreToDo = CreateEblaAlignmentsFromMooreAligner(translationDocument, alignments, corpusName, versionName, userName,
                password, out progress);

            return alignments;
        }
    }
}