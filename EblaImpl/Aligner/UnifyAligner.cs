﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.

    This class was based on the following articles and 2006-2015 Jarek Lipski - 
    Maligna (v3.0.1-SNAPSHOT) source code (https://github.com/loomchild/maligna):

    [1] Peter F. Brown, Jennifer C. Lai, Robert L. Mercer (1991), 
    Aligning sentences in parallel corpora, Proceedings of the 29th annual meeting on Association for Computational Linguistics.
    [2] Jassem, K., & Lipski, J. (2008). A new tool for the bilingual text aligning at the sentence level. 
    Intelligent Information Systems, 279-286.
    [3] Moore, R. C. (2002, October). Fast and accurate sentence alignment of bilingual corpora. 
    In Conference of the Association for Machine Translation in the Americas (pp. 135-144). Springer Berlin Heidelberg.
 */

using System.Collections.Generic;
using System.Linq;
using EblaAPI;
using EblaImpl.Translation;

namespace EblaImpl.Aligner
{
    public class UnifyAligner
    {
        
        public List<BestAlignment> UnifyAlignments(TranslationDocument translationDocument,
            List<BestAlignment> referenceAlignments)
        {
            var newAlignments = new List<BestAlignment>();
            var baseList = new List<SegmentTokens>();
            var versionList = new List<SegmentTokens>();

            baseList.AddRange(translationDocument.BaseDocumentSegmentsTokens.ToList());
            versionList.AddRange(translationDocument.VersionDocumentSegmentsTokens.ToList());

            for (var i = 0; i < referenceAlignments.Count; i++)
            {
                var reference = referenceAlignments[i];
                var newSourceList = baseList.GetRange(i, reference.BaseSegmentList.Length);
                var newTargetList = versionList.GetRange(i, reference.VersionSegmentList.Length);

                var newAlignment = new BestAlignment
                {
                    BaseSegmentList = newSourceList.ToArray(),
                    VersionSegmentList = newTargetList.ToArray(),
                    Score = reference.Score
                };

                newAlignments.Add(newAlignment);
            }

            return newAlignments;
        }


        public List<BestAlignment> UnifyAlignments(List<BestAlignment> initialAlignments,
            List<BestAlignment> referenceAlignments)
        {
            var newAlignments = new List<BestAlignment>();
            var baseList = new List<SegmentTokens>();
            var versionList = new List<SegmentTokens>();

            foreach (var initial in initialAlignments)
            {
                baseList.AddRange(initial.BaseSegmentList.ToList());
                versionList.AddRange(initial.VersionSegmentList.ToList());
            }

            for (var i = 0; i < referenceAlignments.Count; i++)
            {
                var reference = referenceAlignments[i];
                var newSourceList = baseList.GetRange(i, reference.BaseSegmentList.Length);
                var newTargetList = versionList.GetRange(i, reference.VersionSegmentList.Length);

                var newAlignment = new BestAlignment
                {
                    BaseSegmentList = newSourceList.ToArray(),
                    VersionSegmentList = newTargetList.ToArray(),
                    Score = reference.Score
                };

                newAlignments.Add(newAlignment);

            }

            return newAlignments;
        }
    }


}
