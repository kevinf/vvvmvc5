﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EblaImpl.Extractor
{
    public enum DocumentFormat
    {
        pdf,
        html,
        htm,
        xhtml,
        xml,
        doc,
        docx,
        xlsx,
        rtf,
        odt,
        epub,
        txt,
        text
    }
}

