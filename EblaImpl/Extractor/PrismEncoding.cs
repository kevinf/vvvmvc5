﻿using System.ComponentModel;

namespace EblaImpl.Extractor
{
    public enum PrismEncoding
    {
        [Description("ASCIIEncoding")]
        ASCII,
        [Description ("UTF-8")]
        UTF8,
        [Description("UnicodeEncoding")]
        Unicode,
        [Description("UTF7Encoding")]
        UTF7,
        [Description("UTF-32")]
        UTF32
    }
}
