-- MySQL dump 10.13  Distrib 5.5.9, for Win32 (x86)
--
-- Host: localhost    Database: ebladb
-- ------------------------------------------------------
-- Server version	5.5.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `ebladb`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `ebladb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ebladb`;

--
-- Table structure for table `alignmentdetails`
--

DROP TABLE IF EXISTS `alignmentdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alignmentdetails` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AlignmentType` int(11) NOT NULL,
  `Notes` longtext CHARACTER SET latin1,
  `AlignmentStatus` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=51429 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alignmentdetails`
--

LOCK TABLES `alignmentdetails` WRITE;
/*!40000 ALTER TABLE `alignmentdetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `alignmentdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corpora`
--

DROP TABLE IF EXISTS `corpora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpora` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Description` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Name_UNIQUE` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corpora`
--

LOCK TABLES `corpora` WRITE;
/*!40000 ALTER TABLE `corpora` DISABLE KEYS */;
/*!40000 ALTER TABLE `corpora` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CorpusID` int(11) NOT NULL,
  `Content` longtext COLLATE utf8_unicode_ci,
  `Name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Length` int(11) DEFAULT NULL,
  `TOC` longtext COLLATE utf8_unicode_ci,
  `Description` longtext COLLATE utf8_unicode_ci,
  `ReferenceDate` int(11) DEFAULT NULL,
  `AuthorTranslator` longtext COLLATE utf8_unicode_ci,
  `CopyrightInfo` longtext COLLATE utf8_unicode_ci,
  `genre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LanguageCode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Information` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `DocumentsNameCorpusIDUQ` (`CorpusID`,`Name`),
  KEY `VersionsCorpusIDFK` (`CorpusID`),
  CONSTRAINT `VersionsCorpusIDFK` FOREIGN KEY (`CorpusID`) REFERENCES `corpora` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1567 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents`
--

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `predefinedsegmentattributes`
--

DROP TABLE IF EXISTS `predefinedsegmentattributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `predefinedsegmentattributes` (
  `CorpusID` int(11) NOT NULL,
  `Name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `AttributeType` int(11) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ShowInTOC` bit(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SegAttribNamesCorpusID` (`CorpusID`),
  CONSTRAINT `SegAttribNamesCorpusID` FOREIGN KEY (`CorpusID`) REFERENCES `corpora` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `predefinedsegmentattributes`
--

LOCK TABLES `predefinedsegmentattributes` WRITE;
/*!40000 ALTER TABLE `predefinedsegmentattributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `predefinedsegmentattributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `predefinedsegmentattributevalues`
--

DROP TABLE IF EXISTS `predefinedsegmentattributevalues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `predefinedsegmentattributevalues` (
  `PredefinedSegmentAttributeID` int(11) NOT NULL,
  `Value` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ApplyColouring` bit(1) DEFAULT NULL,
  `ColourCode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`PredefinedSegmentAttributeID`,`Value`),
  KEY `SegValueSegName` (`PredefinedSegmentAttributeID`),
  CONSTRAINT `SegValueSegName` FOREIGN KEY (`PredefinedSegmentAttributeID`) REFERENCES `predefinedsegmentattributes` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `predefinedsegmentattributevalues`
--

LOCK TABLES `predefinedsegmentattributevalues` WRITE;
/*!40000 ALTER TABLE `predefinedsegmentattributevalues` DISABLE KEYS */;
/*!40000 ALTER TABLE `predefinedsegmentattributevalues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `segmentalignments`
--

DROP TABLE IF EXISTS `segmentalignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `segmentalignments` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BaseTextSegmentID` int(11) NOT NULL,
  `VersionSegmentID` int(11) NOT NULL,
  `AlignmentID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SegAlignUnique` (`BaseTextSegmentID`,`VersionSegmentID`),
  KEY `SegmentAlignmentsVersionSegmentIDFK` (`VersionSegmentID`),
  KEY `SegmentAlignmentsBaseTextSegmentIDFK` (`BaseTextSegmentID`),
  KEY `SegAlignmentAlignID` (`AlignmentID`),
  CONSTRAINT `SegAlignmentAlignID` FOREIGN KEY (`AlignmentID`) REFERENCES `alignmentdetails` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SegmentAlignmentsBaseTextSegmentIDFK` FOREIGN KEY (`BaseTextSegmentID`) REFERENCES `segmentdefinitions` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SegmentAlignmentsVersionSegmentIDFK` FOREIGN KEY (`VersionSegmentID`) REFERENCES `segmentdefinitions` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51547 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `segmentalignments`
--

LOCK TABLES `segmentalignments` WRITE;
/*!40000 ALTER TABLE `segmentalignments` DISABLE KEYS */;
/*!40000 ALTER TABLE `segmentalignments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `segmentattributes`
--

DROP TABLE IF EXISTS `segmentattributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `segmentattributes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SegmentDefinitionID` int(11) NOT NULL,
  `Name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `Value` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `SegmentAttributeSegmentDefinitionIDFK` (`SegmentDefinitionID`),
  CONSTRAINT `SegmentAttributeSegmentDefinitionIDFK` FOREIGN KEY (`SegmentDefinitionID`) REFERENCES `segmentdefinitions` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=281118 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `segmentattributes`
--

LOCK TABLES `segmentattributes` WRITE;
/*!40000 ALTER TABLE `segmentattributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `segmentattributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `segmentdefinitions`
--

DROP TABLE IF EXISTS `segmentdefinitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `segmentdefinitions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `StartPosition` int(11) NOT NULL,
  `Length` int(11) NOT NULL,
  `DocumentID` int(11) NOT NULL,
  `ContentBehaviour` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SegmentDefinitionDocumentIDFK` (`DocumentID`),
  CONSTRAINT `SegmentDefinitionDocumentIDFK` FOREIGN KEY (`DocumentID`) REFERENCES `documents` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=90644 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `segmentdefinitions`
--

LOCK TABLES `segmentdefinitions` WRITE;
/*!40000 ALTER TABLE `segmentdefinitions` DISABLE KEYS */;
/*!40000 ALTER TABLE `segmentdefinitions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `segmentlongattributes`
--

DROP TABLE IF EXISTS `segmentlongattributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `segmentlongattributes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SegmentDefinitionID` int(11) DEFAULT NULL,
  `Name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `Value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `SegmentLongAttributeSegmentDefinitionIDFK` (`SegmentDefinitionID`),
  CONSTRAINT `SegmentLongAttributeSegmentDefinitionIDFK` FOREIGN KEY (`SegmentDefinitionID`) REFERENCES `segmentdefinitions` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=173360 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `segmentlongattributes`
--

LOCK TABLES `segmentlongattributes` WRITE;
/*!40000 ALTER TABLE `segmentlongattributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `segmentlongattributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userrights`
--

DROP TABLE IF EXISTS `userrights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userrights` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `CorpusID` int(11) NOT NULL,
  `CanRead` bit(1) NOT NULL,
  `CanWrite` bit(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `userrightsuserfk` (`UserID`),
  KEY `userrightscorpusfk` (`CorpusID`),
  CONSTRAINT `userrightscorpusfk` FOREIGN KEY (`CorpusID`) REFERENCES `corpora` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `userrightsuserfk` FOREIGN KEY (`UserID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userrights`
--

LOCK TABLES `userrights` WRITE;
/*!40000 ALTER TABLE `userrights` DISABLE KEYS */;
/*!40000 ALTER TABLE `userrights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(45) NOT NULL,
  `Password` varchar(45) NOT NULL,
  `IsAdmin` bit(1) DEFAULT NULL,
  `UserEmail` varchar(100) DEFAULT NULL,
  `Salt` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','3182AB4E777DDC0D759E25D30468984EC41B3401','','admin@admin.com','X5XlCQ==');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-20 22:10:13
