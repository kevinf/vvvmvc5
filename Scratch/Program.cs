﻿#define _VVV_PRESERVEWHITESPACE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using System.Diagnostics;
using System.Net;
using System.IO;
using System.Xml;
using EblaImpl;
using EblaAPI;
using System.Configuration;

namespace Scratch
{
    internal class ScratchEblaSupport
    {
        static object _cxn;
        static object _rqdata;

        internal static object GetConnection()
        {
            return _cxn;
        }

        internal static void SetConnection(object conn)
        {
            _cxn = conn;
        }

        internal static object GetRequestData()
        {
            return _rqdata;
        }

        internal static void SetRequestData(object data)
        {
            _rqdata = data;
        }

    }

    internal class ScratchHelpers
    {
        //static string _uname = "KevinFlanagan";
        //static string _pwd = "Daniel05";
        static string _uname = "admin";
        static string _pwd = "VVVadmin1";

        internal static ICorpus GetCorpus(string name)
        {
            // TODO - consider caching in Session
            ICorpus corpus = EblaFactory.GetCorpus(ConfigurationManager.AppSettings);

            if (!corpus.Open(name, _uname, _pwd))
                throw new Exception("You do not have permission to access the corpus.");

            return corpus;
        }

        internal static ICorpusStore GetCorpusStore()
        {
            ICorpusStore store = EblaFactory.GetCorpusStore(ConfigurationManager.AppSettings);

            store.Open(_uname, _pwd);
            return store;
        }


        internal static IDocument GetBaseText(string corpusName)
        {
            // TODO - consider caching in Session
            IDocument doc = EblaFactory.GetDocument(ConfigurationManager.AppSettings);

            if (!doc.OpenBaseText(corpusName, _uname, _pwd))
                throw new Exception("You do not have permission to access the document.");

            return doc;
        }

        internal static IDocument GetVersion(string corpusName, string versionName)
        {
            // TODO - consider caching in Session
            IDocument doc = EblaFactory.GetDocument(ConfigurationManager.AppSettings);

            if (!doc.OpenVersion(corpusName, versionName, _uname, _pwd))
                throw new Exception("You do not have permission to access the document.");

            return doc;
        }

        internal static IAlignmentSet GetAlignmentSet(string corpusName, string versionName)
        {
            // TODO - consider caching in Session
            IAlignmentSet set = EblaFactory.GetAlignmentSet(ConfigurationManager.AppSettings);

            if (!set.Open(corpusName, versionName, _uname, _pwd))
                throw new Exception("You do not have permission to access the alignment set.");

            return set;
        }


        internal static IDocument GetDocument(string corpusName, string nameIfVersion)
        {
            if (string.IsNullOrEmpty(nameIfVersion))
                return GetBaseText(corpusName);

            return GetVersion(corpusName, nameIfVersion);
        }

        internal static System.Text.Encoding PrismEncodingToTextEncoding(PrismEncoding encoding)
        {
            switch (encoding)
            {
                case PrismEncoding.ASCII:
                    return System.Text.Encoding.ASCII;
                case PrismEncoding.UTF8:
                    return System.Text.Encoding.UTF8;
                case PrismEncoding.UTF7:
                    return System.Text.Encoding.UTF7;
                case PrismEncoding.UTF32:
                    return System.Text.Encoding.UTF32;
                case PrismEncoding.Unicode:
                    return System.Text.Encoding.Unicode;
                case PrismEncoding.BigEndianUnicode:
                    return System.Text.Encoding.BigEndianUnicode;

            }

            throw new Exception("Unrecognised PrismEncoding: " + encoding.ToString());
        }

        internal static XmlDocument XmlDocFromContent(string content)
        {
            string xml = @"<?xml version=""1.0"" ?><body>";

            xml += content;

            xml += "</body>";

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);
            return xmlDoc;
        }
    }

    public enum PrismEncoding
    {
        ASCII,
        UTF8,
        Unicode,
        UTF7,
        UTF32,
        BigEndianUnicode
    }


    class Program
    {

        static void TabDelimToCorpus(string filename, string corpName)
        {
            var store = ScratchHelpers.GetCorpusStore();

            //foreach (var corpname in store.GetCorpusList())
            //    store.DeleteCorpus(corpname);


            if (store.GetCorpusList().FirstOrDefault(x => x == corpName) != null)
                store.DeleteCorpus(corpName);

            store.CreateCorpus(corpName, string.Empty, "en");

            var corp = ScratchHelpers.GetCorpus(corpName);

            var workingpath = Path.GetDirectoryName(filename);
            workingpath = Path.Combine(workingpath, "eblatemp");
            
            if (!Directory.Exists(workingpath))
                Directory.CreateDirectory(workingpath);

            var lines = File.ReadAllLines(filename, Encoding.UTF8);

            //int verNameCol = 1;
            //int verTextCol = 0;
            //int descripCol = 2;
            //int infoCol = 3;
            //int authTransCol = 7;
            //int genreCol = -1;
            //int copyrightCol = -1;
            //int refdateCol = 5;


            int verNameCol = 1;
            int verTextCol = 7;
            int descripCol = 0;
            int infoCol = 2;
            int authTransCol = 3;
            int genreCol = -1;
            int copyrightCol = 4;
            int refdateCol = 5;



            int versionCount = 0;

            for (int i = 0; i < (lines.Length - 1); i++)
            {
                var xmlDoc = EblaHelpers.CreateXmlDocument("html");
                XmlNode headNode = xmlDoc.CreateElement("head");
                xmlDoc.DocumentElement.AppendChild(headNode);


                var attrib = xmlDoc.CreateAttribute("data-ebla-lang");
                attrib.Value = "eng";
                if (i == 0)
                    attrib.Value = "heb";
                headNode.Attributes.Append(attrib);

                XmlNode xmlBodyNode = xmlDoc.CreateElement("body");
                xmlDoc.DocumentElement.AppendChild(xmlBodyNode);

                // first line is header line ... split line i+1
                var cols = lines[i + 1].Split(new char[] { '\t' });
                // reached blanks?
                if (string.IsNullOrEmpty(cols[0]))
                    break;


                for (int j = 0; j < cols.Length; j++)
                    cols[j] = cols[j].Trim();
                string text = cols[verTextCol].Trim();





                attrib = xmlDoc.CreateAttribute("data-ebla-authtran");
                if (authTransCol != -1 && authTransCol < cols.Length)
                    attrib.Value = cols[authTransCol];
                headNode.Attributes.Append(attrib);

                attrib = xmlDoc.CreateAttribute("data-ebla-copyright");
                if (copyrightCol != -1 && copyrightCol < cols.Length)
                    attrib.Value = cols[copyrightCol];
                headNode.Attributes.Append(attrib);

                attrib = xmlDoc.CreateAttribute("data-ebla-description");
                if (descripCol != -1 && descripCol < cols.Length)
                    attrib.Value = cols[descripCol];
                headNode.Attributes.Append(attrib);

                attrib = xmlDoc.CreateAttribute("data-ebla-genre");
                if (genreCol != -1 && genreCol < cols.Length)
                    attrib.Value = cols[genreCol];
                headNode.Attributes.Append(attrib);

                XmlAttribute infoAttrib = xmlDoc.CreateAttribute("data-ebla-info");

                if (infoCol != -1 && infoCol < cols.Length)
                {
                    infoAttrib.Value = cols[infoCol];
                }
                headNode.Attributes.Append(infoAttrib);

                if (refdateCol != -1 && refdateCol < cols.Length)
                {
                    if (!string.IsNullOrEmpty(cols[refdateCol]))
                    {
                        int rd = 0;
                        if (int.TryParse(cols[refdateCol], out rd))
                        {
                            attrib = xmlDoc.CreateAttribute("data-ebla-date");
                            attrib.Value = rd.ToString();
                            headNode.Attributes.Append(attrib);
                        }
                        else
                        {
                            string s = string.Empty;
                            s += infoAttrib.Value;
                            if (s.Length > 0)
                                s += Environment.NewLine;
                            s += "Publication date: " + cols[refdateCol];
                            infoAttrib.Value = s;
                        }
                    }
                }
                



                XmlText xmlText = xmlDoc.CreateTextNode(text);
                xmlBodyNode.AppendChild(xmlText);

                var attribs = new List<XmlAttribute>();
                attrib = xmlDoc.CreateAttribute("data-eblasegid");
                attrib.Value = "1";
                attribs.Add(attrib);
                attrib = xmlDoc.CreateAttribute("data-eblatype");
                attrib.Value = "startmarker";
                attribs.Add(attrib);
                if (i == 0)
                {
                    attrib = xmlDoc.CreateAttribute("data-userattrib-label");
                    attrib.Value = "1";
                    attribs.Add(attrib);
                }
                else
                {
                    attrib = xmlDoc.CreateAttribute("data-userattrib-alignlabel");
                    attrib.Value = "1";
                    attribs.Add(attrib);
                }
                var pos = new XmlDocPos();
                pos.textContent = xmlText;
                pos.offset = 0;
                Document.InsertStartSegElm(pos, xmlDoc, attribs, false);
                attribs.Clear();
                attrib = xmlDoc.CreateAttribute("data-eblasegid");
                attrib.Value = "1";
                attribs.Add(attrib);
                attrib = xmlDoc.CreateAttribute("data-eblatype");
                attrib.Value = "endmarker";
                attribs.Add(attrib);
                pos.offset = xmlText.InnerText.Length;
                Document.InsertEndSegElm(pos, xmlDoc, attribs, false);

                string html = EblaHelpers.XmlDocumentToString(xmlDoc);

                html = html.Replace("XNLX", "<br />");

                int ihtml = html.IndexOf("<html");
                html = html.Substring(ihtml);

                EblaAPI.IDocument doc = null;
                HtmlErrors errors;

                if (i == 0)
                {
                    doc = ScratchHelpers.GetBaseText(corpName);
                    // base text
                    errors = corp.UploadBaseText(html);

                    string workingfile = Path.Combine(workingpath, "basetext.html");
                    File.Delete(workingfile);
                    File.AppendAllText(workingfile, html);
                }
                else
                {
                    corp.CreateVersion(cols[verNameCol], new DocumentMetadata() );
                    versionCount++;
                    doc = ScratchHelpers.GetVersion(corpName, cols[verNameCol]);
                    Console.WriteLine("Created version " + cols[verNameCol] + " ...");
                    errors = corp.UploadVersion(cols[verNameCol], html);

                    string workingfile = Path.Combine(workingpath, "version" + i + ".html");
                    File.Delete(workingfile);
                    File.AppendAllText(workingfile, html);
                }

                if (errors.HtmlParseErrors.Length > 0)
                {
                    Console.WriteLine("Errors were encountered parsing the HTML file.");

                    foreach (var e in errors.HtmlParseErrors)
                    {
                        string message = e.Reason + " at line " + e.Line.ToString() + ", position " + e.LinePosition.ToString();
                        Console.WriteLine(message);

                    }
                    Console.ReadKey();
                    return;
                }

                //var md = doc.GetMetadata();
                //if (authTransCol != -1 && authTransCol < cols.Length)
                //    md.AuthorTranslator = cols[authTransCol];
                //if (copyrightCol != -1 && copyrightCol < cols.Length)
                //    md.CopyrightInfo = cols[copyrightCol];
                //if (descripCol != -1 && descripCol < cols.Length)
                //    md.Description = cols[descripCol];
                //if (genreCol != -1 && genreCol < cols.Length)
                //    md.Genre = cols[genreCol];
                //if (infoCol != -1 && infoCol < cols.Length)
                //    md.Information = cols[infoCol];
                //if (i > 0)
                //{
                //    md.NameIfVersion = cols[verNameCol];
                //    md.LanguageCode = "eng";
                //}
                //if (refdateCol != -1 && refdateCol < cols.Length)
                //{
                //    if (!string.IsNullOrEmpty(cols[refdateCol]))
                //    {
                //        int rd = 0;
                //        if (int.TryParse(cols[refdateCol], out rd))
                //        {
                //            md.ReferenceDate = rd;
                //        }
                //        else
                //        {
                //            string s = string.Empty;
                //            s += md.Information;
                //            if (s.Length > 0)
                //                s += Environment.NewLine;
                //            s += "Publication date: " + cols[refdateCol];
                //            md.Information = s;
                //        }
                //    }
                //}
                   

                
                //doc.SetMetadata(md);


            } // each line
            Console.WriteLine("Created " + versionCount + " versions.");
            Console.ReadKey();

        }

        static void Main(string[] args)
        {

            //TabDelimToCorpus(@"C:\share\Ezekiel16-7 test.txt", "Ezekiel 16-7");
            //TabDelimToCorpus(@"C:\share\Ezekiel unique.txt", "Ezekiel 16-7");
            //TabDelimToCorpus(@"C:\share\Exodus 12-12-utf8.txt", "Exodus 12-12");
            TabDelimToCorpus(@"C:\share\4sons - nls-utf8.txt", "4 sons");
            return;

            string filespec = string.Empty;

            if (args.Length > 0)
                filespec = args[0];
            else
            {
                //filespec = @"C:\share\Czech Othellos\Bejblik_Oth_ALIGN.html";
                //filespec = @"C:\DevWork\VVV\Czech Othellos\*.html";
                filespec = @"C:\DevWork\VVV\Czech Othellos\utf8\*.html";
            }

            //filespec = @"C:\DevWork\VVV\Czech Othellos\Maly_Oth_1843_ALIGN.html";

            //if (Path.GetExtension(filespec).ToLower().CompareTo(".docx") != 0)
            //{
            //    Console.WriteLine("Please specify a filespec with extension '.docx'");
            //    return;
            //}

            if (!Path.IsPathRooted(filespec))
                filespec = Path.Combine(Directory.GetCurrentDirectory(), filespec);

            List<string> files = new List<string>();

            foreach (var f in Directory.EnumerateFiles(Path.GetDirectoryName(filespec), Path.GetFileName(filespec)))
            {
                files.Add(f);
            }

            bool createCorp = true;

            ICorpusStore corpusStore = null;
            ICorpus corpus = null;
            string engLangCode = "eng";
            string czLangCode = "ces";
            if (createCorp)
            {
                corpusStore = ScratchHelpers.GetCorpusStore();
                var corpora = new HashSet<string>(corpusStore.GetCorpusList());
                string corpName = "Czech";
                if (corpora.Contains(corpName))
                    corpusStore.DeleteCorpus(corpName);
                corpusStore.CreateCorpus(corpName, "test", engLangCode);

                corpus = ScratchHelpers.GetCorpus(corpName);
            }


            try
            {
                //ProcessPavelFile(@"C:\share\Czech Othellos\Bejblik_Oth_ALIGN.html", true);
                for (int i = 0; i < files.Count; i++)
                {
                    if (!createCorp)
                    {
                        Console.WriteLine("Processing " + files[i]);
                        ProcessPavelFile(files[i], i == 0);
                    }
                    else
                    {
                        string filename = Path.GetFileNameWithoutExtension(files[i]);

                        string vfilename = Path.Combine(new string[] {
                            Path.GetDirectoryName(files[i]),
                            filename + "-vexp.htm"
                            });

                        string btfilename = Path.Combine(new string[] {
                            Path.GetDirectoryName(files[i]),
                            filename + "-bexp.htm"});

                        if (i == 0) 
                        {
                            Console.WriteLine("uploading base text");
                            string html = File.ReadAllText(btfilename, Encoding.UTF8);
                            corpus.UploadBaseText(html);
                        }
                        var md = new DocumentMetadata();
                        md.LanguageCode = czLangCode;
                        
                        corpus.CreateVersion(filename, md);
                        Console.WriteLine("uploading version " + filename);
                        corpus.UploadVersion(filename, File.ReadAllText(vfilename, Encoding.UTF8));
                    }




                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Caught exception of type: " + ex.GetType().Name);
                Console.WriteLine("Message: " + ex.Message);
            }
            Console.WriteLine("Done");
            Console.ReadKey();
        }

        public enum PlayItemType
        {
            stageDirection,
            speech
        }

        public abstract class PlayItem
        {
            public abstract PlayItemType ItemType {get;}

            public string Html;

            public List<string> LeadingPseudoTags = new List<string>();
            public List<string> TrailingPseudoTags = new List<string>();

            List<PlayItem> SubItems = new List<PlayItem>();
        }

        public class SDItem : PlayItem
        {

            public override PlayItemType ItemType
            {
	            get { return PlayItemType.stageDirection; }
            }
            
        }
        public class SpeechItem : PlayItem
        {

            public override PlayItemType ItemType
            {
	            get { return PlayItemType.speech; }
            }

            public string Speaker;

            public List<string> labels = new List<string>();
        }

        static List<PlayItem> _baseTextStructure = new List<PlayItem>();
        static List<PlayItem> _versionStructure = new List<PlayItem>();

        class SegInfo
        {
            public XmlDocPos startPos = new XmlDocPos();
            public XmlDocPos endPos = new XmlDocPos();
            public string Speaker;
            public bool IsSD { get { return string.IsNullOrEmpty(Speaker); } }

            public int StartOffset;
            public int EndOffset;

            public int ID;

            public void SetStart(XmlDocPos pos)
            {
                startPos.textContent = pos.textContent;
                startPos.offset = pos.offset;
                StartOffset = XmlDocPos.MeasureTo(pos, false, Document.IgnoreElm);
                
            }
            public void SetEnd(XmlDocPos pos)
            {
                endPos.textContent = pos.textContent;
                endPos.offset = pos.offset;
                EndOffset = XmlDocPos.MeasureTo(pos, false, Document.IgnoreElm);
            }

            public string ExtraAttribName;
            public string ExtraAttribVal;
        }

        static void ProcessPavelFile(string htmlFilename, bool learnBaseTextStructure)
        {
            var doc = new HtmlAgilityPack.HtmlDocument();

            doc.Load(htmlFilename);

            bool hasErrors = false;
            foreach (var e in doc.ParseErrors)
            {
                Console.WriteLine("Error: " + e.Reason);
                Console.WriteLine("Source: " + e.SourceText);
                Console.WriteLine("Line: " + e.Line.ToString());
                Console.WriteLine("Line pos: " + e.LinePosition);
                hasErrors = true;
            }

            if (hasErrors)
                return;



            HtmlNodeCollection htmlbodyNodes = doc.DocumentNode.SelectNodes("/html/body");

            if (htmlbodyNodes == null || htmlbodyNodes.Count == 0)
            {
                
                throw new Exception( "HTML 'body' element not found");
                
            }

            if (htmlbodyNodes.Count > 1)
            {
                throw new Exception("More than one HTML 'body' element found");

            }

            HtmlNode htmlbodyNode = htmlbodyNodes[0];


            XmlDocument xmlDoc = EblaHelpers.CreateXmlDocument("body");
            XmlDocument xmlBaseTextDoc = EblaHelpers.CreateXmlDocument("html");
            XmlDocument xmlVersionDoc = EblaHelpers.CreateXmlDocument("html");

            XmlNode tempNode = xmlBaseTextDoc.CreateElement("head");
            xmlBaseTextDoc.DocumentElement.AppendChild(tempNode);
            XmlNode xmlBaseTextBodyNode = xmlBaseTextDoc.CreateElement("body");
            xmlBaseTextDoc.DocumentElement.AppendChild(xmlBaseTextBodyNode);

            tempNode = xmlVersionDoc.CreateElement("head");
            xmlVersionDoc.DocumentElement.AppendChild(tempNode);
            XmlNode xmlVersionBodyNode = xmlVersionDoc.CreateElement("body");
            xmlVersionDoc.DocumentElement.AppendChild(xmlVersionBodyNode);

            XmlAttribute attrib = xmlDoc.CreateAttribute("id");
            attrib.Value = "0";
            xmlDoc.DocumentElement.Attributes.Append(attrib);

            //attrib = xmlBaseTextDoc.CreateAttribute("id");
            //attrib.Value = "0";
            //xmlBaseTextDoc.DocumentElement.Attributes.Append(attrib);

            //attrib = xmlVersionDoc.CreateAttribute("id");
            //attrib.Value = "0";
            //xmlVersionDoc.DocumentElement.Attributes.Append(attrib);


            //XmlDocument xmlDocTOC = EblaHelpers.CreateXmlDocument("TOC");

            int totalChars = 0;
            int id = 1;
            Corpus.AppendNodes(xmlDoc.DocumentElement, htmlbodyNode, ref id, null, ref totalChars, false);


            string temp = EblaHelpers.XmlDocumentToString(xmlDoc);
            xmlDoc.LoadXml(temp);
#if _VVV_PRESERVEWHITESPACE
            EblaHelpers.ConvertWhitespaceNodesToTextNodes(xmlDoc.DocumentElement);
#endif

            bool lastTextNodeHadTrailingWhitespace = false;
            totalChars = 0;

            //TODO: Check overloaded method. Corpus.RemoveUnnecessaryWhitespace has different parameters.
            //Corpus.RemoveUnnecessaryWhitespace(xmlDoc.DocumentElement, ref lastTextNodeHadTrailingWhitespace, ref totalChars, Document.IgnoreElm);

            XmlNode bodyNode = xmlDoc.DocumentElement;//.SelectSingleNode("body");
            XmlNode mainTable = bodyNode.SelectSingleNode("table");
            if (mainTable == null)
                throw new Exception("Main table not found");
            XmlNode tbody = mainTable.SelectSingleNode("tbody");


            XmlNodeList rows = tbody.SelectNodes("tr");

            int structureIndex = 0;

            PlayItem baseItem = null;
            PlayItem versionItem = null;
            XmlDocPos docPos = new XmlDocPos();

            int nextBaseTextSegID = 1;
            int nextBaseTextSDSegID = 10000;
            int nextVersionSegID = 1;
            int nextVersionSDSegID = 10000;

            int cellBaseTextSegId = 0;

            for (int rowIndex = 0; rowIndex < rows.Count; rowIndex++)
            {
                var row = rows[rowIndex];

                XmlNodeList cells = row.SelectNodes("td");

                if (cells.Count != 2)
                {
                    throw new Exception("Found row with " + cells.Count.ToString() + " cells, expected 2. Rowindex: " + rowIndex.ToString());
                }

                // first 4 rows we can skip
                if (rowIndex < 4)
                    continue;

                var leadingPseudoTags = new List<string>();
                var trailingPseudoTags = new List<string>();
                string speakerName;
                XmlNode cell = null;

                List<string> alignLabel = new List<string>();
                if (true)
                {
                    cell = cells[0];

                    var para = GetCellPara(cell, rowIndex);

                    if (string.Compare(para.InnerText,"[ignore]") == 0)
                        continue;

                    Debug.Assert(para.ChildNodes.Count > 0);

                    //string baseText = cells[0].InnerHtml;

                    StripLeadingPseudoTags(para, leadingPseudoTags);
                    StripTrailingPseudoTags(para, trailingPseudoTags);

                    //var markerType = GetMarkerType(baseText);
                    Debug.Assert(para.InnerText.IndexOf('<') == -1);
                    Debug.Assert(para.InnerText.IndexOf('>') == -1);

                    int unused = 0;
                    int childCount = xmlBaseTextBodyNode.ChildNodes.Count;
                    Corpus.AppendNodes(xmlBaseTextBodyNode, cell, ref unused, ref unused);

                    if (cell.InnerText.IndexOf("Duke of") > -1)
                    {
                        string s = "fkjfk2";
                    }

                    para = xmlBaseTextBodyNode.ChildNodes[childCount + 1];
                    Debug.Assert(para.Name == "p");

                    if (nextBaseTextSegID == 1005)
                    {
                        string s = "fkfkj";
                    }

                    var segsForPara = new List<SegInfo>();
                    var paraStartPos = new XmlDocPos();
                    var paraEndPos = new XmlDocPos();
                    bool speakerSegFound = false;

                    // Is the next segment a speech?
                    speakerName = GetSpeakerName(para, docPos);
                    if (speakerName == null)
                    {
                        // no
                        baseItem = new SDItem();

                        var seginfo = new SegInfo();

                        //baseItem.Html = baseText;
                        docPos.textContent = FindNextTextSiblingOrChild(para.ChildNodes[0]);
                        Debug.Assert(docPos.textContent != null);
                        docPos.offset = 0;
                        seginfo.SetStart(docPos);
                        seginfo.ID = nextBaseTextSDSegID++;
#if IMMSEG
                        InsertStartSDSeg(xmlBaseTextDoc, docPos, nextBaseTextSDSegID);
#endif

                        docPos.textContent = FindLastTextChild(para);
                        Debug.Assert(docPos.textContent != null);
                        docPos.offset = docPos.textContent.Value.Length;
                        seginfo.SetEnd(docPos);
                        segsForPara.Add(seginfo);

#if IMMSEG                        
                        InsertEndSeg(xmlBaseTextDoc, docPos, nextBaseTextSDSegID++);
#endif

                    }
                    else
                    {
                        var si = new SpeechItem();
                        si.labels = new List<string>();
                        si.Speaker = speakerName;
                        baseItem = si;


                        while (docPos.offset < docPos.textContent.Value.Length)
                            if (char.IsWhiteSpace(docPos.textContent.Value[docPos.offset]))
                                docPos.offset++;
                            else
                                break;


                        var startPos = new XmlDocPos();
                        startPos.textContent = docPos.textContent;
                        startPos.offset = docPos.offset;
                        
                        paraStartPos.textContent = startPos.textContent;
                        paraStartPos.offset = startPos.offset;
                        var endPos = new XmlDocPos();

                        
                        paraEndPos.textContent = FindLastTextChild(para);
                        paraEndPos.offset = paraEndPos.textContent.Length;

                        bool foundSD = false;
                        do
                        {

                            // Any embedded s.d.s?
                            var seekPos = SeekToSDOrTRN(para, startPos);

                            if (seekPos == null)
                            {

                                endPos.textContent = FindLastTextChild(para);
                                Debug.Assert(endPos.textContent != null);
                                foundSD = false;
                            }
                            else
                            {
                                //Debug.Assert(string.Compare(seekPos.textContent.Value.Substring(seekPos.offset, 4), "[TRN") != 0);
                                Debug.Assert(seekPos.textContent.Value.IndexOf("{TRN") == -1);
                                endPos.textContent = seekPos.textContent;
                                foundSD = true;
                            }

                            endPos.offset = endPos.textContent.Value.Length;
                            if (foundSD)
                                if (endPos.offset > 0)
                                    if (endPos.textContent.Value[endPos.offset - 1] == '(')
                                        endPos.offset--;

                            

                            bool singleTextNode = object.ReferenceEquals(startPos.textContent, endPos.textContent);
                            // Empty?
                            bool empty = false;
                            if (singleTextNode)
                                empty = startPos.textContent.Value.Substring(startPos.offset, endPos.offset - startPos.offset).Trim().Length == 0;

                            var sdStartPos = new XmlDocPos();
                            sdStartPos.textContent = endPos.textContent;
                            sdStartPos.offset = endPos.offset;
                            var sdEndPos = new XmlDocPos();
                            sdEndPos.textContent = endPos.textContent;
                            sdEndPos.offset = endPos.offset;

                            if (!empty)
                            {
                                alignLabel.Add(nextBaseTextSegID.ToString());
                                si.labels.Add(nextBaseTextSegID.ToString());
                                cellBaseTextSegId = nextBaseTextSegID;

                                var seginfo = new SegInfo();
                                seginfo.Speaker = speakerName;
                                seginfo.SetStart(startPos);
                                seginfo.SetEnd(endPos);
                                seginfo.ExtraAttribName = "label";
                                seginfo.ExtraAttribVal = alignLabel[alignLabel.Count - 1];
                                seginfo.ID = nextBaseTextSegID;
                                segsForPara.Add(seginfo);

                                if (!speakerSegFound)
                                {
                                    nextBaseTextSegID++;
                                    speakerSegFound = true;
                                }

#if IMMSEG
                                var startelm = InsertStartSpeakerSeg(xmlBaseTextDoc, startPos, speakerName, nextBaseTextSegID, "label", alignLabel[alignLabel.Count - 1]);
                                if (singleTextNode)
                                {
                                    endPos.textContent = (XmlText) startelm.NextSibling;
                                    endPos.offset = endPos.textContent.Value.Length;
                                }
                                var e = InsertEndSeg(xmlBaseTextDoc, endPos, nextBaseTextSegID++);
                                sdStartPos.textContent = FindNextTextSiblingOrChild(e.NextSibling);
                                sdStartPos.offset = 0;
#endif
                                sdEndPos.textContent = sdStartPos.textContent;
                                if (sdEndPos.textContent != null)
                                    sdEndPos.offset = sdEndPos.textContent.Length;
                            }

                            if (foundSD)
                            {
                                //var sdStartPos = new XmlDocPos();
                                //sdStartPos.textContent = endPos.textContent;
                                //sdStartPos.offset = endPos.offset;
                                //var startelm = InsertStartSDSeg(xmlBaseTextDoc, endPos, nextBaseTextSegID++);
                                var sib = endPos.textContent.NextSibling;
                                //var sdEndPos = new XmlDocPos();
                                //sdEndPos.textContent = FindNextTextSiblingOrChild(sib);
                                //sdEndPos.offset = sdEndPos.textContent.Length;

                                while (sib.Name != "em" && sib.Name != "strong")
                                {
                                    sib = sib.NextSibling;
                                    Debug.Assert(sib != null);
                                    Debug.Assert(sib.NodeType != XmlNodeType.Text);
                                }

                                sdEndPos.textContent = FindNextTextSiblingOrChild(sib);
                                sdEndPos.offset = sdEndPos.textContent.Length;

                                //Debug.Assert(endPos.textContent.NextSibling.Name == "em");
                                var nextPossibleStart = sib.NextSibling;
                                if (nextPossibleStart == null)
                                {
                                    foundSD = false; // no more speech to segment
                                    sdEndPos = paraEndPos;
                                    var seginfo = new SegInfo();
                                    seginfo.SetStart(sdStartPos);
                                    seginfo.SetEnd(sdEndPos);
                                    segsForPara.Add(seginfo);
                                    seginfo.ID = nextBaseTextSDSegID++;
#if IMMSEG
                                    InsertSDSegPair(xmlBaseTextDoc, sdStartPos, sdEndPos, nextBaseTextSDSegID++);
#endif
                                }
                                else
                                {
                                    while (nextPossibleStart.NodeType != XmlNodeType.Text)
                                    {
                                        nextPossibleStart = nextPossibleStart.NextSibling;
                                        if (nextPossibleStart == null)
                                            break;
                                    }

                                    if (nextPossibleStart == null)
                                    {
                                        foundSD = false;// probably no more speech to segment
                                        sdEndPos = paraEndPos;
                                        var seginfo = new SegInfo();
                                        seginfo.SetStart(sdStartPos);
                                        seginfo.SetEnd(sdEndPos);
                                        segsForPara.Add(seginfo);
                                        seginfo.ID = nextBaseTextSDSegID++;
#if IMMSEG
                                        InsertSDSegPair(xmlBaseTextDoc, sdStartPos, sdEndPos, nextBaseTextSDSegID++);
#endif

                                    }
                                    else
                                    {
                                        startPos.textContent = (XmlText)nextPossibleStart;
                                        startPos.offset = 0;
                                        if (startPos.textContent.Value.Length > 0)
                                            if (startPos.textContent.Value[0] == ')')
                                                startPos.offset = 1;

                                        sdEndPos.textContent = startPos.textContent;
                                        sdEndPos.offset = startPos.offset;
                                        var seginfo = new SegInfo();
                                        seginfo.SetStart(sdStartPos);
                                        seginfo.SetEnd(sdEndPos);
                                        segsForPara.Add(seginfo);
                                        seginfo.ID = nextBaseTextSDSegID++;
#if IMMSEG
                                        InsertSDSegPair(xmlBaseTextDoc, sdStartPos, sdEndPos, nextBaseTextSDSegID++);
#endif

                                    }
                                }
                            }


                        } while (foundSD); // while (skipping sds)

                    }
#if IMMSEG
#else
                    var tempsegsForPara = new List<SegInfo>();
                    if (segsForPara.Count > 0)
                        tempsegsForPara.Add(segsForPara[0]);
                    
                    for (int i = 1; i < segsForPara.Count; i++)
                    {
                        if (segsForPara[i].IsSD)
                            if (tempsegsForPara[tempsegsForPara.Count - 1].IsSD)
                            {
                                tempsegsForPara[tempsegsForPara.Count - 1].EndOffset = segsForPara[i].EndOffset;
                                continue;
                            }

                        tempsegsForPara.Add(segsForPara[i]);
                    }
                    segsForPara = tempsegsForPara;
                    // Any non-sd segs?
                    var speakerSegInfo = segsForPara.Find(x => !x.IsSD);
                    if (speakerSegInfo != null)
                    {
                        speakerSegInfo.SetStart(paraStartPos);
                        cellBaseTextSegId = speakerSegInfo.ID;
                        speakerSegInfo.SetEnd(paraEndPos);
                        tempsegsForPara = new List<SegInfo>();
                        tempsegsForPara.Add(speakerSegInfo);
                        foreach (var s in segsForPara)
                            if (s.IsSD)
                                tempsegsForPara.Add(s);
                        segsForPara = tempsegsForPara;
                    }

                    foreach (var seginfo in segsForPara)
                    {
                        int i = seginfo.StartOffset;
                        var startPos = XmlDocPos.SeekToOffset(ref i, xmlBaseTextDoc.DocumentElement, Document.IgnoreElm);
                        i = seginfo.EndOffset;
                        var endPos = XmlDocPos.SeekToOffset(ref i, xmlBaseTextDoc.DocumentElement, Document.IgnoreElm);
                        Debug.Assert(seginfo.ID > 0);
                        if (seginfo.IsSD)
                            InsertSDSegPair(xmlBaseTextDoc, startPos, endPos, seginfo.ID);
                        else
                            InsertSpeakerSegPair(xmlBaseTextDoc, startPos, endPos, seginfo.Speaker, seginfo.ID, seginfo.ExtraAttribName, seginfo.ExtraAttribVal);
                    }
#endif
                    baseItem.LeadingPseudoTags = new List<string>(leadingPseudoTags);
                    baseItem.TrailingPseudoTags = new List<string>(trailingPseudoTags);

                    Debug.WriteLine("------------------");
                    Debug.WriteLine(para.InnerText);
                    if (baseItem.ItemType == PlayItemType.speech)
                    {
                        var si = (SpeechItem)baseItem;
                        for (int i = 0; i < si.labels.Count; i++)
                            Debug.WriteLine(si.labels[i]);
                    }


                    if (learnBaseTextStructure)
                        _baseTextStructure.Add(baseItem);
                    else
                    {
                        Debug.Assert(baseItem.ItemType == _baseTextStructure[structureIndex].ItemType);
                        if (baseItem.ItemType == PlayItemType.speech)
                        {
                            var si = (SpeechItem)baseItem;
                            var sis = (SpeechItem)_baseTextStructure[structureIndex];
                            Debug.Assert(si.labels.Count == sis.labels.Count);
                            for (int i = 0; i < si.labels.Count; i++)
                                Debug.Assert(si.labels[i] == sis.labels[i]);
                        }
                        baseItem = _baseTextStructure[structureIndex];
                    }
                        
                    leadingPseudoTags.Clear();
                    trailingPseudoTags.Clear();

                    
                }


                cell = cells[1];

                var paras = GetCellParas(cell, rowIndex);

                var segsForCell = new List<SegInfo>();

                PreProcessTranslatorNotePseudoTag(paras[0]);
                StripLeadingPseudoTags(paras[0], leadingPseudoTags);
                StripTrailingPseudoTags(paras[0], trailingPseudoTags);
                var markerType = GetMarkerType(paras[0].InnerText);

                XmlDocPos cellStartPos = null;
                XmlDocPos cellEndPos = null;

                var versionItems = new List<PlayItem>();
                if (markerType == MarkerType.noMarker)
                {
                    PreProcessTranslatorNotePseudoTag(paras[0]);
                    int childCount = xmlVersionBodyNode.ChildNodes.Count;
                    int unused = 0;
                    Corpus.AppendNodes(xmlVersionBodyNode, cell, ref unused, ref unused);

                    var newParas = new List<XmlNode>();
                    for (int n = childCount; n < xmlVersionBodyNode.ChildNodes.Count; n++)
                    {
                        if (xmlVersionBodyNode.ChildNodes[n].Name == "p")
                            newParas.Add(xmlVersionBodyNode.ChildNodes[n]);
                        //newParas.Add(xmlVersionBodyNode.ChildNodes[childCount + n]);
                    }
                    Debug.Assert(paras.Count == newParas.Count);
                    paras = newParas;

                    int alignCount = 0;

                    cellStartPos = null;
                    cellEndPos = null;
                    foreach (var para in paras)
                    {
                        Debug.Assert(para.Name == "p");

                        PreProcessTranslatorNotePseudoTag(para);
                        StripTrailingPseudoTags(para, trailingPseudoTags);


                        Debug.Assert(para.InnerText.IndexOf('<') == -1);
                        Debug.Assert(para.InnerText.IndexOf('>') == -1);

                        // Speech?
                        speakerName = GetSpeakerName(para, docPos);
                        if (speakerName == null)
                            versionItem = new SDItem();
                        else
                        {
                            var si = new SpeechItem();

                            si.Speaker = speakerName;
                            versionItem = si;

                            while (docPos.offset < docPos.textContent.Value.Length)
                                if (char.IsWhiteSpace(docPos.textContent.Value[docPos.offset]))
                                    docPos.offset++;
                                else
                                    break;

                            RemoveTRN(para, docPos );

                            Debug.Assert(para.InnerText.IndexOf("{") == -1);
                            Debug.Assert(para.InnerText.IndexOf("}") == -1);

                            var startPos = new XmlDocPos();
                            startPos.textContent = docPos.textContent;
                            startPos.offset = docPos.offset;
                            var endPos = new XmlDocPos();


                            var paraStartPos = new XmlDocPos();
                            paraStartPos.textContent = startPos.textContent;
                            paraStartPos.offset = startPos.offset;

                            var paraEndPos = new XmlDocPos();
                            paraEndPos.textContent = FindLastTextChild(para);
                            paraEndPos.offset = paraEndPos.textContent.Length;

                            if (cellStartPos == null)
                                cellStartPos = paraStartPos;

                            cellEndPos = paraEndPos;

                            bool foundSDOrTRN = false;
                            do
                            {
                                bool foundSD = false;
                                bool foundTRN = false;

                                // Any embedded s.d.s?
                                var seekPos = SeekToSDOrTRN(para, startPos);

                                if (seekPos == null)
                                {

                                    endPos.textContent = FindLastTextChild(para);
                                    Debug.Assert(endPos.textContent != null);
                                    endPos.offset = endPos.textContent.Value.Length;
                                    foundSDOrTRN = false;
                                }
                                else
                                {
                                    foundTRN = false;
                                    if ((seekPos.offset + 4) <= seekPos.textContent.Value.Length)
                                        foundTRN = string.Compare(seekPos.textContent.Value.Substring(seekPos.offset, 4), "{TRN") == 0;
                                    foundSD = !foundTRN;
                                    endPos.textContent = seekPos.textContent;
                                    foundSDOrTRN = true;
                                    endPos.offset = endPos.textContent.Value.Length;
                                    if (foundTRN)
                                    {
                                        endPos.offset = seekPos.offset;
                                        while (endPos.offset > 0)
                                            if (char.IsNumber(endPos.textContent.Value[endPos.offset - 1]))
                                                endPos.offset--;
                                            else
                                                break;
                                    }
                                }

                                //endPos.offset = endPos.textContent.Value.Length;
                                //bool embeddedSD = false;
                                var sib = endPos.textContent.NextSibling;
                                if (foundSD)
                                    if (endPos.offset > 0)
                                        if (endPos.textContent.Value[endPos.offset - 1] == '(')
                                        {
                                            endPos.offset--;
                                            //embeddedSD = true;
                                        }



                                bool singleTextNode = object.ReferenceEquals(startPos.textContent, endPos.textContent);
                                // Empty?
                                bool empty = false;
                                if (singleTextNode)
                                    empty = startPos.textContent.Value.Substring(startPos.offset, endPos.offset - startPos.offset).Trim().Length == 0;

                                
                                var sdStartPos = new XmlDocPos();
                                sdStartPos.textContent = endPos.textContent;
                                sdStartPos.offset = endPos.offset;

                                var sdEndPos = new XmlDocPos();
                                sdEndPos.textContent = endPos.textContent;
                                sdEndPos.offset = endPos.offset;

                                if (!empty)
                                {
                                    // Attempt to match seg alignments where pair of paras each contained
                                    // two speech segs for the same speaker (the first of which prefixed with speaker name)
                                    // split with a s.d., and similar cases
                                    string attribName = null;
                                    string attribVal = null;
                                    var seginfo = new SegInfo();
                                    seginfo.Speaker = speakerName;
                                    seginfo.SetStart(startPos);
                                    seginfo.SetEnd(endPos);
                                    if (alignLabel.Count > 0)
                                    {
                                        int i = Math.Min(alignCount, alignLabel.Count - 1);
                                        attribName = "alignlabel";
                                        attribVal = alignLabel[i];
                                        seginfo.ExtraAttribName = attribName;
                                        seginfo.ExtraAttribVal = attribVal;
                                        alignCount++;
                                    }
                                    seginfo.ID = nextVersionSegID++;
                                    segsForCell.Add(seginfo);
#if IMMSEG
                                    var startelm = InsertStartSpeakerSeg(xmlVersionDoc, startPos, speakerName, nextVersionSegID, attribName, attribVal);
                                    if (singleTextNode)
                                    {
                                        endPos.textContent = (XmlText)startelm.NextSibling;
                                        endPos.offset = endPos.offset - startPos.offset; // endPos.textContent.Value.Length;
                                    }
                                    InsertEndSeg(xmlVersionDoc, endPos, nextVersionSegID++);
#endif

                                }

                                //if (!embeddedSD)
                                //{
                                //    // assume terminal SD.
                                //    // can't reliably detect where SD ends otherwise
                                //    // e.g. 'exit' in italic, 'Brabantio' in bold
                                //    foundSD = false;
                                //    foundSDOrTRN = false;
                                //}

                                if (foundSD)
                                {
                                    // must be embedded
                                    //var sib = endPos.textContent.NextSibling;
                                    //Debug.Assert(sib != null);
                                    //Debug.Assert(sib.Name == "span");
                                    //sib = sib.NextSibling;
                                    //Debug.Assert(sib.NodeType == XmlNodeType.Text); // contains '('

                                    // look for em node
                                    // (may have hade br between endpos and em)
                                    while (sib.Name != "em" && sib.Name != "strong")
                                    {
                                        sib = sib.NextSibling;
                                        Debug.Assert(sib != null);
                                        Debug.Assert(sib.NodeType != XmlNodeType.Text);
                                    }


                                    sdEndPos.textContent = FindNextTextSiblingOrChild(sib);
                                    sdEndPos.offset = sdEndPos.textContent.Length;

                                    //Debug.Assert(endPos.textContent.NextSibling.Name == "em");
                                    var nextPossibleStart = sib.NextSibling;
                                    if (nextPossibleStart == null)
                                    {
                                        foundSD = false; // no more speech to segment
                                        foundSDOrTRN = false;
                                        sdEndPos = paraEndPos;
                                        var seginfo = new SegInfo();
                                        seginfo.SetStart(sdStartPos);
                                        seginfo.SetEnd(sdEndPos);
                                        segsForCell.Add(seginfo);
                                        seginfo.ID = nextVersionSDSegID++;
                                    }
                                    else
                                    {
                                        while (nextPossibleStart.NodeType != XmlNodeType.Text)
                                        {
                                            nextPossibleStart = nextPossibleStart.NextSibling;
                                            if (nextPossibleStart == null)

                                                break;
                                        }

                                        if (nextPossibleStart == null)
                                        {
                                            foundSD = false;// probably no more speech to segment
                                            foundSDOrTRN = false;
                                            sdEndPos = paraEndPos;
                                            var seginfo = new SegInfo();
                                            seginfo.SetStart(sdStartPos);
                                            seginfo.SetEnd(sdEndPos);
                                            segsForCell.Add(seginfo);
                                            seginfo.ID = nextVersionSDSegID++;

                                        }
                                        else
                                        {
                                            startPos.textContent = (XmlText)nextPossibleStart;
                                            startPos.offset = 0;
                                            if (startPos.textContent.Value.Length > 0)
                                                if (startPos.textContent.Value[0] == ')')
                                                    startPos.offset = 1;

                                            sdEndPos.textContent = startPos.textContent;
                                            sdEndPos.offset = startPos.offset;
                                            var seginfo = new SegInfo();
                                            seginfo.SetStart(sdStartPos);
                                            seginfo.SetEnd(sdEndPos);
                                            segsForCell.Add(seginfo);
                                            seginfo.ID = nextVersionSDSegID++;
                                        }
                                    }
                                }

                                if (foundTRN)
                                {
                                    XmlNode n = null;
                                    if (!empty)
                                    {
                                        n = endPos.textContent.NextSibling;
                                        Debug.Assert(n != null);
                                        Debug.Assert(n.Name == "span");
                                        n = n.NextSibling;
                                        Debug.Assert(n != null);
                                    }
                                    else
                                        n = startPos.textContent;
                                    do
                                    {
                                        var t = FindNextTextSiblingOrChild(n);
                                        Debug.Assert(t != null);
                                        int i = t.InnerText.IndexOf("/TRN}");
                                        if (i > -1)
                                        {
                                            startPos.textContent =  t;
                                            startPos.offset = i + 5;
                                            //while (startPos.offset < startPos.textContent.Value.Length)
                                            //    if (char.IsNumber(startPos.textContent.Value[startPos.offset]))
                                            //        startPos.offset++;
                                            //    else
                                            //        break;
                                            break;
                                        }
                                        n = n.NextSibling;
                                        Debug.Assert(n != null);
                                    }
                                    while (true);

                                    //var pos = endPos;
                                    
                                    //do
                                    //{
                                    //    string s = pos.textContent.Value.Substring(pos.offset);
                                    //    int i = s.IndexOf("/TRN]");

                                    //    if (i > -1)
                                    //    {
                                    //        startPos.textContent = pos.textContent;
                                    //        startPos.offset = i + 5;
                                    //        break;

                                    //    }

                                    //    var t = FindNextTextSiblingOrChild(startPos.textContent);
                                    //    Debug.Assert(t != null);
                                    //    pos.textContent = t;
                                    //    pos.offset = 0;
                                    //}
                                    //while (true);

                                } // if foundTRN


                            } while (foundSDOrTRN); // while (skipping sds)





                        }
                        versionItems.Add(versionItem);

                    }

#if IMMSEG
#else
                    var tempsegsForCell = new List<SegInfo>();
                    if (segsForCell.Count > 0)
                        tempsegsForCell.Add(segsForCell[0]);

                    for (int i = 1; i < segsForCell.Count; i++)
                    {
                        if (segsForCell[i].IsSD)
                            if (tempsegsForCell[tempsegsForCell.Count - 1].IsSD)
                            {
                                tempsegsForCell[tempsegsForCell.Count - 1].EndOffset = segsForCell[i].EndOffset;
                                continue;
                            }

                        tempsegsForCell.Add(segsForCell[i]);
                    }
                    segsForCell = tempsegsForCell;

                    // Any non-sd segs?
                    var speakerSegInfo = segsForCell.Find(x => !x.IsSD);
                    if (speakerSegInfo != null)
                    {
                        speakerSegInfo.SetStart(cellStartPos);
                        speakerSegInfo.SetEnd(cellEndPos);
                        tempsegsForCell = new List<SegInfo>();
                        tempsegsForCell.Add(speakerSegInfo);
                        foreach (var s in segsForCell)
                            if (s.IsSD)
                                tempsegsForCell.Add(s);
                        segsForCell = tempsegsForCell;
                    }

                    foreach (var seginfo in segsForCell)
                    {
                        int i = seginfo.StartOffset;
                        var startPos = XmlDocPos.SeekToOffset(ref i, xmlVersionDoc.DocumentElement, Document.IgnoreElm);
                        i = seginfo.EndOffset;
                        var endPos = XmlDocPos.SeekToOffset(ref i, xmlVersionDoc.DocumentElement, Document.IgnoreElm);
                        Debug.Assert(seginfo.ID > 0);
                        if (seginfo.IsSD)
                            InsertSDSegPair(xmlVersionDoc, startPos, endPos, seginfo.ID);
                        else
                        {
                            seginfo.ExtraAttribVal = cellBaseTextSegId.ToString();
                            InsertSpeakerSegPair(xmlVersionDoc, startPos, endPos, seginfo.Speaker, seginfo.ID, seginfo.ExtraAttribName, seginfo.ExtraAttribVal);
                        }
                    }
#endif

                    versionItems[0].LeadingPseudoTags = new List<string>(leadingPseudoTags);
                    versionItems[versionItems.Count - 1].TrailingPseudoTags = new List<string>(trailingPseudoTags);

                    bool match = false;
                    switch (baseItem.ItemType)
                    {
                        case PlayItemType.speech:
                            foreach (var item in versionItems)
                                if (item.ItemType == PlayItemType.speech)
                                {
                                    match = true;
                                    break;
                                }
                            Debug.Assert(match);
                            break;
                        case PlayItemType.stageDirection:
                            Debug.Assert(versionItems[0].ItemType == PlayItemType.stageDirection);
                            break;
                        default:
                            Debug.Assert(false);
                            break;
                    }


                    //Debug.Assert(baseItem.ItemType == versionItem.ItemType);
                    Debug.Assert(baseItem.LeadingPseudoTags.Count == versionItems[0].LeadingPseudoTags.Count);
                    Debug.Assert(baseItem.TrailingPseudoTags.Count == versionItems[versionItems.Count - 1].TrailingPseudoTags.Count);

                    for (int i = 0; i < baseItem.LeadingPseudoTags.Count; i++)
                        Debug.Assert(baseItem.LeadingPseudoTags[i] == versionItems[0].LeadingPseudoTags[i]);
                    for (int i = 0; i < baseItem.TrailingPseudoTags.Count; i++)
                        Debug.Assert(baseItem.TrailingPseudoTags[i] == versionItems[versionItems.Count - 1].TrailingPseudoTags[i]);

                } // if (first para isn't marker)
                else
                {
                    Debug.Assert(paras.Count == 1);
                }




                structureIndex++;
            } // for (each row)


            var settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.Encoding = System.Text.Encoding.UTF8;

            //settings.NewLineChars = "\r\n";
            //settings.NewLineHandling = NewLineHandling.Replace;
            
            //settings.NewLineOnAttributes = true;

            string filename = Path.GetFileNameWithoutExtension(htmlFilename);

            string outfilename = Path.Combine(new string[] {
                Path.GetDirectoryName(htmlFilename),
                filename + "-vexp.htm"
            });

            using (var outfile = new System.IO.StreamWriter(outfilename, false, Encoding.UTF8))
            {
                using (XmlWriter writer = XmlWriter.Create(outfile, settings))
                {
                    xmlVersionDoc.Save(writer);
                    writer.Close();
                }

            }


            if (learnBaseTextStructure)
            {
                outfilename = Path.Combine(new string[] {
                Path.GetDirectoryName(htmlFilename),
                filename + "-bexp.htm"


            });

                using (var outfile = new System.IO.StreamWriter(outfilename, false, Encoding.UTF8))
                {
                    using (XmlWriter writer = XmlWriter.Create(outfile, settings))
                    {
                        xmlBaseTextDoc.Save(writer);
                        writer.Close();
                    }

                }

            }

            //using (XmlWriter writer = XmlWriter.Create(outfilename, settings))
            //{
            //    xmlVersionDoc.Save(writer);
            //    writer.Close();
            //}

            //using (XmlTextWriter writer = new XmlTextWriter(outfilename, Encoding.UTF8))
            //{
            //    writer.Formatting = Formatting.Indented;
            //    writer.Indentation = 4;
                
            //    xmlVersionDoc.Save(writer);
            //    writer.Close();
                
            //}


            //HtmlNode mainTable = bodyNode.SelectSingleNode("table");
            //if (mainTable == null)
            //    throw new Exception("Main table not found");
            //HtmlNode tbody = mainTable.SelectSingleNode("tbody");


            //HtmlNodeCollection rows = tbody.SelectNodes("tr");

            //int structureIndex = 0;

            //PlayItem baseItem = null;
            //PlayItem versionItem = null;

            //for (int rowIndex = 0; rowIndex < rows.Count; rowIndex++)
            //{
            //    var row = rows[rowIndex];

            //    HtmlNodeCollection cells = row.SelectNodes("td");

            //    if (cells.Count != 2)
            //    {
            //        throw new Exception("Found row with " + cells.Count.ToString() + " cells, expected 2. Rowindex: " + rowIndex.ToString() );
            //    }

            //    // first 4 rows we can skip
            //    if (rowIndex < 4)
            //        continue;

            //    var leadingPseudoTags = new List<string>();
            //    var trailingPseudoTags = new List<string>();
            //    string speakerName;
            //    HtmlNode cell = null;
            //    if (learnBaseTextStructure)
            //    {
            //        cell = cells[0];

            //        var para = GetCellPara(cell, rowIndex);


            //        //string baseText = cells[0].InnerHtml;

            //        StripLeadingPseudoTags(para, leadingPseudoTags);
            //        StripTrailingPseudoTags(para, trailingPseudoTags);

            //        //var markerType = GetMarkerType(baseText);
            //        Debug.Assert(para.InnerText.IndexOf('<') == -1);
            //        Debug.Assert(para.InnerText.IndexOf('>') == -1);


            //        // Is the next segment a speech?
            //        speakerName = GetSpeakerName(para);
            //        if (speakerName == null)
            //        {
            //            // no
            //            baseItem = new SDItem();
            //            //baseItem.Html = baseText;
            //        }
            //        else
            //        {
            //            var si = new SpeechItem();
                        
            //            si.Speaker = speakerName;
            //            baseItem = si;
            //        }

            //        baseItem.LeadingPseudoTags = new List<string>(leadingPseudoTags);
            //        baseItem.TrailingPseudoTags = new List<string>(trailingPseudoTags);

            //        _baseTextStructure.Add(baseItem);

            //        leadingPseudoTags.Clear();
            //        trailingPseudoTags.Clear();
            //    }
            //    else
            //    {
            //        baseItem = _baseTextStructure[structureIndex];
            //    }


            //    cell = cells[1];

            //    var paras = GetCellParas(cell, rowIndex);

            //    PreProcessTranslatorNotePseudoTag(paras[0]);
            //    StripLeadingPseudoTags(paras[0], leadingPseudoTags);
            //    StripTrailingPseudoTags(paras[0], trailingPseudoTags);
            //    var markerType = GetMarkerType(paras[0].InnerText);

            //    var versionItems = new List<PlayItem>();
            //    if (markerType == MarkerType.noMarker)
            //    {
            //        PreProcessTranslatorNotePseudoTag(paras[0]);


            //        foreach (var para in paras)
            //        {
            //            PreProcessTranslatorNotePseudoTag(para);
            //            StripTrailingPseudoTags(para, trailingPseudoTags);

            //            Debug.Assert(para.InnerText.IndexOf('<') == -1);
            //            Debug.Assert(para.InnerText.IndexOf('>') == -1);

            //            // Speech?
            //            speakerName = GetSpeakerName(para);
            //            if (speakerName == null)
            //                versionItem = new SDItem();
            //            else
            //            {
            //                var si = new SpeechItem();

            //                si.Speaker = speakerName;
            //                versionItem = si;
            //            }
            //            versionItems.Add(versionItem);

            //        }

            //        versionItems[0].LeadingPseudoTags = new List<string>(leadingPseudoTags);
            //        versionItems[versionItems.Count - 1].TrailingPseudoTags = new List<string>(trailingPseudoTags);

            //        bool match = false;
            //        switch (baseItem.ItemType)
            //        {
            //            case PlayItemType.speech:
            //                foreach (var item in versionItems)
            //                    if (item.ItemType == PlayItemType.speech)
            //                    {
            //                        match = true;
            //                        break;
            //                    }
            //                Debug.Assert(match);
            //                break;
            //            case PlayItemType.stageDirection:
            //                Debug.Assert(versionItems[0].ItemType == PlayItemType.stageDirection);
            //                break;
            //            default:
            //                Debug.Assert(false);
            //                break;
            //        }


            //        //Debug.Assert(baseItem.ItemType == versionItem.ItemType);
            //        Debug.Assert(baseItem.LeadingPseudoTags.Count == versionItems[0].LeadingPseudoTags.Count);
            //        Debug.Assert(baseItem.TrailingPseudoTags.Count == versionItems[versionItems.Count - 1].TrailingPseudoTags.Count);

            //        for (int i = 0; i < baseItem.LeadingPseudoTags.Count; i++)
            //            Debug.Assert(baseItem.LeadingPseudoTags[i] == versionItems[0].LeadingPseudoTags[i]);
            //        for (int i = 0; i < baseItem.TrailingPseudoTags.Count; i++)
            //            Debug.Assert(baseItem.TrailingPseudoTags[i] == versionItems[versionItems.Count - 1].TrailingPseudoTags[i]);

            //    } // if (first para isn't marker)
            //    else
            //    {
            //        Debug.Assert(paras.Count == 1);
            //    }




            //    structureIndex++;
            //} // for (each row)


        }

        static void InsertSpeakerSegPair(XmlDocument doc, XmlDocPos startPos, XmlDocPos endPos, string speakerName, int id, string extraAttribName, string extraAttribVal)
        {
            bool singleTextNode = object.ReferenceEquals(startPos.textContent, endPos.textContent);
            // Empty?
            bool empty = false;
            if (singleTextNode)
                empty = startPos.textContent.Value.Substring(startPos.offset, endPos.offset - startPos.offset).Trim().Length == 0;

            if (empty)
                return;

            var startelm = InsertStartSpeakerSeg(doc, startPos, speakerName, id, extraAttribName, extraAttribVal);

            if (singleTextNode)
            {
                endPos.textContent = (XmlText)startelm.NextSibling;
                endPos.offset = endPos.textContent.Value.Length;
            }


            InsertEndSeg(doc, endPos, id);
        }

        static void InsertSDSegPair(XmlDocument doc, XmlDocPos startPos, XmlDocPos endPos, int id)
        {

            bool singleTextNode = object.ReferenceEquals(startPos.textContent, endPos.textContent);
            // Empty?
            bool empty = false;
            if (singleTextNode)
                empty = startPos.textContent.Value.Substring(startPos.offset, endPos.offset - startPos.offset).Trim().Length == 0;

            if (empty)
                return;
            var startelm = InsertStartSDSeg(doc, startPos, id);

            if (singleTextNode)
            {
                endPos.textContent = (XmlText)startelm.NextSibling;
                endPos.offset = endPos.textContent.Value.Length;
            }


            InsertEndSeg(doc, endPos, id);

        }

        static XmlElement InsertStartSpeakerSeg(XmlDocument doc, XmlDocPos pos, string speakerName, int id, string extraAttribName, string extraAttribVal)
        {
            var attribs = new List<XmlAttribute>();
            var attrib = doc.CreateAttribute("data-eblatype");
            attrib.Value = "startmarker";
            attribs.Add(attrib);

            attrib = doc.CreateAttribute("data-userattrib-type");
            attrib.Value = "Speech";
            attribs.Add(attrib);

            attrib = doc.CreateAttribute("data-userattrib-speaker");
            attrib.Value = speakerName;
            attribs.Add(attrib);

            if (!string.IsNullOrEmpty(extraAttribName))
            {
                attrib = doc.CreateAttribute("data-userattrib-" + extraAttribName);
                attrib.Value = extraAttribVal;
                attribs.Add(attrib);

            }
            
            attrib = doc.CreateAttribute("data-eblasegid");
            attrib.Value = id.ToString();
            attribs.Add(attrib);

            return Document.InsertStartSegElm(pos, doc, attribs, false);

        }

        static XmlElement InsertStartSDSeg(XmlDocument doc, XmlDocPos pos, int id)
        {
            var attribs = new List<XmlAttribute>();
            var attrib = doc.CreateAttribute("data-eblatype");
            attrib.Value = "startmarker";
            attribs.Add(attrib);

            attrib = doc.CreateAttribute("data-userattrib-type");
            attrib.Value = "S.D.";
            attribs.Add(attrib);

            attrib = doc.CreateAttribute("data-ebla-cb");
            attrib.Value = "1";
            attribs.Add(attrib);

            attrib = doc.CreateAttribute("data-eblasegid");
            attrib.Value = id.ToString();
            attribs.Add(attrib);

            return Document.InsertStartSegElm(pos, doc, attribs, false);

        }

        static XmlElement InsertEndSeg(XmlDocument doc, XmlDocPos pos, int id)
        {
            var attribs = new List<XmlAttribute>();
            var attrib = doc.CreateAttribute("data-eblatype");
            attrib.Value = "endmarker";
            attribs.Add(attrib);

            attrib = doc.CreateAttribute("data-eblasegid");
            attrib.Value = id.ToString();
            attribs.Add(attrib);

            var e = Document.InsertEndSegElm(pos, doc, attribs, false);

#if DEBUG
            var p = e.ParentNode;
            if (p.Name == "span")
                Debug.Assert(p.Attributes.GetNamedItem("data-eblatype") == null);
#endif
            return e;
        }

        const string _precedesMarker1 = "[předchází]";
        const string _precedesMarker2 = "[predchází]";
        const string _missingMarker = "[chybí]";
        const string _followsMarker = "[následuje]";

        public enum MarkerType
        {
            noMarker,
            precedesMarker,
            missingMarker,
            followsMarker
        }

        static MarkerType GetMarkerType(string text)
        {
            //text = WebUtility.HtmlDecode(text).Trim();

            if (string.Compare(text, _precedesMarker1) == 0)
                return MarkerType.precedesMarker;
            if (string.Compare(text, _precedesMarker2) == 0)
                return MarkerType.precedesMarker;

            if (string.Compare(text, _missingMarker) == 0)
                return MarkerType.missingMarker;
            if (string.Compare(text, _followsMarker) == 0)
                return MarkerType.followsMarker;

            return MarkerType.noMarker;
        }

        static void PreProcessTranslatorNotePseudoTag(XmlNode node)
        {
            if (IsTextNode(node))
            {
                var textNode = node;
                textNode.InnerText = textNode.InnerText.Replace("<TRN>", "{TRN}");
                textNode.InnerText = textNode.InnerText.Replace("</TRN>", "{/TRN}");

            }
            else
            {
                foreach (XmlNode child in node.ChildNodes)
                    PreProcessTranslatorNotePseudoTag(child);
            }

        }

        static void StripLeadingPseudoTags(XmlNode para, List<string> pseudoTags)
        {
            if (para.ChildNodes.Count == 0)
                return;

            if (!(IsTextNode(para.ChildNodes[0])))
                return;

            var textNode = (para.ChildNodes[0]);

            while (true)
            {
                //string text = WebUtility.HtmlDecode(textNode.InnerText).TrimStart();
                string text = textNode.InnerText.TrimStart();

                if (text.Length == 0)
                    return;

                if (text[0] != '<')
                    return;

                int i = text.IndexOf('>');
                if (i == -1)
                    throw new Exception("Unterminated pseudotag");

                pseudoTags.Add(text.Substring(0, i + 1));
                //textNode.InnerText = WebUtility.HtmlEncode( text.Substring(i + 1));
                textNode.InnerText = text.Substring(i + 1);
            }
        }
        static void StripTrailingPseudoTags(XmlNode para, List<string> pseudoTags)
        {
            if (para.ChildNodes.Count == 0)
                return;

            if (!(IsTextNode(para.ChildNodes[para.ChildNodes.Count - 1])))
                return;

            var textNode = (para.ChildNodes[para.ChildNodes.Count - 1]);


            while (true)
            {
                //string text = WebUtility.HtmlDecode( textNode.InnerText.TrimEnd());
                string text = textNode.InnerText.TrimEnd();

                if (text.Length == 0)
                    return;

                if (text[text.Length - 1] != '>')
                    return;

                int i = text.LastIndexOf('<');
                if (i == -1)
                    throw new Exception("Unterminated pseudotag");

                pseudoTags.Add(text.Substring(i, text.Length - i));
                //textNode.InnerText = WebUtility.HtmlEncode( text.Substring(0, i));
                textNode.InnerText = text.Substring(0, i);

            }

        }



        static bool IsEmptyTextNode(XmlNode node, bool skipWhitespace)
        {
            if (IsTextNode(node))
            {
                var textNode = node;
                string t = textNode.InnerText;
                if (skipWhitespace)
                    t = t.Trim();
                if (t.Length == 0)
                    return true;
            }

            return false;

        }

        static XmlText FindLastTextChild(XmlNode node)
        {
            if (node == null)
                return null;
            if (node.NodeType == XmlNodeType.Text)
                return (XmlText)node;

            for (int i = node.ChildNodes.Count; i > 0; i--)
            {
                var t = FindLastTextChild(node.ChildNodes[i - 1]);
                if (t != null)
                    return t;
            }
            return null;
        }

        static XmlDocPos SeekToSDOrTRN(XmlNode para, XmlDocPos fromPos)
        {
            XmlNode current = fromPos.textContent;
            XmlDocPos workingPos = new XmlDocPos();
            workingPos.textContent = fromPos.textContent;
            workingPos.offset = fromPos.offset;

            while (true)
            {
                if (object.ReferenceEquals(workingPos.textContent, current)) // just started
                    ;
                else
                    if (current.NodeType == XmlNodeType.Text)
                    {
                        workingPos.textContent = (XmlText) current;
                        workingPos.offset = 0;
                    }

                int i = workingPos.textContent.Value.Substring(workingPos.offset).IndexOf("{TRN");
                if (i > -1)
                {
                    var pos = new XmlDocPos();
                    pos.textContent = workingPos.textContent;
                    pos.offset = i + workingPos.offset;
                    return pos;
                }
                

                // Any next sibling?
                if (current.NextSibling == null)
                    return null;

                if (current.NextSibling.Name == "em" || current.NextSibling.Name == "strong")
                {
                    var pos = new XmlDocPos();
                    pos.textContent = workingPos.textContent;
                    pos.offset = pos.textContent.Value.Length;
                    return pos;
                    
                }

                current = current.NextSibling;

            }

            //// Any next sibling?
            //if (current.NextSibling == null)
            //{
            //    return null;

            //    //// Are we a direct child of the para?
            //    //if (object.ReferenceEquals(current.ParentNode, para))
            //    //    return null; // nothing left to look for then

            //    //return SeekToSD(para, current.ParentNode);
            //}

            //// Is this a text node just before an 'em'?
            //if (current.NodeType == XmlNodeType.Text)
            //{


            //    if (current.NextSibling.Name == "em")
            //        return (XmlText) current;


            //}

            //return SeekToSD(para, current.NextSibling);


        }

        static void RemoveTRN(XmlNode para, XmlDocPos fromPos)
        {
            //var pos = new XmlDocPos();
            //pos.textContent = FindNextTextSiblingOrChild(para);
            //pos.offset = 0;
            do
            {
                var trnPos = SeekToTRN(para, fromPos);
                if (trnPos == null)
                    return;

                while (trnPos.offset > 0)
                    if (char.IsNumber(trnPos.textContent.Value[trnPos.offset - 1]))
                        trnPos.offset--;
                    else
                        break;

                var t = trnPos.textContent;
                int count = 0;
                do
                {
                    int i = t.Value.IndexOf("{/TRN");

                    if (i > -1)
                    {
                        trnPos.textContent.Value = trnPos.textContent.Value.Substring(0, trnPos.offset) +
                            t.Value.Substring(i + 6);

                        if (object.ReferenceEquals(trnPos.textContent, t))
                        {
                        }
                        else
                        {
                            t.ParentNode.RemoveChild(t);
                        }
                        break;
                    }

                    var n = t.NextSibling;
                    if (count > 0)
                        t.ParentNode.RemoveChild(t);

                    count++;

                    do
                    {
                        Debug.Assert(n != null);
                        if (n.NodeType == XmlNodeType.Text)
                        {
                            t = (XmlText)n;
                            break;
                        }
                        var n2 = n.NextSibling;
                        n.ParentNode.RemoveChild(n);
                        n = n2;
                    }
                    while (true);

                }
                while (true);

            }
            while (true);


        }

        static XmlDocPos SeekToTRN(XmlNode para, XmlDocPos fromPos)
        {
            XmlNode current = fromPos.textContent;
            XmlDocPos workingPos = new XmlDocPos();
            workingPos.textContent = fromPos.textContent;
            workingPos.offset = fromPos.offset;

            while (true)
            {
                if (object.ReferenceEquals(workingPos.textContent, current)) // just started
                    ;
                else
                    if (current.NodeType == XmlNodeType.Text)
                    {
                        workingPos.textContent = (XmlText)current;
                        workingPos.offset = 0;
                    }

                int i = workingPos.textContent.Value.Substring(workingPos.offset).IndexOf("{TRN");
                if (i > -1)
                {
                    var pos = new XmlDocPos();
                    pos.textContent = workingPos.textContent;
                    pos.offset = i + workingPos.offset;
                    return pos;
                }

                // Any next sibling?
                if (current.NextSibling == null)
                    return null;

                current = current.NextSibling;

            }

        }


        static XmlText FindNextTextSiblingOrChild(XmlNode node)
        {
            if (node == null)
                return null;
            if (node.NodeType == XmlNodeType.Text)
                return (XmlText)node;

            foreach (XmlNode child in node.ChildNodes)
            {
                var t = FindNextTextSiblingOrChild(child);
                if (t != null)
                    return t;
            }

            return FindNextTextSiblingOrChild(node.NextSibling);
        }

        static string GetSpeakerName(XmlNode para, XmlDocPos speechStartPos)
        {
            // Check for continuation name
            if (para.ChildNodes[0].NodeType == XmlNodeType.Text)
            {
                if (para.ChildNodes[0].InnerText.Length > 2)
                    if (para.ChildNodes[0].InnerText[0] == '[')
                    {
                        int n = para.ChildNodes[0].InnerText.IndexOf(']');
                        Debug.Assert(n > 1);
                        if (para.ChildNodes[0].InnerText.Length > n + 1)
                        {
                            string s = para.ChildNodes[0].InnerText.Substring(1, n - 1);
                            speechStartPos.textContent = (XmlText)(para.ChildNodes[0]);
                            speechStartPos.offset = n + 1;
                            return s;
                        }

                    }
            }


            // para is a speech if starts with speaker name.
            // Speaker name consists of one or more 'strong' nodes followed by a text node with '.'.
            // there may be additional material before the '.', e.g.:
            // <p><strong>Duke</strong> <strong>of</strong> <strong>Venice</strong> (<em>to Othello</em>). What, in your own part, can you say to this?</p>

            XmlText textNode = null;
            var sb = new StringBuilder();
            foreach (XmlNode node in para.ChildNodes)
            {
                if (IsTextNode(node))
                {
                    if (!IsEmptyTextNode(node, true))
                        return null;
                }
                else if (node.Name == "strong")
                {
                    Debug.Assert(node.ChildNodes.Count == 1);
                    Debug.Assert(IsTextNode(node.ChildNodes[0]));
                    textNode = (XmlText) (node.ChildNodes[0]);
                    sb.Append(textNode.InnerText);

                    // TODO - check if we need to deal with name abbreviations,
                    // e.g. "<p><strong>Oth.</strong> & <strong>Iago</strong>. Speak!</p>

                    if (textNode.InnerText.IndexOf(".") != -1) // sdff
                    {
                        // We're done ... so long as there's speech after the dot (see example below)
                        speechStartPos.textContent = FindNextTextSiblingOrChild(node.NextSibling);
                        speechStartPos.offset = 0;
                        if (speechStartPos.textContent != null)
                        {
                            
                            return sb.ToString();
                        }
                        return null;
                    }

                    bool buildingName = true;
                    var tempNode = node;
                    string separator = string.Empty;
                    while (true)
                    {
                        tempNode = tempNode.NextSibling;

                        if (tempNode == null)
                        {
                            // We found 'strong' without finding a sibling or strong-enclosed '.'
                            // Must be something like this:
                            // <p><strong>Brabantio</strong><em> appears above, at a window.</em></p>
                            // In other words, not a speech
                            return null;
                        }

                        
                        if (IsEmptyTextNode(tempNode, false))
                            continue;
                        if (IsTextNode(tempNode))
                        {
                            // whitespace?
                            textNode = (XmlText) tempNode;
                            if (textNode.InnerText.Trim().Length == 0)
                            {
                                sb.Append(textNode.InnerText);
                                continue;
                            }
                            // valid possible separator?
                            if (textNode.InnerText.Trim() == "," || textNode.InnerText.Trim() == "&")
                            {
                                separator = textNode.InnerText;
                            }
                            else
                            {
                                buildingName = false;
                                int dotpos = textNode.InnerText.Trim().IndexOf('.');
                                if (dotpos != -1)
                                {
                                    // finished building the name.

                                    // But it's still not a speech unless there's something after the '.'
                                    // Otherwise could be a s.d. like this:
                                    // <p><strong>Othello</strong>, <strong>Iago</strong>, <strong>&scaron;lechtici</strong>.</p>
                                    // (version of:
                                    // <p><em>Enter </em><strong>Othello</strong><em>, </em><strong>Iago</strong><em>, and </em><strong>Gentlemen</strong><em>.</em></p>

                                    if (textNode.InnerText.Trim().Length > (dotpos + 1))
                                    {
                                        speechStartPos.textContent = textNode;
                                        speechStartPos.offset = textNode.Value.IndexOf('.') + 1;
                                        return sb.ToString().Trim();
                                    }
                                    speechStartPos.textContent = FindNextTextSiblingOrChild(textNode.NextSibling);
                                    speechStartPos.offset = 0;
                                    if (speechStartPos.textContent != null)
                                        return sb.ToString().Trim();

                                    return null;
                                }
                            }
                        }

                        if (buildingName)
                        {
                            if (tempNode.Name == "strong")
                            {
                                Debug.Assert(tempNode.ChildNodes.Count == 1);
                                Debug.Assert(IsTextNode(tempNode.ChildNodes[0]));
                                textNode = (XmlText) (tempNode.ChildNodes[0]);

                                // put on any separator found between
                                sb.Append(separator);
                                separator = string.Empty;
                                sb.Append(textNode.InnerText);

                            }

                        }
                        else
                        {
                            //Debug.Assert(tempNode.Name != "strong");
                        }

                    } // wihle (looking for '.')

                } // if (found 'strong')
                else
                    return null;

            }

            return null;
        }

        static List<XmlNode> GetCellParas(XmlNode cell, int rowIndex)
        {
            var paras = new List<XmlNode>();

            foreach (XmlNode node in cell.ChildNodes)
            {
                if (node.Name == "p")
                    paras.Add(node);
                else if (!(IsTextNode(node)))
                {
                    throw new Exception("Bad cell at rowindex: " + rowIndex.ToString());

                }

            }

            if (paras.Count == 0)
                throw new Exception("Bad cell at rowindex: " + rowIndex.ToString());

            return paras;
        }

        static bool IsTextNode(XmlNode node)
        {
            return (node.NodeType == XmlNodeType.Text) ;
        }

        static XmlNode GetCellPara(XmlNode cell, int rowIndex)
        {
            var children = new List<XmlNode>();
            foreach (XmlNode c in cell.ChildNodes)
                if (!IsEmptyTextNode(c, true))
                    children.Add(c);
            if (children.Count != 1)
            {
                throw new Exception("Found cell with != 1 child node, rowindex: " + rowIndex.ToString());
            }
            //if (!(IsTextNode(cell.ChildNodes[0])))
            //    throw new Exception("Bad cell at rowindex: " + rowIndex.ToString());

            //if (!(IsTextNode(cell.ChildNodes[2])))
            //    throw new Exception("Bad cell at rowindex: " + rowIndex.ToString());

            if (children[0].Name != "p")
                throw new Exception("Bad cell at rowindex: " + rowIndex.ToString());

            //var textNode = (cell.ChildNodes[0]);

            //if (textNode.InnerText.Trim().Length > 0)
            //    throw new Exception("Bad cell at rowindex: " + rowIndex.ToString());

            //textNode = (cell.ChildNodes[2]);

            //if (textNode.InnerText.Trim().Length > 0)
            //    throw new Exception("Bad cell at rowindex: " + rowIndex.ToString());

            return children[0];
        }
    }
}
