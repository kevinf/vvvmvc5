﻿namespace Prism.Models
{
    public class UploadFilesResult
    {
        public string CorpusName { get; set; }
        public string Name { get; set; }
        public int Length { get; set; }
        public string Type { get; set; }
        public string UploadType { get; set; }
    }
}