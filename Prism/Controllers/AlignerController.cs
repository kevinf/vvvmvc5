/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using EblaAPI;
using Prism.Models;
using System.Xml;
using EblaImpl;

namespace Prism.Controllers
{
    [Authorize]
    public class AlignerController : PrismController
    {
        //==============================================
        //
        //  Page rendering
        //
        //==============================================

        public ActionResult Test()
        {
            return View();
        }

        public class VarInfo
        {
            public List<VersionVarInfo> VersionVarInfo {get;set;}
        }

        public class VersionVarInfo
        {
            public string VersionName { get; set; }
            public List<VersionSegmentVarInfo> SegmentVarInfo { get; set; }
        }

        public class VersionSegmentVarInfo
        {
            public int WordCount { get; set; }
            public double EddyVal { get; set; }
            public int BaseTextSegmentID { get; set; }
        }

        [HttpGet]
        public ActionResult SegStats(string CorpusName, int baseTextSegmentID, List<string> VariationVersionList)
        {

            ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

            /*
            IDocument doc = PrismHelpers.GetBaseText(CorpusName);
            var basetextsegs = doc.FindSegmentDefinitions(0, doc.Length(), false, false, null);

            Dictionary<string, VersionVarInfo> resultsMap = new Dictionary<string,VersionVarInfo>();

            foreach (var basetextseg in basetextsegs)
            {
                if (basetextseg.Length == 0)
                    continue;

                var stats = corpus.CalculateSegmentVariation(basetextseg.ID, VariationMetricTypes.metricA);

                foreach (var stat in stats.VersionSegmentVariations)
                {
                    VersionVarInfo vvi = null;
                    if (resultsMap.ContainsKey(stat.VersionName))
                        vvi = resultsMap[stat.VersionName];
                    else
                    {
                        vvi = new VersionVarInfo();
                        vvi.VersionName = stat.VersionName;
                        vvi.SegmentVarInfo = new List<VersionSegmentVarInfo>();
                        resultsMap.Add(stat.VersionName, vvi);
                    }

                    VersionSegmentVarInfo seginfo = new VersionSegmentVarInfo();
                    seginfo.EddyVal = stat.EddyValue;
                    seginfo.BaseTextSegmentID = basetextseg.ID;
                    int wordcount = 0;
                    foreach (var t in stat.TokenInfo)
                    {
                        wordcount += (int) t.AverageCount;
                    }
                    seginfo.WordCount = wordcount;
                    vvi.SegmentVarInfo.Add(seginfo);

                }
            }

            VarInfo results = new VarInfo();
            results.VersionVarInfo = new List<VersionVarInfo>();
            foreach (string s in resultsMap.Keys)
            {
                results.VersionVarInfo.Add(resultsMap[s]);
            }

            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(results.GetType());

            using (System.IO.StreamWriter myWriter = new System.IO.StreamWriter(@"C:\temp\varinfo.xml", false))
            {

                
                x.Serialize(myWriter, results);
                myWriter.Close();
            } 
            */


            var v = corpus.CalculateSegmentVariation(baseTextSegmentID, VariationMetricTypes.metricA, VariationVersionList);


            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append("<thead>");
            sb.Append("<tr><th>(word)</th>");

           
            foreach (var segvar in v.VersionSegmentVariations)
            {
                sb.Append("<th>");
                sb.Append(segvar.VersionName);
                sb.Append("</th>");

            }

            sb.Append("</tr>");
            sb.Append("</thead>");

            sb.Append("<tbody>");

            foreach (var t in v.TokenInfo)
            {
                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append(t.Token);
                sb.Append("</td>");

                foreach (var segvar in v.VersionSegmentVariations)
                {
                    sb.Append("<td>");
                    // Horribly slow, don't care. Just a demo.
                    bool found = false;
                    foreach (var t2 in segvar.TokenInfo)
                    {
                        if (string.Compare(t2.Token, t.Token) == 0)
                        {
                            found = true;
                            sb.Append(t2.AverageCount.ToString());
                            break;
                        }
                    }
                    if (!found)
                    {
                        sb.Append("0");
                    }
                    sb.Append("</td>");
                }

                sb.Append("</tr>");
            }




            sb.Append("</tbody>");



            ViewData["tablecontent"] = sb.ToString();
            ViewData["corpusname"] = CorpusName;
            ViewData["segid"] = baseTextSegmentID;
            return PartialView();
        }

        [HttpGet]
        public ActionResult SegStatsDownload(string CorpusName, int baseTextSegmentID, List<string> VariationVersionList)
        {

            ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);


            var v = corpus.CalculateSegmentVariation(baseTextSegmentID, VariationMetricTypes.metricA, VariationVersionList);

            var output = new System.IO.MemoryStream();
            var sw = new System.IO.StreamWriter(output, System.Text.Encoding.UTF8);
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            var writer = XmlWriter.Create(sw, settings); // new System.Xml.XmlTextWriter(output, System.Text.Encoding.UTF8);

            writer.WriteStartElement("Concordance");

            foreach (var segvar in v.VersionSegmentVariations)
            {
                writer.WriteStartElement("Version");
                writer.WriteAttributeString("name", segvar.VersionName);


                foreach (var t in segvar.TokenInfo)
                {
                    writer.WriteStartElement("TokenCount");
                    writer.WriteAttributeString("token", t.Token);
                    writer.WriteAttributeString("count", t.AverageCount.ToString());



                    writer.WriteEndElement();

                }

                writer.WriteEndElement();

            }


            writer.WriteEndElement();
            writer.Flush();


            //System.Text.StringBuilder sb = new System.Text.StringBuilder();

            //sb.Append("<thead>");
            //sb.Append("<tr><th>(word)</th>");


            //foreach (var segvar in v.VersionSegmentVariations)
            //{
            //    sb.Append("<th>");
            //    sb.Append(segvar.VersionName);
            //    sb.Append("</th>");

            //}

            //sb.Append("</tr>");
            //sb.Append("</thead>");

            //sb.Append("<tbody>");

            //foreach (var t in v.TokenInfo)
            //{
            //    sb.Append("<tr>");
            //    sb.Append("<td>");
            //    sb.Append(t.Token);
            //    sb.Append("</td>");

            //    foreach (var segvar in v.VersionSegmentVariations)
            //    {
            //        sb.Append("<td>");
            //        // Horribly slow, don't care. Just a demo.
            //        bool found = false;
            //        foreach (var t2 in segvar.TokenInfo)
            //        {
            //            if (string.Compare(t2.Token, t.Token) == 0)
            //            {
            //                found = true;
            //                sb.Append(t2.AverageCount.ToString());
            //                break;
            //            }
            //        }
            //        if (!found)
            //        {
            //            sb.Append("0");
            //        }
            //        sb.Append("</td>");
            //    }

            //    sb.Append("</tr>");
            //}




            //sb.Append("</tbody>");

            ////System.IO.StringWriter sw = new System.IO.StringWriter();
            //var sw = new System.IO.StreamWriter(output);
            //sw.WriteLine("This is a test");

            sw.Flush();
            output.Position = 0;

            //ViewData["tablecontent"] = sb.ToString();
            //ViewData["corpusname"] = CorpusName;
            //ViewData["segid"] = baseTextSegmentID;
            string fname = CorpusName + "-base-text-segment-" + baseTextSegmentID.ToString() + "-concordance.xml";
            return File(output, "application/xml", fname);
        }

        
        public ActionResult Index(string CorpusName, string VersionName, int? basetextSegIDToView, int? versionSegIDToView)
        {

            try
            {
                //ViewData["extract"] = string.Empty;
                ViewData["nextbasetextattribfilterrowindex"] = (0).ToString();
                ViewData["nextversionattribfilterrowindex"] = (0).ToString();

                if (!basetextSegIDToView.HasValue)
                    basetextSegIDToView = 0;
                if (!versionSegIDToView.HasValue)
                    versionSegIDToView = 0;
                ViewData["basetextSegIDToView"] = basetextSegIDToView.Value.ToString();
                ViewData["versionSegIDToView"] = versionSegIDToView.Value.ToString();
                
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

                //EblaAPI.PredefinedSegmentAttribute[] atts = corpus.GetPredefinedSegmentAttributes();
                //ViewData["colourdata"] = DocumentController.GetColouringData(atts);

                IDocument basetextDoc = PrismHelpers.GetDocument(CorpusName, null);
                IDocument versionDoc = PrismHelpers.GetDocument(CorpusName, VersionName);

                ViewData["BaseTextTOCSegments"] =  DocumentController.ConstructContentList( basetextDoc);
                ViewData["VersionTOCSegments"] = DocumentController.ConstructContentList(versionDoc);

                var model = new AlignModel();
                model.BaseTextModel = new DocumentExtractModel();
                model.VersionModel = new DocumentExtractModel();
                //model.StartPos = 0;
                //model.Length = 500;
                //model.docModel = new DocumentModel();
                model.VersionModel.NameIfVersion = VersionName;
                model.VersionModel.CorpusName = CorpusName;
                model.BaseTextModel.CorpusName = CorpusName;

                model.BaseTextModel.SegmentCount = basetextDoc.GetSegmentCount();
                model.VersionModel.SegmentCount = versionDoc.GetSegmentCount();

                //model.DocLength = doc.Length();
                DocumentMetadata meta = basetextDoc.GetMetadata();
                CorpusController.DocumentMetadataToModel(meta, model.BaseTextModel);
                meta = versionDoc.GetMetadata();
                CorpusController.DocumentMetadataToModel(meta, model.VersionModel);

                return View(model);

            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to open the document: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }


        }

        //==============================================
        //
        //  AJAX calls
        //
        //==============================================

        [HttpPost]
        public ActionResult GetAlignments(AlignModel model)
        {
            try
            {
                IAlignmentSet set = PrismHelpers.GetAlignmentSet(model.BaseTextModel.CorpusName, model.VersionModel.NameIfVersion);
                IDocument basetextDoc = PrismHelpers.GetBaseText(model.BaseTextModel.CorpusName);
                IDocument versionDoc = PrismHelpers.GetVersion(model.BaseTextModel.CorpusName, model.VersionModel.NameIfVersion);

                int basetextStartPos = 0;
                int basetextLength = basetextDoc.Length();

                int versionStartPos = 0;
                int versionLength = versionDoc.Length();

                if (model.BaseTextModel.ExtractSegmentID != -1)
                {
                    SegmentDefinition segdef = basetextDoc.GetSegmentDefinition(model.BaseTextModel.ExtractSegmentID);
                    basetextStartPos = segdef.StartPosition;
                    basetextLength = segdef.Length;
                }

                if (model.VersionModel.ExtractSegmentID != -1)
                {
                    SegmentDefinition segdef = versionDoc.GetSegmentDefinition(model.VersionModel.ExtractSegmentID);
                    versionStartPos = segdef.StartPosition;
                    versionLength = segdef.Length;
                }


                SegmentAlignment[] alignments = set.FindAlignments(DocumentController.EblaAttribFilterFromModel(model.BaseTextModel).ToArray(), DocumentController.EblaAttribFilterFromModel(model.VersionModel).ToArray(), basetextStartPos, basetextLength, versionStartPos, versionLength);

                GetAlignmentsResultModel result = new GetAlignmentsResultModel();
                result.Succeeded = true;
                result.Alignments = alignments;

                return MonoWorkaroundJson(result);
            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }
        }

        [HttpPost]
        public ActionResult AlignSegmentPair(AlignSegmentPairModel model)
        {
            try
            {
                // Called from the UI in response to the user selecting two segments and clicking 'align'.
                // Apart from segment IDs, an alignment has a type (normal or null), status and notes.
                // We're not using 'type' just now.
                // Alignments made by the user will always have a status of 'confirmed'.
                // (Alignments made by the auto-aligner will have a status of 'draft'.)
                // Notes will be added after the alignment has been made, in a separate operation.

                // In some cases, the user will be undertaking a one-to-many or many-to-one alignment.
                // That is, one or other segment will already be the subject of an alignment.
                // If neither is, we create a new alignment.
                // If either is, we check for validity (many-to-many is not supported), retrieve 
                // the existing alignment, and add the new ID.
                // If the pair of IDs turns out to be a duplicate (though the UI should ensure that's
                // not attempted), we'll just ignore the request.

                // Any pre-existing?
                IAlignmentSet set = PrismHelpers.GetAlignmentSet(model.CorpusName, model.VersionName);

                SegmentAlignment alignment = set.FindAlignment(model.BaseTextSegmentID, true);
                AlignSegmentPairResultModel result = new AlignSegmentPairResultModel();

                bool createNew = true;
                if (alignment != null)
                {
                    var ids = new HashSet<int>(alignment.SegmentIDsInVersion);
                    if (!ids.Contains(model.VersionSegmentID))
                    {
                        ids.Add(model.VersionSegmentID);
                        alignment.SegmentIDsInVersion = ids.ToArray();
                        set.UpdateAlignment(alignment);
                        createNew = false;
                        result.AlignmentID = alignment.ID;
                    }

                }
                else
                {
                    alignment = set.FindAlignment(model.VersionSegmentID, false);

                    if (alignment != null)
                    {
                        var ids = new HashSet<int>(alignment.SegmentIDsInBaseText);
                        if (!ids.Contains(model.BaseTextSegmentID))
                        {
                            ids.Add(model.BaseTextSegmentID);
                            alignment.SegmentIDsInBaseText = ids.ToArray();
                            set.UpdateAlignment(alignment);
                            createNew = false;
                            result.AlignmentID = alignment.ID;
                        }

                    }
                }

                result.isNew = false;
                if (createNew)
                {
                    SegmentAlignment a = new SegmentAlignment();
                    a.AlignmentStatus = AlignmentStatuses.confirmed;
                    a.AlignmentType = AlignmentTypes.normalAlign;
                    a.SegmentIDsInBaseText = new int[] { model.BaseTextSegmentID };
                    a.SegmentIDsInVersion = new int[] { model.VersionSegmentID };
                    result.AlignmentID = set.CreateAlignment(a);
                    result.isNew = true;

                }

                result.Succeeded = true;

                return MonoWorkaroundJson(result);

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }
        }

        [HttpPost]
        public ActionResult UnalignSegmentPair(AlignSegmentPairModel model)
        {
            try
            {
                IAlignmentSet set = PrismHelpers.GetAlignmentSet(model.CorpusName, model.VersionName);

                SegmentAlignment alignment = set.FindAlignment(model.BaseTextSegmentID, model.VersionSegmentID);

                if (alignment != null) // ui should ensure this is never the case, barring multi-user scenarios, perhaps
                {
                    if (alignment.SegmentIDsInBaseText.Length == 1 && alignment.SegmentIDsInVersion.Length == 1)
                    {
                        set.DeleteAlignment(alignment.ID);
                    }
                    else
                    {
                        if (alignment.SegmentIDsInBaseText.Length == 1)
                        {
                            var ids = new HashSet<int>(alignment.SegmentIDsInVersion);
                            ids.Remove(model.VersionSegmentID);
                            alignment.SegmentIDsInVersion = ids.ToArray();
                        }
                        else
                        {
                            var ids = new HashSet<int>(alignment.SegmentIDsInBaseText);
                            ids.Remove(model.BaseTextSegmentID);
                            alignment.SegmentIDsInBaseText = ids.ToArray();

                        }
                        set.UpdateAlignment(alignment);
                    }
                }
                return MonoWorkaroundJson(new ResultModel { Succeeded = true });

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        [HttpPost]
        public ActionResult AutoAlign(AutoAlignModel model)
        {
            try
            {
                IAlignmentSet set = PrismHelpers.GetAlignmentSet(model.BaseTextModel.CorpusName, model.VersionModel.NameIfVersion);
                IDocument basetextDoc = PrismHelpers.GetBaseText(model.BaseTextModel.CorpusName);
                IDocument versionDoc = PrismHelpers.GetVersion(model.BaseTextModel.CorpusName, model.VersionModel.NameIfVersion);

                int basetextStartPos = 0;
                int basetextLength = basetextDoc.Length();

                int versionStartPos = 0;
                int versionLength = versionDoc.Length();
                SegmentDefinition segdef = null;

                if (model.BaseTextModel.ExtractSegmentID != -1)
                {
                    segdef = basetextDoc.GetSegmentDefinition(model.BaseTextModel.ExtractSegmentID);
                    basetextStartPos = segdef.StartPosition;
                    basetextLength = segdef.Length;
                }

                if (model.VersionModel.ExtractSegmentID != -1)
                {
                    segdef = versionDoc.GetSegmentDefinition(model.VersionModel.ExtractSegmentID);
                    versionStartPos = segdef.StartPosition;
                    versionLength = segdef.Length;

                }

                int basetextEndPos = basetextStartPos + basetextLength;
                int versionEndPos = versionStartPos + versionLength;

                segdef = basetextDoc.GetSegmentDefinition(model.ConfirmedAlignmentBaseTextSegId);
                int lastBaseTextAlignEnd = segdef.StartPosition + segdef.Length;
                if (lastBaseTextAlignEnd > basetextStartPos)
                    basetextStartPos = lastBaseTextAlignEnd;

                segdef = versionDoc.GetSegmentDefinition(model.ConfirmedAlignmentVersionSegId);
                int lastVersionAlignEnd = segdef.StartPosition + segdef.Length;
                if (lastVersionAlignEnd > versionStartPos)
                    versionStartPos = lastVersionAlignEnd;

                int lastAlignedBaseTextSegID = -1;
                int lastAlignedVersionSegID = -1;

                int alignmentsCreated = 0;

                var cbFilter = new List<SegmentContentBehaviour>() { SegmentContentBehaviour.normal };

                while (true)
                {
                    var basetextSegDefs = new List<SegmentDefinition>(basetextDoc.FindSegmentDefinitions(lastBaseTextAlignEnd, basetextEndPos - lastBaseTextAlignEnd, false, false, DocumentController.EblaAttribFilterFromModel(model.BaseTextModel).ToArray(), cbFilter.ToArray()));
                    var versionSegDefs = new List<SegmentDefinition>(versionDoc.FindSegmentDefinitions(lastVersionAlignEnd, versionEndPos - lastVersionAlignEnd, false, false, DocumentController.EblaAttribFilterFromModel(model.VersionModel).ToArray(), cbFilter.ToArray()));

                    basetextSegDefs.Sort(new EblaImpl.SegmentDefinitionComparer());
                    versionSegDefs.Sort(new EblaImpl.SegmentDefinitionComparer());

                    int basetextSegIndex = 0;
                    int versionSegIndex = 0;
                    bool requery = false;
                    while (basetextSegIndex < basetextSegDefs.Count && versionSegIndex < versionSegDefs.Count)
                    {
                        var basetextSeg = basetextSegDefs[basetextSegIndex];
                        var versionSeg = versionSegDefs[versionSegIndex];

                        bool skip = false;
                        if (basetextSeg.StartPosition < lastBaseTextAlignEnd)
                        {
                            basetextSegIndex++;
                            skip = true;
                        }
                        if (versionSeg.StartPosition < lastVersionAlignEnd)
                        {
                            versionSegIndex++;
                            skip = true;
                        }

                        if (skip)
                            continue;

                        // Is either already aligned?
                        var a = set.FindAlignment(basetextSeg.ID, true);
                        if (a != null)
                        {
                            if (a.AlignmentStatus == AlignmentStatuses.confirmed)
                            {
                                lastBaseTextAlignEnd = basetextSeg.StartPosition + basetextSeg.Length;

                                foreach (int id in a.SegmentIDsInVersion)
                                {
                                    var seg = versionDoc.GetSegmentDefinition(id);
                                    if ((seg.StartPosition + seg.Length) > lastVersionAlignEnd)
                                        lastVersionAlignEnd = seg.StartPosition + seg.Length;

                                }
                                requery = true;
                                break;

                            }

                            set.DeleteAlignment(a.ID);
                        }
                        a = set.FindAlignment(versionSeg.ID, false);
                        if (a != null)
                        {
                            if (a.AlignmentStatus == AlignmentStatuses.confirmed)
                            {
                                lastVersionAlignEnd = versionSeg.StartPosition + versionSeg.Length;
                                foreach (int id in a.SegmentIDsInBaseText)
                                {
                                    var seg = basetextDoc.GetSegmentDefinition(id);
                                    if ((seg.StartPosition + seg.Length) > lastBaseTextAlignEnd)
                                        lastBaseTextAlignEnd = seg.StartPosition + seg.Length;

                                }

                                requery = true;
                                break;

                            }
                            set.DeleteAlignment(a.ID);

                        }

                        a = new SegmentAlignment();
                        a.AlignmentStatus = AlignmentStatuses.draft;
                        a.SegmentIDsInBaseText = new int[] { basetextSeg.ID };
                        a.SegmentIDsInVersion = new int[] { versionSeg.ID };

                        if (true)
                        {
                            string dbg;
                            if (lastAlignedBaseTextSegID > -1)
                            {
                                if (basetextSeg.ID != lastAlignedBaseTextSegID + 1)
                                    dbg = "kjf";
                                if (versionSeg.ID != lastAlignedVersionSegID + 1)
                                    dbg = "fkjf";
                            }

                            lastAlignedBaseTextSegID = basetextSeg.ID;
                            lastAlignedVersionSegID = versionSeg.ID;
                        }
                        
                        alignmentsCreated++;

                        set.CreateAlignment(a);
                        basetextSegIndex++;
                        versionSegIndex++;
                        lastBaseTextAlignEnd = basetextSeg.StartPosition + basetextSeg.Length;
                        lastVersionAlignEnd = versionSeg.StartPosition + versionSeg.Length;
                        
                    }

                    if (!requery)
                        break;
                }

                return MonoWorkaroundJson(new ResultModel { Succeeded = true});

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }
            
        }

        [HttpPost]
        public ActionResult RemoveAlignments(RemoveAlignmentsModel model)
        {
            try
            {
                IAlignmentSet set = PrismHelpers.GetAlignmentSet(model.BaseTextModel.CorpusName, model.VersionModel.NameIfVersion);
                IDocument basetextDoc = PrismHelpers.GetBaseText(model.BaseTextModel.CorpusName);
                IDocument versionDoc = PrismHelpers.GetVersion(model.BaseTextModel.CorpusName, model.VersionModel.NameIfVersion);

                int basetextStartPos = 0;
                int basetextLength = basetextDoc.Length();

                int versionStartPos = 0;
                int versionLength = versionDoc.Length();
                SegmentDefinition segdef = null;

                if (model.BaseTextModel.ExtractSegmentID != -1)
                {
                    segdef = basetextDoc.GetSegmentDefinition(model.BaseTextModel.ExtractSegmentID);
                    basetextStartPos = segdef.StartPosition;
                    basetextLength = segdef.Length;
                }

                if (model.VersionModel.ExtractSegmentID != -1)
                {
                    segdef = versionDoc.GetSegmentDefinition(model.VersionModel.ExtractSegmentID);
                    versionStartPos = segdef.StartPosition;
                    versionLength = segdef.Length;

                }


                set.DeleteAlignments(model.DraftOnly, DocumentController.EblaAttribFilterFromModel(model.BaseTextModel).ToArray(), DocumentController.EblaAttribFilterFromModel(model.VersionModel).ToArray(), basetextStartPos, basetextLength, versionStartPos, versionLength);

                return MonoWorkaroundJson(new ResultModel { Succeeded = true });

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        [HttpPost]
        public ActionResult ConfirmAlignments(AutoAlignModel model)
        {
            try
            {
                IAlignmentSet set = PrismHelpers.GetAlignmentSet(model.BaseTextModel.CorpusName, model.VersionModel.NameIfVersion);
                IDocument basetextDoc = PrismHelpers.GetBaseText(model.BaseTextModel.CorpusName);
                IDocument versionDoc = PrismHelpers.GetVersion(model.BaseTextModel.CorpusName, model.VersionModel.NameIfVersion);

                int basetextStartPos = 0;
                int basetextLength = basetextDoc.Length();

                int versionStartPos = 0;
                int versionLength = versionDoc.Length();
                SegmentDefinition segdef = null;

                if (model.BaseTextModel.ExtractSegmentID != -1)
                {
                    segdef = basetextDoc.GetSegmentDefinition(model.BaseTextModel.ExtractSegmentID);
                    basetextStartPos = segdef.StartPosition;
                    basetextLength = segdef.Length;
                }

                if (model.VersionModel.ExtractSegmentID != -1)
                {
                    segdef = versionDoc.GetSegmentDefinition(model.VersionModel.ExtractSegmentID);
                    versionStartPos = segdef.StartPosition;
                    versionLength = segdef.Length;

                }


                set.ConfirmAlignments(DocumentController.EblaAttribFilterFromModel(model.BaseTextModel).ToArray(), DocumentController.EblaAttribFilterFromModel(model.VersionModel).ToArray(), basetextStartPos, basetextLength, versionStartPos, versionLength);

                return MonoWorkaroundJson(new ResultModel { Succeeded = true });

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        private static void CreateMooreAlignments(List<BestAlignment> results, IAlignmentSet set)
        {
            var newAlignment = new SegmentAlignment {AlignmentStatus = AlignmentStatuses.draft};
            foreach (var bestAlignment in results)
            {
                for (var x = 0; x < bestAlignment.BaseSegmentList.Length + 1; x++)
                {
                    for (var y = 0; y < bestAlignment.VersionSegmentList.Length + 1; y++)
                    {
                        if (x >= bestAlignment.BaseSegmentList.Length || y >= bestAlignment.VersionSegmentList.Length)
                            continue;

                        newAlignment.SegmentIDsInBaseText = new int[] {bestAlignment.BaseSegmentList[x].Id};
                        newAlignment.SegmentIDsInVersion = new int[] {bestAlignment.VersionSegmentList[y].Id};
                        newAlignment.Scores = new float[] {bestAlignment.Score};

                        var existingAlignment = set.FindAlignment(bestAlignment.BaseSegmentList[x].Id,
                            bestAlignment.VersionSegmentList[y].Id);

                        if (existingAlignment == null)
                            set.CreateAlignment(newAlignment);
                    }
                }
            }
        }

        private static IDictionary<Guid, int> tasks = new Dictionary<Guid, int>();

        public ActionResult Start()
        {
            var taskId = Guid.NewGuid();
            tasks.Add(taskId, 0);

            Task.Factory.StartNew(() =>
            {
                for (var i = 0; i <= 100; i++)
                {
                    tasks[taskId] = i; // update task progress
                    Thread.Sleep(50); // simulate long running operation
                }
                tasks.Remove(taskId);
            });

            return Json(taskId);
        }

        public ActionResult Progress(Guid id)
        {
            return Json(tasks.Keys.Contains(id) ? tasks[id] : 100);
        }


        public ActionResult GetAlignerStatus(string taskId)
        {
            try
            {

                var isBusy = BackgroundAlignerManager.IsAlignerBusy(taskId);
                var status = BackgroundAlignerManager.GetAlignerStatus(taskId);

                return
                    MonoWorkaroundJson(new AlignerStatusResultModel
                    {
                        Succeeded = true,
                        Status = status,
                        Busy = isBusy
                    });

            }
            catch (Exception ex)
            {
                return
                    MonoWorkaroundJson(new ResultModel
                    {
                        Succeeded = false,
                        ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message)
                    });

            }
        }

        [HttpPost]
        public ActionResult StartMooreAligner(MooreAlignModel model)
        {
            try
            {
                BackgroundAlignerManager.StartAutoalignment(model.BaseTextModel.CorpusName,
                    model.VersionModel.NameIfVersion,
                    AccountController.SessionEblaUsername, AccountController.SessionEblaPassword);

                return MonoWorkaroundJson(new ResultModel {Succeeded = true});

            }
            catch (Exception ex)
            {
                return
                    MonoWorkaroundJson(new ResultModel
                    {
                        Succeeded = false,
                        ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message)
                    });

            }
        }

    }

    public class BackgroundAligner
    {
        private Thread _thread;
        private object _lock;
        private DateTime _timeStarted;
        private string _statusText;
        private ITranslationAligner _translationAligner;
        private IAlignmentSet _alignmentSet;
        private TranslationDocument _translationDocument;
        private volatile bool _shouldStop;

        string _corpusName;
        string _versionName;
        string _username;
        string _password;

        public void Start(string corpusName, string versionName,
            string username, string password)
        {
            _lock = new object();

            _translationAligner = PrismHelpers.GetTranslationAligner(corpusName, versionName);
            //_alignmentSet = PrismHelpers.GetAlignmentSet(corpusName, versionName);
            _translationDocument = _translationAligner.GetMooreTranslationDocument(corpusName,
                        versionName, username, password);

            _corpusName = corpusName;
            _versionName = versionName;
            _username = username;
            _password = password;
            SetStatusText("0% complete.");

            _thread = new Thread(DoWork) {Priority = ThreadPriority.BelowNormal};
            _timeStarted = DateTime.Now;
            _thread.Start();

        }

        public void DoWork()
        {
            while (true)
            {
                if (_shouldStop)
                {
                    SetStatusText("Cancelled.");
                    break;
                }

                try
                {
                    int progress = 0;

                    var moreToDo = _translationAligner.ContinueAutoAlignment(_translationDocument, _corpusName, _versionName, _username, _password, out progress);
                    if (!moreToDo)
                    {
                        // TODO - if internationalised, change Prism-Aligner.js accordingly
                        SetStatusText("Completed.");
                        break;

                    }
                    SetStatusText(progress.ToString() + "% complete.");
                }
                catch (Exception ex)
                {
                    SetStatusText("Aborted. Error message: " + ex.Message);
                    break;

                }

            }

        }       

        public void RequestStop()
        {
            _shouldStop = true;
        }

        public ThreadState Status()
        {
            return _thread.ThreadState;
        }

        public string StatusText()
        {
            lock (_lock)
                return _statusText;
        }

        private void SetStatusText(string s)
        {
            lock (_lock)
                _statusText = s;
        }

    }

    public class BackgroundAlignerManager : OpenableEblaItem
    {
        public BackgroundAlignerManager(NameValueCollection configProvider) : base(configProvider)
        {
        }

        private static ITranslationAligner TranslationAligner { get; set; }
        private static IAlignmentSet AlignmentSet { get; set; }
        private static object _lock = new object();

        public static bool IsAlignerBusy(string taskId)
        {
            lock (_lock)
            {
                if (!TaskList.ContainsKey(taskId)) return false;
                if (TaskList[taskId].Status() == ThreadState.Running)
                    return true;
                return false;
            }
        }

        public static void Cancel(string taskId)
        {
            lock (_lock)
            {
                if (TaskList.ContainsKey(taskId))
                {
                    TaskList[taskId].RequestStop();
                }
            }
        }

        public static string GetAlignerStatus(string taskId)
        {
            lock (_lock)
            {
                if (TaskList.ContainsKey(taskId))
                {
                    return TaskList[taskId].StatusText();
                }

                return "none.";
            }
        }

        public static void StartAutoalignment(string corpusName, string versionName,
            string username, string password)
        {
            lock (_lock)
            {
                if (IsAlignerBusy(corpusName))
                    throw new Exception("Moore auto-alignment is already in progress.");

                var c = new BackgroundAligner();

                c.Start(corpusName, versionName, username, password);
                if (TaskList.ContainsKey(corpusName))
                    TaskList.Remove(corpusName);
                TaskList.Add(corpusName, c);
            }

        }

        private static Dictionary<string, BackgroundAligner> TaskList { get; } =
            new Dictionary<string, BackgroundAligner>();
    }

}
