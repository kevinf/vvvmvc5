﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using EblaAPI;
using EblaImpl;

namespace Prism.Controllers
{


    [HandleError]
    public class HomeController : PrismController
    {

        //==============================================
        //
        //  Page rendering
        //
        //==============================================

        public ActionResult Index()
        {


            return View();
        }

        //[Authorize]
        //public ActionResult CorpusStorePage()
        //{
        //    ICorpusStore store = EblaFactory.GetCorpusStore(ConfigurationManager.AppSettings);

        //    store.Open(AccountController.SessionEblaUsername, AccountController.SessionEblaPassword);

        //    return View(store.GetCorpusList());

        //}

        public ActionResult Credits()
        {
            return View();
        }

		public ActionResult Project()
        {
            if (PrismHelpers.IsThirdParty)
                return View("Project3P");
            return View();
        }

		public ActionResult Platform()
        {
            return View();
        }

		public ActionResult Feedback()
		{
			return View();
		}

        public ActionResult Error(string message)
        {
            ViewData["message"] = message;

            return View();
        }

        public ActionResult SetLanguage(string langCode, string returnUrl)
        {

            var langCookie = new HttpCookie("lang", langCode) { HttpOnly = true };
            langCookie.Expires = DateTime.Now.AddYears(1);
            Response.AppendCookie(langCookie);

            if (!String.IsNullOrEmpty(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
    }
}
