﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using EblaAPI;
using EblaImpl;
using System.Configuration;
using System.Data.Common;
using Prism.Models;
using System.IO;

namespace Prism.Controllers
{
    [Authorize]
    public class CorpusStoreController : PrismController
    {
  

        //==============================================
        //
        //  Page rendering
        //
        //==============================================

        public ActionResult Index()
        {
            
            
            try
            {

                return View(GetModel());

            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to open the corpus store: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        public ActionResult UsersAdmin()
        {
            try
            {


                UserInfo[] ui = GetCorpusStore().GetUserInfo(null, null);

                return View(ui);

            }
            catch (Exception ex)
            {
                string message = PrismResources.An_error_occurred + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        public ActionResult UserAdmin(string Username)
        {
            try
            {

                UserInfo[] ui = GetCorpusStore().GetUserInfo(Username, null);

                UserRightsModel u = new UserRightsModel();
                u.CanAdmin = ui[0].IsAdmin;
                u.Username = Username;
                u.Email = ui[0].Email;

                var corporaMap = new Dictionary<string, UserCorpusRightsModel>();
                string[] corpusList = GetCorpusStore().GetCorpusList();

                foreach (string corpus in corpusList)
                    corporaMap.Add(corpus, new UserCorpusRightsModel() {CorpusName=corpus});

                foreach (var r in ui[0].CorpusRights)
                {
                    corporaMap[r.CorpusName].SetRightsValue(r.CanRead, r.CanWrite);
                }

                var rights = new List<UserCorpusRightsModel>();
                foreach (string corpus in corporaMap.Keys)
                    rights.Add(corporaMap[corpus]);

                u.CorpusRights = rights.ToArray();

                return View(u);

            }
            catch (Exception ex)
            {
                string message = PrismResources.An_error_occurred + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }

        }

        [HttpPost]
        public ActionResult UserAdmin(UserRightsModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(model);

                GetCorpusStore().UpdateUser(model.Username, model.CanAdmin, model.Email);

                foreach (var r in model.CorpusRights)
                {
                    GetCorpusStore().SetUserCorpusRights(r.CorpusName, model.Username, r.CanRead(), r.CanWrite());
                }
                return RedirectToAction("UsersAdmin", "CorpusStore");


            }
            catch (Exception ex)
            {
                string message = PrismResources.An_error_occurred + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }

        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            try
            {
                //GetCorpusStore().ChangePassword(model.NewPassword);
                AccountController.ChangePassword(model.NewPassword);
                return View("ChangePasswordSuccess");
            }
            catch (Exception ex)
            {
                string message = PrismResources.An_error_occurred + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        //public ActionResult ChangePasswordSuccess()
        //{
        //    return View();
        //}

        public PartialViewResult ConfirmDeletion()
        {
            return PartialView();
        }

        public ActionResult Delete(string corpusName)
        {
            try
            {
                GetCorpusStore().DeleteCorpus(corpusName);

                AddSuccessAlert(string.Format("The corpus ({0}) was deleted.", corpusName));

                //return RedirectToAction("Index");
                //return Content(bool.TrueString);
                return View("Index", GetModel());
            }
            catch (Exception ex)
            {

                AddDangerAlert(string.Format("An error occurred: {0}", ex.Message));

                return View("Index", GetModel());
                //var message = "An error occurred: " + ex.Message;
                //return RedirectToAction("Error", "Home", new { message = message });

                //return Content(bool.FalseString);
            }
        }

        public ActionResult DeleteUser(string Username)
        {
            try
            {
                GetCorpusStore().DeleteUser(Username);
                UserInfo[] ui = GetCorpusStore().GetUserInfo(null, null);

                
                return View("UsersAdmin", ui);

            }
            catch (Exception ex)
            {
                string message = PrismResources.An_error_occurred + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        public ActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateUser(NewUserModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    GetCorpusStore().CreateUser(model.UserName, model.Password, model.CanAdmin, model.Email);

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);

                    return View(model);
                }

            }
            else
            {
                return View(model);

            }
            return RedirectToAction("UsersAdmin", "CorpusStore");
        }

        public ActionResult Create()
        {
            CorpusController.SetupLangCodes(ViewData, PrismHelpers.GetCorpusStore().GetCultures());
            return View();
        }

        [HttpPost]
        public ActionResult Create(NewCorpusModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    GetCorpusStore().CreateCorpus(model.CorpusName, model.Description, model.LanguageCode);

                    AddSuccessAlert("The corpus was created successfully.");
                }
                catch (Exception ex)
                {
                    AddDangerAlert(string.Format("An error occurred: {0}", ex.Message));

                    return View(model);
                }
            }
            else
            {
                return View(model);
            }
            return RedirectToAction("Index", "Corpus", new { CorpusName = model.CorpusName });
        }

        [HttpPost]
        public JsonResult CreateCorpusWizard(string CorpusName, string Description, string SelectedLanguageCode)
        {
            if (string.IsNullOrEmpty(CorpusName))
            {
                ModelState.AddModelError("CorpusName", "Corpus name is required.");
            }

            if (ModelState.IsValid)
            {

                try
                {
                    GetCorpusStore().CreateCorpus(CorpusName, Description, SelectedLanguageCode);
                }
                catch (Exception ex)
                {
                    var errorMessage = string.Format("An error occurred: {0}", ex.Message);
                    return
                        Json(new ResultModel
                        {
                            Succeeded = false,
                            ErrorMsg =
                                System.Net.WebUtility.HtmlEncode(errorMessage)
                        });
                }
            }
            else
            {
                return
                    Json(new ResultModel
                    {
                        Succeeded = false,
                        ErrorMsg =
                            System.Net.WebUtility.HtmlEncode("Invalid corpus.")
                    });

            }

            return
                Json(new ResultModel
                {
                    Succeeded = true,
                    ErrorMsg = "The corpus was created successfully."
                });
        }

        [HttpGet]
        public ActionResult CorpusWizard2()
        {
            var model = new UploadFilesResult();
            return View(model);
        }

        [HttpGet]
        public ActionResult CorpusWizard()
        {
            var languageId = string.Empty;
            var corpusName = string.Empty;
            var description = string.Empty;
            var model = GetCorpusWizardAndPartialViewModel(languageId, corpusName, description);
            return View(model);
        }

        private CorpusWizardModel GetCorpusWizardAndPartialViewModel(string languageId, string corpusName,
            string description)
        {

            var model = new CorpusWizardModel {CorpusCreation = new CorpusWizardCreateModel()};

            var corpusStore = PrismHelpers.GetCorpusStore();
            if (corpusStore == null)
            {
                throw new Exception("Error while getting 'Corpus Store'");
            }

            var languages = CorpusController.SetupLangCodes(corpusStore.GetCultures());
            model.CorpusCreation.LanguageCode = languages;
            model.CorpusCreation.SelectedLanguageCode = string.IsNullOrEmpty(languageId)
                ? model.CorpusCreation.LanguageCode?[0].Value
                : languageId;
            model.CorpusCreation.Description = description;
            model.CorpusCreation.CorpusName = corpusName;
            model.BaseUploadFilesResult = new UploadFilesResult();
            model.VersionUploadFilesResult = new UploadFilesResult();

            model.LanguageCode = CorpusController.SetupLangCodes(PrismHelpers.GetCorpusStore().GetCultures());
            model.SelectedLanguageCode = string.IsNullOrEmpty(languageId)
                ? model.LanguageCode?[0].Value
                : languageId;

            model.AlignmentAlgorithm = AlignmentAlgorithms.GaleAndChurch;
            model.SegmentationType = SegmentationTypes.Paragraph;

            return model;
        }

        [HttpGet]
        public ActionResult CorpusWizardCreate(string languageId, string corpusName, string description)
        {
            var model = GetCorpusWizardAndPartialViewModel(languageId, corpusName, description);
            return PartialView("_CorpusWizardCreate", model.CorpusCreation);
        }

        public PartialViewResult CorpusWizardUploadBase()
        {
            try
            {
                var model = new UploadFilesResult();
                return PartialView(model);
            }
            catch (Exception ex)
            {
                var message = "An error occurred while trying to open UploadFilesResult : " + ex.Message;
                throw new Exception(message);
            }
        }

        public PartialViewResult CorpusWizardUploadVersion()
        {
            try
            {
                var model = new UploadFilesResult();
                return PartialView(model);
            }
            catch (Exception ex)
            {
                var message = "An error occurred while trying to open UploadFilesResult : " + ex.Message;
                throw new Exception(message);
            }
        }

        [HttpPost]
        public ContentResult UploadFiles()
        {
            var r = new List<UploadFilesResult>();

            var uploadType = Request.Form["uploadType"];

            foreach (string file in Request.Files)
            {
                var hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf == null || hpf.ContentLength == 0)
                    continue;

                if (hpf.FileName != null)
                {
                    var savedFileName = Path.Combine(Server.MapPath("~/App_Data"),
                        Path.GetFileName(hpf.FileName));
                    hpf.SaveAs(savedFileName);

                    r.Add(new UploadFilesResult()
                    {
                        Name = hpf.FileName,
                        Length = hpf.ContentLength,
                        Type = hpf.ContentType,
                        UploadType = uploadType
                    });
                }
            }
            return
                Content(
                    "{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" +
                    string.Format("{0} bytes", r[0].Length) + "\",\"uploadtype\":\"" + r[0].UploadType  + 
                    "\"}", "application/json");
        }

        //==============================================
        //
        //  AJAX calls
        //
        //==============================================


        //==============================================
        //
        //  Implementation
        //
        //==============================================


        private ICorpusStore _corpusStore = null;

        private ICorpusStore GetCorpusStore()
        {
            // TODO - consider storing in Session

            if (_corpusStore != null)
                return _corpusStore;

            ICorpusStore store = EblaFactory.GetCorpusStore(ConfigurationManager.AppSettings);

            store.Open(AccountController.SessionEblaUsername, AccountController.SessionEblaPassword);

            _corpusStore = store;

            return _corpusStore;
        }

        private CorpusStoreModel GetModel()
        {
            CorpusStoreModel model = new CorpusStoreModel();
            string[] corpusList = GetCorpusStore().GetCorpusList();
            List<CorpusModel> l = new List<CorpusModel>();

            var rights = GetCorpusStore().GetUserInfo(AccountController.SessionEblaUsername, null);


            foreach (string s in corpusList)
            {
                CorpusModel m = new CorpusModel();
                m.CorpusName = s;
                l.Add(m);
                bool openCorpus = false;
                if (AccountController.SessionEblaIsAdmin)
                    openCorpus = true;
                else
                    foreach (var r in rights[0].CorpusRights)
                    {
                        if (string.Compare(r.CorpusName, s) == 0)
                        {
                            // TODO -wrap this logic somewhere else
                            if (r.CanRead || r.CanWrite)
                            {
                                openCorpus = true;

                            }
                            break;
                        }
                    }

                if (openCorpus)
                {
                    ICorpus c = PrismHelpers.GetCorpus(s);
                    m.CorpusDescription = c.GetDescription();

                }
            }

            model.CorpusList = l.ToArray();
            return model;
        }


    }
}
