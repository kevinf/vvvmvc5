﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EblaAPI;
using EblaImpl;
using Prism.Models;

namespace Prism.Controllers
{
    [Authorize]
    public class VisualiseController : PrismController
    {

        [HttpGet]
        public ActionResult FindSegments(string CorpusName)
        {
            var m = new FindSegmentModel();
            var items = new List<SelectListItem>();

            var versions = CorpusController.GetActiveCorpusSelection();

            items.Add(new SelectListItem() { Text = "[all]", Value = string.Empty });
            foreach (var v in versions)
                items.Add(new SelectListItem() { Text = v, Value = v });

            ViewData["versionlist"] = items;
            var basetext = PrismHelpers.GetBaseText(CorpusName);
            ViewData["TOCSegments"] = DocumentController.ConstructContentList(basetext);
            return PartialView("FindSegmentDialog", m);
        }

        bool WithinLimits(double floor, double ceiling, double value)
        {
            if (floor >= 0)
                if (value < floor)
                    return false;
            if (ceiling >= 0)
                if (value > ceiling)
                    return false;
            return true;
        }

        [HttpPost]
        public ActionResult GetFindSegmentsData(string CorpusName, string SearchText, bool SearchBaseText, bool SearchVersion, string VersionName,
            double VivFloor, double VivCeiling, double EddyFloor, double EddyCeiling, int metricTypeVal, AttributeModel[] AttributeFilters, int WithinSegmentID, SegmentContentBehaviour ContentBehaviour)
        {
            try
            {
                if (!Enum.IsDefined(typeof(VariationMetricTypes), metricTypeVal))
                    throw new Exception(PrismResources.Unrecognised_metric_type_value + metricTypeVal.ToString());

                VariationMetricTypes metricType = (VariationMetricTypes)metricTypeVal;

                if (SearchBaseText && SearchVersion)
                    throw new Exception("UI error - SearchBaseText && SearchVersion is true");
                SearchText = SearchText.Trim();
                //if (SearchText.Length < 3)
                //    throw new Exception("Search text must be at least three characters");

                //var corpus = PrismHelpers.GetCorpus(CorpusName);
                var baseText = PrismHelpers.GetBaseText(CorpusName);
                var corpus = PrismHelpers.GetCorpus(CorpusName);

                int startPosForSearch = 0;
                int lengthForSearch = baseText.Length();
                if (WithinSegmentID != -1)
                {
                    var withinSeg = baseText.GetSegmentDefinition(WithinSegmentID);
                    if (withinSeg == null)
                        throw new Exception("Unable to retrieve 'within' segment with ID " +WithinSegmentID.ToString() );

                    startPosForSearch = withinSeg.StartPosition;
                    lengthForSearch = withinSeg.Length;
                }


                var sb = new System.Text.StringBuilder();

                var versionDocs = new Dictionary<string, IDocument>();
                var alsets = new Dictionary<string, IAlignmentSet>();
                var alignmentMaps = new Dictionary<string, Dictionary<int, SegmentAlignment>>();
                var versions = CorpusController.GetActiveCorpusSelection();
                MultipleSegmentVariationData data = null;
                HashSet<int> segIdsWithinLimits = null;
                // TODO - optimise this by adding arg to RetrieveMultipleSegmentVariationData that specifies the segs
                // required, then perform other filtering first to obtain that arg, particular w.r.t. WithinSegmentID?
                if (VivFloor >= 0 || VivCeiling >= 0 || EddyFloor >= 0 || EddyCeiling >= 0)
                {
                    if (VivFloor >= 0 && VivCeiling >= 0)
                        if (VivFloor > VivCeiling)
                            throw new Exception("Floor value must not be greater than ceiling value");
                    if (EddyFloor >= 0 && EddyCeiling >= 0)
                        if (EddyFloor > EddyCeiling)
                            throw new Exception("Floor value must not be greater than ceiling value");

                    data = corpus.RetrieveMultipleSegmentVariationData(null, null, metricType, versions, false);

                    segIdsWithinLimits = new HashSet<int>();

                    int versionIndexStart = 0;
                    int versionIndexEnd = data.VersionNames.Length - 1;
                    if (false)//!string.IsNullOrEmpty(VersionName))
                    {
                        versionIndexStart = new List<string>(data.VersionNames).IndexOf(VersionName);
                        versionIndexEnd = versionIndexStart;
                    }

                    for (int i = 0; i < data.BaseTextSegmentIDs.Length; i++)
                    {
                        if (!WithinLimits(VivFloor, VivCeiling, data.VivValues[i]))
                            continue;

                        if (EddyFloor >= 0 || EddyCeiling >= 0)
                        {
                            bool skip = true;
                            for (int vindex = versionIndexStart; vindex <= versionIndexEnd; vindex++)
                                if (WithinLimits(EddyFloor, EddyCeiling, data.EddyValues[vindex][i]))
                                {
                                    skip = false;
                                    break;
                                }
                            if (skip)
                                continue;
                        }
                        segIdsWithinLimits.Add(data.BaseTextSegmentIDs[i]);

                    } // for (all base text segs)

                } // if (variation criteria set)

                if (SearchVersion)
                {
                    if (!string.IsNullOrEmpty(VersionName))
                    {
                        //versionDocs.Add(PrismHelpers.GetVersion(CorpusName, VersionName));
                        //alsets.Add(PrismHelpers.GetAlignmentSet(CorpusName, VersionName));
                        versions.Clear();
                        versions.Add(VersionName);
                    }
                    //else
                    GetVersionsData(baseText, versions, CorpusName, versionDocs, alsets, alignmentMaps);

                    //{
                        
                    //    foreach (var v in versions)
                    //    {
                    //        var d = PrismHelpers.GetVersion(CorpusName, v);
                    //        versionDocs.Add(d);
                    //        var alset = PrismHelpers.GetAlignmentSet(CorpusName, v);
                    //        alsets.Add(alset);
                    //        var l = new List<SegmentAlignment>();
                    //        l.AddRange(alset.FindAlignments(null, null, 0, baseText.Length(), 0, d.Length()));
                    //        var m = new Dictionary<int, SegmentAlignment>();
                    //        foreach (var al in l)
                    //        {
                    //            if (al.SegmentIDsInBaseText.Length == 1)
                    //            {
                    //                m.Add(al.SegmentIDsInBaseText[0], al);
                    //            }
                    //        }
                    //        alignmentMaps.Add(m);
                    //    }
                    //}
                }

                var attribFilter = DocumentController.EblaAttribsFromModel(AttributeFilters);
                var cbFilter = new List<SegmentContentBehaviour>() { ContentBehaviour };
                var segs = baseText.FindSegmentDefinitions(startPosForSearch, lengthForSearch, false, false, attribFilter.ToArray(), cbFilter.ToArray());
                foreach (var seg in segs)
                {
                    if (seg.Length < 1 || seg.Length > 3000)
                        continue;

                    if (segIdsWithinLimits != null)
                        if (!segIdsWithinLimits.Contains(seg.ID))
                            continue;

                    bool includeSegment = true;
                    var segText = baseText.GetDocumentContentText(seg.StartPosition, seg.Length);

                    if (!string.IsNullOrEmpty(SearchText))
                    {
                        if (SearchBaseText)
                        {
                            includeSegment &= segText.Contains(SearchText);
                        }
                        else
                        {
                            //if (versionDocs.Count == 1)
                            //{
                            //    segText = GetVersionContent(seg.ID, alsets[0], versionDocs[0]);
                            //    includeSegment &= (segText != null && segText.Contains(SearchText));
                                    
                            //}
                            //else
                            {
                                bool foundIt = false;
                                for (int vindex = 0; vindex < versions.Count; vindex++)
                                {
                                    if (!alignmentMaps[versions[vindex]].ContainsKey(seg.ID))
                                        continue;
                                    string s = GetVersionContent(alignmentMaps[versions[vindex]][seg.ID], versionDocs[versions[vindex]]);
                                    if (s != null && s.Contains(SearchText))
                                    {
                                        if (versionDocs.Count > 1)
                                            segText = "[" + versions[vindex] + "] ";
                                        else
                                            segText = string.Empty;
                                        segText += s;
                                        foundIt = true;
                                        break;
                                    }
                                }
                                includeSegment &= foundIt;
                            }
                        }


                    } // if (searching for text)


                    if (includeSegment)
                    {
                        if (segText.Length > 100)
                            segText = segText.Substring(0, 49) + " [...] " + segText.Substring(segText.Length - 49);

                        sb.Append("<tr>");
                        sb.Append("<td>");
                        sb.Append("<input type='checkbox' class='findsegcheckbox' id='seg-" + seg.ID.ToString() + "' name='seg-" + seg.ID.ToString() + "' value='" + seg.ID.ToString() + "' />");
                        sb.Append("</td>");
                        sb.Append("<td>");
                        sb.Append(seg.Length.ToString());
                        sb.Append("</td>");
                        sb.Append("<td>");
                        sb.Append(HttpUtility.HtmlEncode(segText));
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }

                if (sb.Length == 0)
                {
                    sb.Append("No results found.");
                }
                else
                {
                    sb.Insert(0, "<table><tr><th><input type='checkbox' id='findsegcheckall'/></th><th>Length</th><th>Text</th></tr>");
                    sb.Append("</table>");
                }

                var results = new FindSegmentResultsModel();
                results.Succeeded = true;
                results.TableHtml = sb.ToString();
                return MonoWorkaroundJson(results);


            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }
        }

        void GetVersionsData(IDocument baseText, List<string> versions, string CorpusName, Dictionary<string, IDocument> versionDocs, Dictionary<string, IAlignmentSet> alsets, Dictionary<string, Dictionary<int, SegmentAlignment>> alignmentMaps)
        {
            foreach (var v in versions)
            {
                var d = PrismHelpers.GetVersion(CorpusName, v);
                versionDocs.Add(v, d);
                var alset = PrismHelpers.GetAlignmentSet(CorpusName, v);
                alsets.Add(v, alset);
                var l = new List<SegmentAlignment>();
                l.AddRange(alset.FindAlignments(null, null, 0, baseText.Length(), 0, d.Length()));
                var m = new Dictionary<int, SegmentAlignment>();
                foreach (var al in l)
                {
                    if (al.SegmentIDsInBaseText.Length == 1)
                    {
                        // 1-to-n should usually be represented by a single alignment with 
                        // SegmentIDsInVersion longer than 1, but cater for representation
                        // as two separate alignments since that's how the Moore aligner 
                        // implementation is currently doing it.

                        // This check also deals with n-to-1 alignments, but only by 
                        // skipping all but the first part.
                        if (!m.ContainsKey(al.SegmentIDsInBaseText[0]))
                            m.Add(al.SegmentIDsInBaseText[0], al);
                    }
                }
                alignmentMaps.Add(v, m);
            }
        }
        
        string QuoteCSVValue(string s)
        {
            return "\"" + (s == null ? string.Empty : s.Trim()) + "\"";
        }

        void ArraysToCSV(System.IO.StreamWriter sw, List<List<string>> data)
        {
            for (int i = 0; i < data.Count; i++)
            {
                var row = data[i];
                for (int j = 0; j < row.Count; j++)
                {
                    sw.Write(QuoteCSVValue(row[j]));
                    if (j < (row.Count - 1))
                        sw.Write(",");
                }
                if (i < (data.Count - 1))
                    sw.Write(Environment.NewLine);
            }
        }

        [HttpPost]
        public ActionResult ExportVersionsTable(string CorpusName, int metricTypeVal, string AttribName)
        {
            try
            {
                var results = DoGetSegmentVersionsData(CorpusName, metricTypeVal, AttribName, false);


                var output = new System.IO.MemoryStream();

                var sw = new System.IO.StreamWriter(output, System.Text.Encoding.UTF8);

                var rows = new List<List<string>>();
                var row = new List<string>();

                row.Add("Viv");

                if (!string.IsNullOrEmpty(AttribName))
                    row.Add(AttribName);

                //row.Add("Base text");

                for (int i = 0; i < results.colModel.Length; i++)
                {
                    var s = results.colModel[i].title;
                    if (!string.IsNullOrEmpty(results.colModel[i].refdate))
                        s += " (" + results.colModel[i].refdate + ")";
                    row.Add(s);
                    if (i > 0) // no eddy for base text
                        row.Add("eddy");
                }

                rows.Add(row);

                for (int i = 0; i < results.Rows.Length; i++)
                {
                    row = new List<string>();
                    row.Add(results.Rows[i].VivValue.ToString());
                    if (!string.IsNullOrEmpty(AttribName))
                        row.Add(results.Rows[i].AttribValue);

                    row.Add(results.Rows[i].BaseTextContent);
                    for (int cell = 0; cell < results.Rows[i].Cells.Length; cell++)
                    {
                        row.Add(results.Rows[i].Cells[cell].VersionContent);
                        row.Add(results.Rows[i].Cells[cell].EddyValue.ToString());
                    }
                    rows.Add(row);
                }

                ArraysToCSV(sw, rows);
                sw.Flush();

                output.Position = 0;

                string fname = "VVV-table";
                //fname = SanitiseFilename(fname);
                fname += ".csv";
                return File(output, "text/csv", fname);
            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to export the document: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }
        }

        SegmentVersionsResultModel DoGetSegmentVersionsData(string CorpusName, int metricTypeVal, string AttribName, bool getSVDs)
        {
            if (!Enum.IsDefined(typeof(VariationMetricTypes), metricTypeVal))
                throw new Exception(PrismResources.Unrecognised_metric_type_value + metricTypeVal.ToString());

            VariationMetricTypes metricType = (VariationMetricTypes)metricTypeVal;

            var ids = CorpusController.GetSegmentSelectionList();
            var baseText = PrismHelpers.GetBaseText(CorpusName);
            var corpus = PrismHelpers.GetCorpus(CorpusName);

            var versions = CorpusController.GetActiveCorpusSelection();

            var versionDocs = new Dictionary<string, IDocument>();
            var alsets = new Dictionary<string, IAlignmentSet>();
            var alignmentMaps = new Dictionary<string, Dictionary<int, SegmentAlignment>>();

            GetVersionsData(baseText, versions, CorpusName, versionDocs, alsets, alignmentMaps);

            //var alsets = new Dictionary<string, IAlignmentSet>();
            //foreach (var v in versions)
            //    alsets.Add(v, PrismHelpers.GetAlignmentSet(CorpusName, v));

            //var versionDocs = new Dictionary<string, IDocument>();
            //foreach (var v in versions)
            //    versionDocs.Add(v, PrismHelpers.GetVersion(CorpusName, v));

            //var versionsSort = new SortedDictionary<float, string>();
            var metadataList = new List<DocumentMetadata>();
            //foreach (var v in versions)
            for (int i = 0; i < versions.Count; i++)
            {
                var md = versionDocs[versions[i]].GetMetadata();
                metadataList.Add(md);
                //float key = 0;
                //if (md.ReferenceDate.HasValue)
                //    key = md.ReferenceDate.Value;
                //while (versionsSort.ContainsKey(key))
                //    key += 0.001f;

                //versionsSort.Add(key, v);
            }

            metadataList.Sort((a, b) =>
                a.ReferenceDate.HasValue ?
                    (b.ReferenceDate.HasValue ?
                        a.ReferenceDate.Value.CompareTo(b.ReferenceDate.Value) :
                        1) :
                    (b.ReferenceDate.HasValue ? -1 : 0));
            versions.Clear();
            foreach (var md in metadataList)
                versions.Add(md.NameIfVersion);

            //versions = new List<string>(versionsSort.Values);

            var data = corpus.RetrieveMultipleSegmentVariationData(null, ids, metricType, versions, getSVDs);

            var results = new SegmentVersionsResultModel();
            results.VersionNames = data.VersionNames;
            results.AttribName = AttribName;
            //var colNames = new List<string>();
            //colNames.Add("Base text");
            //colNames.AddRange(versions);
            //results.colNames = colNames.ToArray();
            var colModel = new List<ColumnModel>();
            int count = 0;
            //colModel.Add(new ColumnModel() { title = "Viv", name = "viv" });
            var md2 = baseText.GetMetadata();
            colModel.Add(new ColumnModel() { title = "Base text", name = "bt", refdate = md2.ReferenceDate.HasValue ? md2.ReferenceDate.Value.ToString() : string.Empty });
            //foreach (var v in versions)
            for (int i = 0; i < versions.Count; i++)
            {
                md2 = versionDocs[versions[i]].GetMetadata();
                colModel.Add(new ColumnModel() { title = versions[i], name = "v" + count.ToString(), refdate = md2.ReferenceDate.HasValue ? md2.ReferenceDate.Value.ToString() : string.Empty });
                count++;
            }
            results.colModel = colModel.ToArray();

            var rows = new List<RowModel>();

            var versionToEddyIndex = new Dictionary<string, int>();
            var tempList = new List<string>(data.VersionNames);
            foreach (var v in versions)
                versionToEddyIndex.Add(v, tempList.IndexOf(v));

            for (int i = 0; i < data.BaseTextSegmentIDs.Length; i++)
            {
                var id = data.BaseTextSegmentIDs[i];

                var cells = new List<RowCellModel>();
                var seg = baseText.GetSegmentDefinition(id);
                //if (seg == null)
                //    // request for a segment that doesn't exist
                //    continue;
                string basetextContent = baseText.GetDocumentContentText(seg.StartPosition, seg.Length);
                var row = new RowModel();
                row.BaseTextContent = basetextContent;
                row.BaseTextSegmentID = id;
                row.VivValue = data.VivValues[i];
                if (getSVDs)
                    row.SVDPts = data.SVDs[i].ToArray();
                if (!string.IsNullOrEmpty(AttribName))
                {

                    var a = seg.Attributes.FirstOrDefault(att => string.Compare(att.Name, AttribName) == 0);
                    if (a != null)
                        row.AttribValue = a.Value;
                }
                //foreach (var v in versions)
                for (int j = 0; j < versions.Count; j++)
                {

                    var eddyValues = data.EddyValues[versionToEddyIndex[versions[j]]];
                    string versionContent = string.Empty;

                    int[] segidsinversion = null;
                    if (alignmentMaps[versions[j]].ContainsKey(id))
                    {
                        segidsinversion = alignmentMaps[versions[j]][seg.ID].SegmentIDsInVersion;
                        string s = GetVersionContent(alignmentMaps[versions[j]][seg.ID], versionDocs[versions[j]]);
                        if (s != null)
                            versionContent = s;
                    }
                    //string versionContent = GetVersionContent(id, alsets[j], versionDocs[j]);
                    var cell = new RowCellModel();
                    cell.EddyValue = -1;
                    cell.SegmentIDsInVersion = new List<int>().ToArray();
                    if (versionContent != null)
                    {
                        cell.EddyValue = eddyValues[i];
                        cell.SegmentIDsInVersion = segidsinversion;
                    }
                    else
                        versionContent = string.Empty;

                    //var al = alsets[v].FindAlignment(id, true);

                    //if (al != null)
                    //{
                    //    var versionDoc = versionDocs[v];
                    //    var sb = new System.Text.StringBuilder();
                    //    foreach (int id2 in al.SegmentIDsInVersion)
                    //    {
                    //        if (sb.Length > 0)
                    //            sb.Append(" ");
                    //        var d = versionDoc.GetSegmentDefinition(id2);

                    //        if (d.Length > 0)
                    //            sb.Append(versionDoc.GetDocumentContentText(d.StartPosition, d.Length));
                    //    }

                    //    versionContent = sb.ToString();
                    //    cell.EddyValue = eddyValues[i];
                    //}

                    cell.VersionContent = versionContent;
                    cells.Add(cell);
                }
                row.Cells = cells.ToArray();
                rows.Add(row);
            }
            results.Rows = rows.ToArray();

            results.Succeeded = true;
            return results;
        }
        
        [HttpPost]
        public ActionResult GetSegmentVersionsData(string CorpusName, int metricTypeVal, string AttribName, bool getSVDs)
        {
            try
            {
                var results = DoGetSegmentVersionsData(CorpusName, metricTypeVal, AttribName, getSVDs);

                //if (!Enum.IsDefined(typeof(VariationMetricTypes), metricTypeVal))
                //    throw new Exception("Unrecognised metric type value: " + metricTypeVal.ToString());

                //VariationMetricTypes metricType = (VariationMetricTypes)metricTypeVal;

                //var ids = CorpusController.GetSegmentSelectionList();
                //var baseText = PrismHelpers.GetBaseText(CorpusName);
                //var corpus = PrismHelpers.GetCorpus(CorpusName);

                //var versions = CorpusController.GetActiveCorpusSelection();

                //List<IDocument> versionDocs = new List<IDocument>();
                //List<IAlignmentSet> alsets = new List<IAlignmentSet>();
                //var alignmentMaps = new List<Dictionary<int, SegmentAlignment>>();

                //GetVersionsData(baseText, versions, CorpusName, versionDocs, alsets, alignmentMaps);

                ////var alsets = new Dictionary<string, IAlignmentSet>();
                ////foreach (var v in versions)
                ////    alsets.Add(v, PrismHelpers.GetAlignmentSet(CorpusName, v));

                ////var versionDocs = new Dictionary<string, IDocument>();
                ////foreach (var v in versions)
                ////    versionDocs.Add(v, PrismHelpers.GetVersion(CorpusName, v));

                ////var versionsSort = new SortedDictionary<float, string>();
                //var metadataList = new List<DocumentMetadata>();
                ////foreach (var v in versions)
                //    for (int i = 0; i < versions.Count; i++) 
                //{
                //    var md = versionDocs[i].GetMetadata();
                //    metadataList.Add(md);
                //    //float key = 0;
                //    //if (md.ReferenceDate.HasValue)
                //    //    key = md.ReferenceDate.Value;
                //    //while (versionsSort.ContainsKey(key))
                //    //    key += 0.001f;
                   
                //    //versionsSort.Add(key, v);
                //}

                //metadataList.Sort((a, b) =>
                //    a.ReferenceDate.HasValue ?
                //        (b.ReferenceDate.HasValue ?
                //            a.ReferenceDate.Value.CompareTo(b.ReferenceDate.Value) :
                //            1) :
                //        (b.ReferenceDate.HasValue ? -1 : 0));
                //versions.Clear();
                //foreach (var md in metadataList)
                //    versions.Add(md.NameIfVersion);

                ////versions = new List<string>(versionsSort.Values);

                //var data = corpus.RetrieveMultipleSegmentVariationData(null, ids, metricType, versions, getSVDs);

                //var results = new SegmentVersionsResultModel();
                //results.VersionNames = data.VersionNames;
                //results.AttribName = AttribName;
                ////var colNames = new List<string>();
                ////colNames.Add("Base text");
                ////colNames.AddRange(versions);
                ////results.colNames = colNames.ToArray();
                //var colModel = new List<ColumnModel>();
                //int count = 0;
                ////colModel.Add(new ColumnModel() { title = "Viv", name = "viv" });
                //var md2 = baseText.GetMetadata();
                //colModel.Add(new ColumnModel() {title = "Base text", name = "bt", refdate = md2.ReferenceDate.HasValue ? md2.ReferenceDate.Value.ToString() : string.Empty });
                ////foreach (var v in versions)
                //    for (int i = 0; i < versions.Count; i++) 
                //{
                //    md2 = versionDocs[i].GetMetadata();
                //    colModel.Add(new ColumnModel() { title = versions[i], name = "v" + count.ToString(), refdate = md2.ReferenceDate.HasValue ? md2.ReferenceDate.Value.ToString() : string.Empty });
                //    count++;
                //}
                //results.colModel = colModel.ToArray();

                //var rows = new List<RowModel>();

                //var versionToEddyIndex = new Dictionary<string, int>();
                //var tempList = new List<string>(data.VersionNames);
                //foreach (var v in versions)
                //    versionToEddyIndex.Add(v, tempList.IndexOf(v));

                //for (int i = 0; i < data.BaseTextSegmentIDs.Length; i++ )
                //{
                //    var id = data.BaseTextSegmentIDs[i];
                    
                //    var cells = new List<RowCellModel>();
                //    var seg = baseText.GetSegmentDefinition(id);
                //    //if (seg == null)
                //    //    // request for a segment that doesn't exist
                //    //    continue;
                //    string basetextContent = baseText.GetDocumentContentText(seg.StartPosition, seg.Length);
                //    var row = new RowModel();
                //    row.BaseTextContent = basetextContent;
                //    row.BaseTextSegmentID = id;
                //    row.VivValue = data.VivValues[i];
                //    if (getSVDs)
                //        row.SVDPts = data.SVDs[i].ToArray();
                //    if (!string.IsNullOrEmpty(AttribName))
                //    {
                        
                //        var a = seg.Attributes.FirstOrDefault(att => string.Compare(att.Name, AttribName) == 0);
                //        if (a != null)
                //            row.AttribValue = a.Value;
                //    }
                //    //foreach (var v in versions)
                //        for (int j = 0; j < versions.Count; j++) 
                //    {

                //        var eddyValues = data.EddyValues[versionToEddyIndex[versions[j]]];
                //        string versionContent = string.Empty;

                //        if (alignmentMaps[j].ContainsKey(id))
                //        {
                //            string s = GetVersionContent(alignmentMaps[j][seg.ID], versionDocs[j]);
                //            if (s != null)
                //                versionContent = s;
                //        }
                //        //string versionContent = GetVersionContent(id, alsets[j], versionDocs[j]);
                //        var cell = new RowCellModel();
                //        cell.EddyValue = -1;

                //        if (versionContent != null)
                //            cell.EddyValue = eddyValues[i];
                //        else
                //            versionContent = string.Empty;

                //        //var al = alsets[v].FindAlignment(id, true);
                                                
                //        //if (al != null)
                //        //{
                //        //    var versionDoc = versionDocs[v];
                //        //    var sb = new System.Text.StringBuilder();
                //        //    foreach (int id2 in al.SegmentIDsInVersion)
                //        //    {
                //        //        if (sb.Length > 0)
                //        //            sb.Append(" ");
                //        //        var d = versionDoc.GetSegmentDefinition(id2);

                //        //        if (d.Length > 0)
                //        //            sb.Append(versionDoc.GetDocumentContentText(d.StartPosition, d.Length));
                //        //    }

                //        //    versionContent = sb.ToString();
                //        //    cell.EddyValue = eddyValues[i];
                //        //}
                      
                //        cell.VersionContent = versionContent;
                //        cells.Add(cell);
                //    }
                //    row.Cells = cells.ToArray();
                //    rows.Add(row);
                //}
                //results.Rows = rows.ToArray();

                //results.Succeeded = true;
                return MonoWorkaroundJson(results);

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }
        }

        string GetVersionContent(SegmentAlignment al, IDocument versionDoc)
        {
            string versionContent = null;
            
            if (al != null)
            {
                  var sb = new System.Text.StringBuilder();
                foreach (int id2 in al.SegmentIDsInVersion)
                {
                    if (sb.Length > 0)
                        sb.Append(" ");
                    var d = versionDoc.GetSegmentDefinition(id2);

                    if (d.Length > 0)
                        sb.Append(versionDoc.GetDocumentContentText(d.StartPosition, d.Length));
                }

                versionContent = sb.ToString();
            }
            return versionContent;
        }

        string GetVersionContent(int baseTextSegId, IAlignmentSet alset, IDocument versionDoc)
        {
            
            var al = alset.FindAlignment(baseTextSegId, true);
            return GetVersionContent(al, versionDoc);
        }

        [HttpPost]
        public ActionResult SegmentVersionsTable(SegmentVersionsTableModel model)
        {
            try
            {
                CorpusController.SetSegmentSelectionList(new List<int>());
                return SegmentVersionsTable(model.CorpusName);
            }
            catch (Exception ex)
            {
                string message = PrismResources.An_error_occurred + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }
        }

        public ActionResult SegmentVersionsTable(string CorpusName)
        {
            try
            {
                //var ids = CorpusController.GetSegmentSelectionList();
                //var baseText = PrismHelpers.GetBaseText(CorpusName);

                //var versions = CorpusController.GetActiveCorpusSelection();

                //var alsets = new Dictionary<string, IAlignmentSet>();
                //foreach (var v in versions)
                //    alsets.Add(v, PrismHelpers.GetAlignmentSet(CorpusName, v));

                //var versionDocs = new Dictionary<string, IDocument>();
                //foreach (var v in versions)
                //    versionDocs.Add(v, PrismHelpers.GetVersion(CorpusName, v));

               

                var model = new SegmentVersionsTableModel();
                var corpus = PrismHelpers.GetCorpus(CorpusName);
                var attribs = corpus.GetPredefinedSegmentAttributes(false);
                var attribNames = new SortedDictionary<string, string>();
                foreach (var a in attribs)
                    attribNames.Add(a.Name, a.Name);
                attribNames.Add( "[none]", string.Empty);
                model.AttribNames = attribNames;

                //model.BaseText = baseText;
                //model.Versions  = versionDocs;
                //model.Alignments = alsets;
                //model.SegmentIDs = ids;
                return View(model);
            }
            catch (Exception ex)
            {
                string message = PrismResources.An_error_occurred + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }

        }

        public ActionResult Index(string CorpusName, string NameIfVersion)
        {
            try
            {
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

                // Get the colouring defined for certain segment types depending on their attributes.
                // We'll use this primarily for example purposes - it's more likely on this screen
                // that colour will be used to represent Viv/Eddy values

                //EblaAPI.PredefinedSegmentAttribute[] atts = corpus.GetPredefinedSegmentAttributes();
                //ViewData["colourdata"] = DocumentController.GetColouringData(atts);
                ViewData["corpusname"] = CorpusName;
                ViewData["corpusdescription"] = corpus.GetDescription();

                IDocument basetextDoc = PrismHelpers.GetDocument(CorpusName, null);
                IDocument versionDoc = PrismHelpers.GetDocument(CorpusName, NameIfVersion);

                // For now, just get the whole of each document, with markup for all segments.
                // Note: first 'true' flag requests markup be inserted to add '[' and ']' markers.
                // Second 'true' flag requests markup be inserted around segment text, to make it selectable by ID etc.
                // for colouring purposes.
                // First flag could be set 'false' if markers aren't needed.
                var metricTypes = new List<VariationMetricTypes>();
                metricTypes.Add(VariationMetricTypes.metricA);
                metricTypes.Add(VariationMetricTypes.metricB);
                metricTypes.Add(VariationMetricTypes.metricC);
                metricTypes.Add(VariationMetricTypes.metricD);
                metricTypes.Add(VariationMetricTypes.metricE);
                ViewData["basetextcontent"] = basetextDoc.GetDocumentContent(0, basetextDoc.Length(), false, true, null, false, null, null, metricTypes);
				ViewData["basetextdate"] = basetextDoc.GetMetadata().ReferenceDate;
                ViewData["versioncontent"] = versionDoc.GetDocumentContent(0, versionDoc.Length(), false, true, null, false, null, null, metricTypes);
				ViewData["versionname"] = NameIfVersion;
				ViewData["versiondate"] = versionDoc.GetMetadata().ReferenceDate;

                return View();

            }
            catch (Exception ex)
            {
                string message = PrismResources.An_error_occurred + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }
        }

        public static System.Globalization.CultureInfo FromISOName(string name)
        {
            return System.Globalization.CultureInfo
                .GetCultures(System.Globalization.CultureTypes.NeutralCultures)
                .FirstOrDefault(c => c.ThreeLetterISOLanguageName == name);
        }

		//
        public ActionResult Viv(string CorpusName, int? vivFloor, int? vivCeiling)
        {
            try
            {
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

                // Get the colouring defined for certain segment types depending on their attributes.
                // We'll use this primarily for example purposes - it's more likely on this screen
                // that colour will be used to represent Viv/Eddy values

                //EblaAPI.PredefinedSegmentAttribute[] atts = corpus.GetPredefinedSegmentAttributes();
                //ViewData["colourdata"] = DocumentController.GetColouringData(atts);
                ViewData["corpusname"] = CorpusName;
                ViewData["corpusdescription"] = corpus.GetDescription();


                int vivFloorValue = 0;
                int vivCeilingValue = 0;

                // Use some hard-coded defaults for now
                vivFloorValue = 50;
                vivCeilingValue = 85;

                if (vivFloor.HasValue)
                    vivFloorValue = vivFloor.Value;
                if (vivCeiling.HasValue)
                    vivCeilingValue = vivCeiling.Value;

                ViewData["vivfloor"] = vivFloorValue.ToString();
                ViewData["vivceiling"] = vivCeilingValue.ToString();

                IDocument basetextDoc = PrismHelpers.GetDocument(CorpusName, null);

                var ci = FromISOName(basetextDoc.GetMetadata().LanguageCode);

                ViewData["basetextlangcode"] = ci == null ? string.Empty : ci.TwoLetterISOLanguageName;

                ViewData["versionlangcode"] = string.Empty;
                // What about versions? For now, just use the lang code of the first one

                var versions = corpus.GetVersionList();
                if (versions.Length > 0)
                {
                    var version = PrismHelpers.GetVersion(CorpusName, versions[0]);
                    ViewData["versionlangcode"] = FromISOName(version.GetMetadata().LanguageCode).TwoLetterISOLanguageName;
                }

                var metricTypes = new List<VariationMetricTypes>();
                metricTypes.Add(VariationMetricTypes.metricA);
                metricTypes.Add(VariationMetricTypes.metricB);
                metricTypes.Add(VariationMetricTypes.metricC);
                metricTypes.Add(VariationMetricTypes.metricD);
                metricTypes.Add(VariationMetricTypes.metricE);

                // For now, just get the whole of each document, with markup for all segments.
                // Note: first 'true' flag requests markup be inserted to add '[' and ']' markers.
                // Second 'true' flag requests markup be inserted around segment text, to make it selectable by ID etc.
                // for colouring purposes.
                // First flag could be set 'false' if markers aren't needed.
                var attribsToEmit = new List<string>() { "cb" };
                ViewData["basetextcontent"] = basetextDoc.GetDocumentContent(0, basetextDoc.Length(), false, true, null, false, attribsToEmit.ToArray(), CorpusController.GetActiveCorpusSelection().ToArray(), metricTypes);

                return View();

            }
            catch (Exception ex)
            {
                string message = PrismResources.An_error_occurred + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }
		}

        public ActionResult Alignments(string CorpusName)
        {
            try
            {
                var model = CorpusController.GetModel(CorpusName);

                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);
				String versionList = "<select><option value='' >Select</option>";

                foreach (var v in model.VersionList)
                {
                    string nameWithDate = v.NameIfVersion;
                    if (v.ReferenceDate.HasValue)
                        nameWithDate += " (" + v.ReferenceDate.Value.ToString() + ")";
                    versionList += "<option value='" + v.NameIfVersion + "'>" + nameWithDate + "</option>";
                }

                //foreach ( String nameWithDate in corpus.GetVersionListWithDates() )
                //{
                //    versionList += "<option>" + nameWithDate + "</option>";
                //}
				versionList += "</select>";

                // Get the colouring defined for certain segment types depending on their attributes.
                // We'll use this primarily for example purposes - it's more likely on this screen
                // that colour will be used to represent Viv/Eddy values

                //EblaAPI.PredefinedSegmentAttribute[] atts = corpus.GetPredefinedSegmentAttributes();
                //ViewData["colourdata"] = DocumentController.GetColouringData(atts);
                ViewData["corpusname"] = CorpusName;
				ViewData["corpusdescription"] = corpus.GetDescription();
				ViewData["versionlist"] = versionList;

                IDocument basetextDoc = PrismHelpers.GetDocument(CorpusName, null);
                ViewData["basetextcontent"] = basetextDoc.GetDocumentContent(0, basetextDoc.Length(), false, true, null, false, null, null, null);
				ViewData["basetextdate"] = basetextDoc.GetMetadata().ReferenceDate;

                return View();

            }
            catch (Exception ex)
            {
                string message = PrismResources.An_error_occurred + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }
		}

		public ActionResult OthelloMap(string CorpusName)
		{
			try
			{
				ViewData["corpusname"] = CorpusName;
				return View();	
			}
			catch (Exception ex)
			{
				string message = PrismResources.An_error_occurred + ex.Message;
				return RedirectToAction("Error", "Home", new { message = message });
			}
		}

        //==============================================
        //
        //  AJAX calls
        //
        //==============================================

        [HttpGet]
        public ActionResult VivTest(string CorpusName, int SegmentID, List<string> VariationVersionList)
        {
            ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

            var v = corpus.CalculateSegmentVariation(SegmentID, VariationMetricTypes.metricA, VariationVersionList);

			return PartialView("VivReportDialog", v);
        }

        [HttpPost]
        public ActionResult GraphData(string CorpusName, VariationMetricTypes metricType, int? BaseTextSegmentID, List<string> VariationVersionList)
        {
            try
            {
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

                var graphModelMap = new Dictionary<string, GraphModel>();

                if (BaseTextSegmentID.HasValue)
                {

                    var v = corpus.CalculateSegmentVariation(BaseTextSegmentID.Value, metricType, VariationVersionList);

                    foreach (var versionstats in v.VersionSegmentVariations)
                    {
                        GraphModel graphModel = new GraphModel();
                        graphModel.VersionName = versionstats.VersionName;
                        graphModel.EddyValue = versionstats.EddyValue;
                        graphModelMap.Add(graphModel.VersionName, graphModel);
                    }
                }
                else
                {
                    string[] versions = corpus.GetVersionList();

                    foreach (string s in versions)
                    {
                        GraphModel graphModel = new GraphModel();
                        graphModel.VersionName = s;
                        graphModel.EddyValue = corpus.RetrieveAverageVariation(s, metricType);
                        graphModelMap.Add(graphModel.VersionName, graphModel);

                    }
                }

                foreach (string s in graphModelMap.Keys)
                {
                    IDocument doc = PrismHelpers.GetDocument(CorpusName, s);
                    var m = doc.GetMetadata();

                    graphModelMap[s].ReferenceDate = m.ReferenceDate;
                    graphModelMap[s].Genre = m.Genre;
                }

                GraphResultModel result = new GraphResultModel();
                result.Succeeded = true;

                result.GraphModels = graphModelMap.Values.ToArray();

                return MonoWorkaroundJson(result);

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }
        }

    }
}
