$(document).ready(function() {
    // fix sub nav on scroll

    var $win = $(window);
    var $nav = $('.subnav');
    /*  navTop = $('.subnav').length && $('.subnav').offset().top - 40,
        isFixed = 0
    
       
       // hack sad times - holdover until rewrite for 2.1
       $nav.on('click', function () {
         if (!isFixed) setTimeout(function () {  $win.scrollTop($win.scrollTop() - 47) }, 10);
         else setTimeout(function () {  $win.scrollTop($win.scrollTop() -100) }, 10);
       })
       //$win.on('scroll', processScroll) 
   
       function processScroll() {
      
         var i, scrollTop = $win.scrollTop()
         if (scrollTop >= navTop && !isFixed) {
           isFixed = 1; 
           $nav.addClass('subnav-fixed')
         } else if (scrollTop <= navTop && isFixed) {
           isFixed = 0;
           $nav.removeClass('subnav-fixed')
         }
       }*/
    $nav.on('click',
        function() {
            setTimeout(function() { $win.scrollTop($win.scrollTop() - 130) }, 10);
        });

    $('.bs-docs-sidenav > li > a').on('click',
        function(event) {
            event.preventDefault();
            //$( '.bs-docs-sidenav > li' ).removeClass( 'active' );

            var $ct = $(event.currentTarget),
                $target = $($ct.attr('href'));

            //$ct.parent().addClass( 'active' );

            //$win.scrollTop( $clicked.offset().top - 117 );
            $('html, body').animate({
                scrollTop: $target.offset().top - 117
            });
        });

    $('.tt').tooltip();
    $('.po').popover();

    $('.carousel').carousel();


    var opt = {
        html: true,
        trigger: 'manual',
        placement: 'bottom',
        content: $('#help-content').html()
    };

    $('#help-popover').popover(opt);

    $('#help-popover').click(function(event) {
        event.preventDefault();
        $(event.target).popover('toggle');
    });

    // initially refresh scroll spys because of dynamically loaded contents
    // using 2 sec delay
    setTimeout(function() {
            $('[data-spy="scroll"]').each(function() {
                var $spy = $(this).scrollspy('refresh');
            });
        },
        2000);
    // refresh on resize
    $win.resize(function(event) {
        $('[data-spy="scroll"]').each(function() {
            var $spy = $(this).scrollspy('refresh');
        });
    });

    /*$('.navbar li').click(function(event) {
	    
	    $(this).parent().children('li').removeClass('active');
	    $(this).addClass('active');
	});*/

});

