﻿

var PrismNS = PrismNS || {};

PrismNS.EddyOverviewChart = {};

PrismNS.EddyOverviewChart.plot = null;

$(function () {

    EblaNS.FillVariationTypeSelectElm('#variation-type-select');

    var $vs = $('#variation-type-select');

    //    _.each(EblaNS.VariationMetricTypes, function (type, value) {
    //        $vs.append('<option value="' + type.value + '">' + type.name + '</option>');
    //    });

    // switch viv type show on select
    $vs.change(function (event) {
        //PrismNS.EddyOverviewChart.ShowChart();
        PrismNS.EddyOverviewChart.GetChartData(false);

    });

    $('#legend-sort-select').change(function () {

        PrismNS.EddyOverviewChart.SortData();
        PrismNS.EddyOverviewChart.ShowChart(null, null, null, null);
    });


    PrismNS.EddyOverviewChart.GetChartData(true);

    $('#zoomout').click(function (e) { PrismNS.EddyOverviewChart.ShowChart(null, null, null, null); });

    $('#checkall').click(function (e) { PrismNS.EddyOverviewChart.SetSeriesSelected(true); });

    $('#checknone').click(function (e) { PrismNS.EddyOverviewChart.SetSeriesSelected(false); });

});

var previousPoint = null;
var previousPointDisplayTime = null;

////var chartSeriesNames = new Array();
////var chartSeries = new Array();
////var chartSeriesToggle = new Array();

PrismNS.EddyOverviewChart.ChartSeriesInfo = function (label, name, date, averageeddy, series, state) {
//    this.name = name;
//    this.date = date;
//    this.averageeddy = averageeddy;
//    this.series = series;
//    this.state = state;

    return {label: label, name: name, date: date, averageeddy: averageeddy, series: series, state: state };
}


PrismNS.EddyOverviewChart.ChartSeriesInfoArray = new Array();
PrismNS.EddyOverviewChart.ChartSeriesInfoMap = new Array();

PrismNS.EddyOverviewChart.SetSeriesSelected = function (selected) {
    for (var i = 0; i < PrismNS.EddyOverviewChart.ChartSeriesInfoArray.length; i++) {

        PrismNS.EddyOverviewChart.ChartSeriesInfoArray[i].state = selected;
    }
    PrismNS.EddyOverviewChart.ShowChart(null, null, null, null);
}

PrismNS.EddyOverviewChart.GetChartData = function (firsttime) {
    var corpusname = $('#corpusname').val();

    var $vs = $('#variation-type-select');

    //alert($vs.val());

    var url = _siteUrl + "Charts/GetEddyOverviewChartData";
    var args = { CorpusName: corpusname, metricTypeVal: parseInt($vs.val()) };


    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                var selectedseriesnames = new Array();
                if (!firsttime) {
                    for (var i = 0; i < PrismNS.EddyOverviewChart.ChartSeriesInfoArray.length; i++) {
                        if (PrismNS.EddyOverviewChart.ChartSeriesInfoArray[i].state)
                            selectedseriesnames.push(PrismNS.EddyOverviewChart.ChartSeriesInfoArray[i].name);
                    }
                }
                PrismNS.EddyOverviewChart.PrepareChartData(result);
                if (!firsttime) {
                    for (var i = 0; i < PrismNS.EddyOverviewChart.ChartSeriesInfoArray.length; i++) {
                        var nameFound = PrismNS.EddyOverviewChart.ChartSeriesInfoArray[i].name;
                        for (var j = 0; j < selectedseriesnames.length; j++)
                            if (selectedseriesnames[j] == nameFound) {
                                PrismNS.EddyOverviewChart.ChartSeriesInfoArray[i].state = true;
                                break;
                            }
                    }
                    //                    for (var i = 0; i < selectedseriesnames.length; i++)
                    //                        PrismNS.EddyOverviewChart.ChartSeriesInfoMap[selectedseriesnames[i]].state = true;
                }
                PrismNS.EddyOverviewChart.ShowChart(null, null, null, null);
                return;

            }
            alert(PrismNS.Strings.Retrieving_chart_data_failed + ': ' + result.ErrorMsg);

        }

    });

}

PrismNS.EddyOverviewChart.SortData = function () {

    var sortorder = $("select#legend-sort-select").val();

    switch (sortorder) {
        case 'eddy':
            PrismNS.EddyOverviewChart.ChartSeriesInfoArray.sort(PrismNS.EddyOverviewChart.EddyInfoSorter);
            break;
        case 'name':
            PrismNS.EddyOverviewChart.ChartSeriesInfoArray.sort(PrismNS.EddyOverviewChart.NameInfoSorter);
            break;
        case 'year':
            PrismNS.EddyOverviewChart.ChartSeriesInfoArray.sort(PrismNS.EddyOverviewChart.DateInfoSorter);
            break;
    }
}

PrismNS.EddyOverviewChart.EddyInfoSorter = function (a, b) {
    if (a.averageeddy < b.averageeddy)
        return -1;
    if (b.averageeddy < a.averageeddy)
        return 1;
    return 0;
}

PrismNS.EddyOverviewChart.DateInfoSorter = function (a, b) {
    if (a.date < b.date)
        return -1;
    if (b.date < a.date)
        return 1;
    return 0;
}

PrismNS.EddyOverviewChart.NameInfoSorter = function (a, b) {
    if (a.name < b.name)
        return -1;
    if (b.name < a.name)
        return 1;
    return 0;
}

PrismNS.EddyOverviewChart.PrepareChartData = function (chartdata) {

    var eddyvalues = chartdata.EddyValues;


    PrismNS.EddyOverviewChart.ChartSeriesInfoArray = new Array();
    PrismNS.EddyOverviewChart.ChartSeriesInfoMap = new Array();

    if (eddyvalues.length == 0) {
        alert(PrismNS.Strings.No_eddy_data);
        return;
    }


    for (var i = 0; i < chartdata.Versions.length; i++) {
        var segids = new Array();
        var points = [];
        var eddyAcc = 0;
        for (var j = 0; j < eddyvalues[i].length; j++) {
            if (eddyvalues[i][j] != -1) {

                points.push([chartdata.SegmentStartPositions[j], eddyvalues[i][j]]);
                segids.push(chartdata.BaseTextSegIds[j]);
                eddyAcc += eddyvalues[i][j];
            }
        }

        var averageeddy = 0;
        if (segids.length > 0)
            averageeddy = eddyAcc / segids.length;

        //chartSeries[chartdata.Versions[i]] = { label: chartdata.Versions[i], data: points, _segids: segids };
        //chartSeriesToggle[chartdata.Versions[i]] = 1;
        //chartSeriesNames.push(chartdata.Versions[i]);

        var fullVersionName = chartdata.Versions[i];
        if (chartdata.VersionDates[i] != null)
            fullVersionName += " (" + chartdata.VersionDates[i] + ")";
        fullVersionName += " - " + averageeddy.toFixed(2);

        var series = { label: fullVersionName, data: points, _segids: segids, _shortname: chartdata.Versions[i] };

        var drawSeries = chartdata.Versions.length <= 10; // otherwise too many; let user decide which to draw
        var info = PrismNS.EddyOverviewChart.ChartSeriesInfo(fullVersionName, chartdata.Versions[i], chartdata.VersionDates[i],
                                averageeddy, series, drawSeries);

        PrismNS.EddyOverviewChart.ChartSeriesInfoArray.push(info);
        PrismNS.EddyOverviewChart.ChartSeriesInfoMap[fullVersionName] = info;
    }

}

PrismNS.EddyOverviewChart.LegendItemChecked = function (label) {
    if (PrismNS.EddyOverviewChart.ChartSeriesInfoMap[label].state)
    //if (chartSeriesToggle[label])
        return ' checked="checked" ';

    return '';
}

PrismNS.EddyOverviewChart.ShowChart = function (xaxismin, xaxismax, yaxismin, yaxismax) {


    var seriesList = new Array();

    for (var i = 0; i < PrismNS.EddyOverviewChart.ChartSeriesInfoArray.length; i++) {
        if (PrismNS.EddyOverviewChart.ChartSeriesInfoArray[i].state) {
            seriesList.push(PrismNS.EddyOverviewChart.ChartSeriesInfoArray[i].series);
        }
        else {
            seriesList.push({ label: PrismNS.EddyOverviewChart.ChartSeriesInfoArray[i].label, data: [] });
        }
    }


    //    for (var i = 0; i < chartSeriesNames.length; i++) {
    //        if (chartSeriesToggle[chartSeriesNames[i]]) {
    //            seriesList.push(chartSeries[chartSeriesNames[i]]);
    //        }
    //        else {
    //            seriesList.push({ label: chartSeriesNames[i], data: [] });
    //        }
    //    }

    //    for each (var series in chartSeries) {
    //        if (chartSeriesToggle[series.label])
    //            seriesList.push(series);
    //    }


    //    for (var i = 0; i < chartdata.Versions.length; i++) {
    //        var segids = new Array();
    //        var points = [];
    //        for (var j = 0; j < eddyvalues[i].length; j++) {
    //            if (eddyvalues[i][j] != -1) {

    //                points.push([ chartdata.SegmentStartPositions[j] , eddyvalues[i][j]]);
    //                segids.push(chartdata.BaseTextSegIds[j]);
    //            }
    //        }

    //        series.push({ label: chartdata.Versions[i], data: points, _segids: segids });
    //    }

    var options = {
        series: { lines: { show: true }, points: { show: true} },
        xaxes: [{ position: 'top'}],
        legend: {
            container: $('#legendcontainer'),
            noColumns: 4,
            labelFormatter: function (label, series) {
                var cb = '<input type="checkbox" class="legendcheckbox" name="' + label + '"' + PrismNS.EddyOverviewChart.LegendItemChecked(label) + ' id="id' + label + '"> ' + label;
                return cb;
            }
        },
        grid: { hoverable: true} //,
        //            xaxis: { zoomRange: [null, null], panRange: [null, null] },
        //            yaxis: { zoomRange: [null, null], panRange: [null, null] },
        //            zoom: {
        //                interactive: true
        //            },
        //            pan: {
        //                interactive: true
        //            }
            ,
        selection: { mode: "xy" }

    };

    if (xaxismin != null) {
        options.xaxis = { min: xaxismin, max: xaxismax };
        options.yaxis = { min: yaxismin, max: yaxismax };
    }

    PrismNS.Utils.EnableButton('zoomout', xaxismin != null);

    PrismNS.EddyOverviewChart.plot = $.plot($('#chart'), seriesList, options);

    var legendHeightPx = (seriesList.length + 1) / 4 * 25;
    $('#legendcontainer').height(legendHeightPx);

    $('.legendcheckbox').change(function () {
        //alert('Handler for .change() called.');
        PrismNS.EddyOverviewChart.ChartSeriesInfoMap[this.name].state = this.checked;
        //chartSeriesToggle[this.name] = this.checked;
        PrismNS.EddyOverviewChart.ShowChart(null, null, null, null);

    });


    var ctx = PrismNS.EddyOverviewChart.plot.getCanvas().getContext("2d");
    ctx.textAlign = 'center';

    $("#chart").bind("plotselected", function (event, ranges) {

        PrismNS.EddyOverviewChart.ShowChart(ranges.xaxis.from, ranges.xaxis.to, ranges.yaxis.from, ranges.yaxis.to);

    });


    $("#chart").bind("plothover", function (event, pos, item) {
        //        $("#x").text(pos.x.toFixed(2));
        //        $("#y").text(pos.y.toFixed(2));

        if (true) {
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;
                    var now = new Date();
                    previousPointDisplayTime = now.getTime();

                    if (PrismNS.EddyOverviewChart.plot.getSelection() != null)
                        return; // don't show tooltips while selecting
                    $("#tooltip").remove();
                    //var x = item.datapoint[0].toFixed(2),
                    //                      y = item.datapoint[1].toFixed(2);

                    //var url = _siteUrl + 'Document/GetBaseTextSegmentText';
                    var url = _siteUrl + 'Document/GetEddyOverviewChartTooltipText';

                    var args = { CorpusName: _corpusName, BaseTextSegmentID: item.series._segids[item.dataIndex], VersionName: item.series._shortname };


                    CallControllerAsync(url, args, function (result) {
                        if (result != null) {
                            if (result.Succeeded) {
                                //var text = result.SegmentText + /*"Segment " + item.series._segids[item.dataIndex] +*/" (" + item.series.label /*+ " " + item.datapoint[1]*/ + ")" /* + result.SegmentText*/;

                                var text = result.BaseTextSegmentText + "<br/>--<br/>" + result.VersionSegmentText + " (" + item.series.label + ")";
                                showTooltip(item.pageX, item.pageY,
                                                    text);

                                return;

                            }
                            //alert('Aligning failed: ' + result.ErrorMsg);

                        }

                    });

                }
            }
            else {
                if (!!previousPointDisplayTime) {
                    var now1 = new Date();

                    if (now1.getTime() - previousPointDisplayTime > 500) {
                        $("#tooltip").remove();
                        previousPoint = null;

                    }
                }

            }
        }
    });
}


function showTooltip(x, y, contents) {
    $('<div id="tooltip">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y + 5,
        left: x + 5,
        border: '1px solid #fdd',
        padding: '2px',
        'background-color': '#fee',
        opacity: 0.80
    }).appendTo("body").fadeIn(200);
}
