﻿var PrismNS = PrismNS || {};
PrismNS.SegmentVersionsTable = {};

PrismNS.SegmentVersionsTable.VersionData = null;
PrismNS.SegmentVersionsTable.ShowSVD = false;
PrismNS.SegmentVersionsTable.sizetowindow = true;

var previousPoint = null;
var previousPointDisplayTime = null;
PrismNS.SegmentVersionsTable.vivScale = d3.scale.sqrt();
PrismNS.SegmentVersionsTable.eddyScale = d3.scale.sqrt();

PrismNS.SegmentVersionsTable.scale = new chroma.scale(['#eaeaea', '#87c0ff']);

$(function () {

    EblaNS.FillVariationTypeSelectElm('#variation-type-select');

    var $vs = $('#variation-type-select');
    $('#submitmetrictypeval').val($vs.val());
    $('#submitcorpusname').val(PrismNS.Utils.GetCorpusName());

    // switch viv type show on select
    $vs.change(function (event) {

        PrismNS.SegmentVersionsTable.GetAndShowGridData();
        $('#submitmetrictypeval').val($vs.val());
    });

    var $as = $('#attribname');
    $as.change(function (event) {
        $('#submitattribname').val($as.val());
        PrismNS.SegmentVersionsTable.GetAndShowGridData();
    });

    $('#deleterows').click(function (e) { PrismNS.SegmentVersionsTable.DeleteRows(e); });


    //    $('#exportall').click(function (e) { PrismNS.SegmentVersionsTable.Export(e); });

    $('#addrows').click(function (e) {
        PrismNS.SegmentVersionsTable.AddRows(e);
    });

    $('#showsvd').change(function (e) {
        PrismNS.SegmentVersionsTable.ShowSVD = this.checked;
        PrismNS.SegmentVersionsTable.GetAndShowGridData();
    });

    $('#sizetowindow').change(function (e) {
        PrismNS.SegmentVersionsTable.sizetowindow = this.checked;
        PrismNS.SegmentVersionsTable.ReloadGrid();
    });

    $(window).resize(function () {
        setTimeout(function () {
            if (PrismNS.SegmentVersionsTable.sizetowindow)
                PrismNS.SegmentVersionsTable.ReloadGrid();
        }, 100);
    });


    PrismNS.SegmentVersionsTable.GetAndShowGridData();

});

//PrismNS.SegmentVersionsTable.Export = function () {
//    var TableData = PrismNS.SegmentVersionsTable.VersionData;
//    var rows = new Array();

//    var headerRow = new Array();
//    headerRow.push("Viv");
//    if (TableData.AttribName)
//        if (TableData.AttribName.length > 0) {
//            headerRow.push(TableData.AttribName);
//        }

//    for (var i = 0; i < TableData.colModel.length; i++) {
//        var title = TableData.colModel[i].title;
//        if (TableData.colModel[i].refdate)
//            if (TableData.colModel[i].refdate.length > 0)
//                title += " (" + TableData.colModel[i].refdate + ")";
//        headerRow.push(title);
//        headerRow.push("eddy");
//    }

//    rows.push(headerRow);

//    for (var i = 0; i < TableData.Rows.length; i++) {
//        var row = new Array();
//        row.push(TableData.Rows[i].VivValue);
//        if (TableData.AttribName)
//            if (TableData.AttribName.length > 0) {
//                row.push(TableData.Rows[i].AttribValue);
//            }

//        row.push($.trim(TableData.Rows[i].BaseTextContent));

//        for (var cell = 0; cell < TableData.Rows[i].Cells.length; cell++) {
//            row.push($.trim(TableData.Rows[i].Cells[cell].VersionContent));
//            row.push(TableData.Rows[i].Cells[cell].EddyValue);
//        }

//        rows.push(row);
//    }

//    //var csvContent = "data:text/csv;charset=utf-8,";
//    var csvContent = "data:attachment/csv;charset=utf-8,";
//    rows.forEach(function (row, index) {

//        var dataString = '';
//        row.forEach(function (cell, cellindex) {
//            var cellString = '' + cell;
//            cellString.replace('"', '""');
//            dataString += '"' + cellString + '"';
//            if (cellindex < row.length)
//                dataString += ',';
//        });

//        //var dataString = infoArray.join(",");
//        csvContent += index < rows.length ? dataString + "\n" : dataString;
//    });

//    var encodedUri = encodeURI(csvContent);
//    //window.open(encodedUri);

//    var a = document.createElement('a');
//    var jsa = $(a);
//    jsa.attr('href', encodedUri);
//    jsa.attr('download', 'myFile.csv');
//    jsa.attr('target', '_blank');
//    //a.href = 'data:attachment/csv,' + csvContent;
//    //a.target = '_blank';
//    //a.download = 'myFile.csv';

//    document.body.appendChild(a);
//    a.click();

//}

PrismNS.SegmentVersionsTable.GetAndShowGridData = function () {
    var $vs = $('#variation-type-select');
    var $as = $('#attribname');

    var url = _siteUrl + 'Visualise/GetSegmentVersionsData';

    var args = { CorpusName: PrismNS.Utils.GetCorpusName(), metricTypeVal: parseInt($vs.val()), AttribName: $as.val(), getSVDs: PrismNS.SegmentVersionsTable.ShowSVD };

    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                PrismNS.SegmentVersionsTable.VersionData = result;
                PrismNS.SegmentVersionsTable.ReloadGrid();

                return;

            }
            alert(PrismNS.Strings.Populate_failed + ' ' + result.ErrorMsg);
        }
    });


}

PrismNS.SegmentVersionsTable.ReloadGrid = function () {
    //PrismNS.SegmentVersionsTable.GetGrid().GridUnload();
    $.jgrid.gridUnload('#list2');
    PrismNS.SegmentVersionsTable.PopulateTable();
    PrismNS.SegmentVersionsTable.SizeGridToWindow();

}



//PrismNS.SegmentVersionsTable.GetGrid = function() {
//    //return $('#list2');
//    return $.jgrid('#list2');
//}

PrismNS.SegmentVersionsTable.SizeGridToWindow = function () {
    //var grid = PrismNS.SegmentVersionsTable.GetGrid()
    var gridOffset = $('#list2').offset();
    var availHeight = $(document).height() - gridOffset.top;

    $('#list2').setGridHeight(availHeight - 20);
}

PrismNS.SegmentVersionsTable.DeleteRows = function (e) {
    //var rowids = jQuery("#list2").jqGrid(
    //var grid = PrismNS.SegmentVersionsTable.GetGrid();

    var ids = $('#list2').getGridParam('selarrrow');

    var sel = PrismNS.Utils.GetSegmentSelection();
    // Are we removing them all?
    if (ids.length == sel.count) {
        PrismNS.Utils.ClearSegmentSelection();
        $('#list2').clearGridData(true).trigger("reloadGrid");
        return;
    }


    for (var i = ids.length - 1; i >= 0; i--) {
        var rowData = $('#list2').getRowData(ids[i]);
        var segId = rowData.segid;
        PrismNS.Utils.RemoveSegmentFromSelection(segId);
        $('#list2').delRowData(ids[i]);
    }
}

PrismNS.SegmentVersionsTable.AddRows = function (e) {

    var data = { CorpusName: _corpusName };

    var url = _siteUrl + "Visualise/FindSegments";

    showModal(url, PrismNS.Strings.Addsegments, PrismNS.modalEnum.okCancel, data, null, null, function (formdata) { PrismNS.SegmentVersionsTable.AddSelectedRows(formdata); return -1; /*close*/ });
}

PrismNS.SegmentVersionsTable.AddSelectedRows = function (formdata) {
    if (!formdata)
        return;
    if (formdata.length == 0)
        return;

    var temp = new Array();
    for (var i = 0; i < formdata.length; i++) {
        if (formdata[i].name.substr(0, 4) == "seg-") {
            temp.push(formdata[i].value);
        }
        //        if (!PrismNS.Utils.AddSegmentToSelection(formdata[i].value)) {
        //            alert('Too many rows were specified. Only ' + (i - 2) + ' of ' + (formdata.length - 2) + ' were added to the selection.');
        //            break;
        //        }
    }

    var okcount = PrismNS.Utils.AddSegmentsToSelection(temp);
    if (okcount < temp.length) {
        var msg = PrismNS.Utils.StringFormat(PrismNS.Strings.Too_many_rows, okcount, temp.length);
        alert(msg);
        //alert('Too many rows were specified. Only ' + okcount + ' of ' + temp.length + ' were added to the selection.');
    }


    PrismNS.SegmentVersionsTable.GetAndShowGridData();
}

function svdFormatter(cellvalue, options, rowObject) {
    return "<div class='svdchart' id='svd" + cellvalue + "'></div>";
}

function vivFormatter(cellvalue, options, rowObject) {
    var normalizedViv = PrismNS.SegmentVersionsTable.scale.getColor(PrismNS.SegmentVersionsTable.vivScale(cellvalue));
    var bc = normalizedViv.hex();
    return "<div style='background-color: " + bc + "'>" + cellvalue.toFixed(4) + "</div>";
}

PrismNS.SegmentVersionsTable.PopulateTable = function () {

    var TableData = PrismNS.SegmentVersionsTable.VersionData;
    if (!TableData)
        return;
    var colNames = new Array();
    var colModel = new Array();
    if (PrismNS.SegmentVersionsTable.ShowSVD) {
        colNames.push('SVD');
        colModel.push({ name: 'svd', formatter: svdFormatter, width: 80 });
    }

    colNames.push('Viv');
    //colModel.push({ name: 'viv', formatter: 'number', formatoptions: { decimalPlaces: 4 }, width: 30 });
    colModel.push({ name: 'viv', formatter: vivFormatter, width: 30 });
    if (TableData.AttribName)
        if (TableData.AttribName.length > 0) {
            colNames.push(TableData.AttribName);
            colModel.push({ name: TableData.AttribName, width: 50 });
        }

    for (var i = 0; i < TableData.colModel.length; i++) {
        var title = TableData.colModel[i].title;
        if (TableData.colModel[i].refdate)
            if (TableData.colModel[i].refdate.length > 0)
                title += "<br/>(" + TableData.colModel[i].refdate + ")";
        colNames.push(title);

        colModel.push({ name: TableData.colModel[i].name });
    }
    colNames.push('segid');
    colModel.push({ name: 'segid', hidden: true });

    //var grid = PrismNS.SegmentVersionsTable.GetGrid();


    $('#list2').jqGrid({
        datatype: "local",
        height: 250,
        autowidth: PrismNS.SegmentVersionsTable.sizetowindow,
        rowNum: 10000,
        colNames: colNames,
        colModel: colModel,
        multiselect: true,
        multiSort: true,
        sortable: true,
        caption: PrismNS.Strings.Segment_versions
    });

    var minViv = 0;
    var maxViv = 0;
    var minEddy = 0;
    var maxEddy = 0;
    if (TableData.Rows.length > 0) {
        minViv = TableData.Rows[0].VivValue;
        maxViv = TableData.Rows[0].VivValue;
        if (TableData.Rows[0].Cells.length > 0) {
            minEddy = TableData.Rows[0].Cells[0].EddyValue;
            maxEddy = TableData.Rows[0].Cells[0].EddyValue;
        }
    }


    for (var i = 0; i < TableData.Rows.length; i++) {
        var v = TableData.Rows[i].VivValue;
        if (v > maxViv)
            maxViv = v;
        if (v < minViv)
            minViv = v;


        for (var cell = 0; cell < TableData.Rows[i].Cells.length; cell++) {
            var e = TableData.Rows[i].Cells[cell].EddyValue;
            if (e > maxEddy)
                maxEddy = e;
            if (e >= 0 && e < minEddy)
                minEddy = e;
        }

    }

    PrismNS.SegmentVersionsTable.vivScale.domain([minViv, maxViv]).range([0, 1]);
    PrismNS.SegmentVersionsTable.eddyScale.domain([minEddy, maxEddy]).range([0, 1]);

    for (var i = 0; i < TableData.Rows.length; i++) {
        var row = new Object();
        row.segid = TableData.Rows[i].BaseTextSegmentID;
        row.bt = $.trim(TableData.Rows[i].BaseTextContent);
        //        row.bt = "<div class='celltop'>" + $.trim(TableData.Rows[i].BaseTextContent) + "</div><br/>"
        //                   + "<div class='cellbottom'>Viv: " + TableData.Rows[i].VivValue.toFixed(4) + "</div>";
        row.viv = TableData.Rows[i].VivValue;
        row.svd = i;
        if (TableData.AttribName) {
            if (TableData.AttribName.length > 0) {
                row[TableData.AttribName] = TableData.Rows[i].AttribValue;
            }
        }
        for (var cell = 0; cell < TableData.Rows[i].Cells.length; cell++) {
            var e = TableData.Rows[i].Cells[cell].EddyValue;
            if (e < 0) e = 0;
            var normalizedEddy = PrismNS.SegmentVersionsTable.scale.getColor(PrismNS.SegmentVersionsTable.eddyScale(e));
            var bc = normalizedEddy.hex();
            var alignerhref = '';
            if (TableData.Rows[i].Cells[cell].EddyValue != -1)
                alignerhref = _siteUrl +
                    'Aligner?CorpusName=' +
                    encodeURIComponent(_corpusName) +
                    '&VersionName=' +
                    encodeURIComponent(TableData.colModel[cell + 1].title) +
                    '&basetextSegIDToView=' +
                    TableData.Rows[i].BaseTextSegmentID +
                    '&versionSegIDToView=' +
                    TableData.Rows[i].Cells[cell].SegmentIDsInVersion[0];
            row["v" + cell] = "<div class='celltop'>" +
                $.trim(TableData.Rows[i].Cells[cell].VersionContent) +
                "</div><br/>" +
                "<div class='cellbottom'><span style='background-color:" +
                bc +
                "'>" +
                PrismNS.Strings.Eddy +
                ": " +
                TableData.Rows[i].Cells[cell].EddyValue.toFixed(4) +
                "</span>" +
                (TableData.Rows[i].Cells[cell].EddyValue == -1
                    ? ""
                    : " <a href='" + alignerhref + "' title='" + PrismNS.Strings.See_seg_in_aligner + "'>A</a>") +
                "</div>";
        }

        $('#list2').addRowData(i + 1, row);
    }


    $('.svdchart').each(function (index, element) {
        var points = [];

        var id = $(this).attr('id');

        var dataindex = parseInt(id.substr(3));
        var svdpts = TableData.Rows[dataindex].SVDPts;
        for (var d = 0; d < svdpts.length; d++) {

            points.push([svdpts[d].X, svdpts[d].Y,]);
        }

        var chartSeries = { label: '', data: points };

        var seriesList = new Array();

        seriesList.push(chartSeries);

        var options = {
            series: { lines: { show: false }, points: { show: true } } /*,
            xaxes: [{ position: 'top'}],

            grid: { hoverable: true} //,

            ,
            selection: { mode: "xy" }*/,
            legend: { show: false },
            xaxis: { show: false },
            yaxis: { show: false },
            grid: { hoverable: true }
        };


        $.plot($(this), seriesList, options);


        $(this).bind("plothover",
            function (event, pos, item) {
                //        $("#x").text(pos.x.toFixed(2));
                //        $("#y").text(pos.y.toFixed(2));

                if (true) {
                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;
                            var now = new Date();
                            previousPointDisplayTime = now.getTime();


                            $("#tooltip").remove();
                            var text = TableData.VersionNames[item.dataIndex]; // item.series.label;
                            showTooltip(item.pageX,
                                item.pageY,
                                text);
                            //var x = item.datapoint[0].toFixed(2),
                            //                      y = item.datapoint[1].toFixed(2);

                            //var url = _siteUrl + 'Document/GetBaseTextSegmentText';
                            //                        var url = _siteUrl + 'Document/GetEddyOverviewChartTooltipText';

                            //                        var args = { CorpusName: _corpusName, BaseTextSegmentID: item.series._segids[item.dataIndex], VersionName: item.series._shortname };


                            //                        CallControllerAsync(url, args, function (result) {
                            //                            if (result != null) {
                            //                                if (result.Succeeded) {
                            //                                    //var text = result.SegmentText + /*"Segment " + item.series._segids[item.dataIndex] +*/" (" + item.series.label /*+ " " + item.datapoint[1]*/ + ")" /* + result.SegmentText*/;

                            //                                    var text = result.BaseTextSegmentText + "<br/>--<br/>" + result.VersionSegmentText + " (" + item.series.label + ")";
                            //                                    showTooltip(item.pageX, item.pageY,
                            //                                                    text);

                            //                                    return;

                            //                                }
                            //                                //alert('Aligning failed: ' + result.ErrorMsg);

                            //                            }

                            //                        });

                        }
                    } else {
                        if (!!previousPointDisplayTime) {
                            var now1 = new Date();

                            if (now1.getTime() - previousPointDisplayTime > 500) {
                                $("#tooltip").remove();
                                previousPoint = null;

                            }
                        }

                    }
                }
            });


    });

}

function showTooltip(x, y, contents) {
    $('<div id="tooltip">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y + 5,
        left: x + 5,
        border: '1px solid #fdd',
        padding: '2px',
        'background-color': '#fee',
        opacity: 0.80
    }).appendTo("body").fadeIn(200);
}

