﻿

var EblaNS = EblaNS || {};


EblaNS.VariationMetricTypes = {
    metricA: { value: 1, name: 'A - Eddy=Euclidean distance, Viv=average Eddy' },
    metricB: { value: 2, name: 'B - Original Viv and Eddy formulae' },
    metricC: { value: 3, name: 'C - Eddy=Euclidean distance, Viv=S.D. of Eddy' },
    metricD: { value: 4, name: 'D - Eddy=(1-) Dice\'s coefficient, Viv=average Eddy' },
    metricE: { value: 5, name: 'E - Eddy=Angular distance, Viv=average Eddy' }
};

EblaNS.FillVariationTypeSelectElm = function (elmselector) {
    var $vs = $(elmselector);

    _.each(EblaNS.VariationMetricTypes, function (type, value) {
        $vs.append('<option value="' + type.value + '">' + type.name + '</option>');
    });

}