﻿/*
Version Variation Visualisation (VVV)
http://www.delightedbeauty.org
    
Copyright (c) Kevin Flanagan, 2012.
http://www.kftrans.co.uk
  
This file is part of VVV.

VVV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VVV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VVV.  If not, see <http://www.gnu.org/licenses/>.
*/

var PrismNS = PrismNS || {};

PrismNS.CorpusIndex = {};


$(function () {

    //    EblaNS.FillVariationTypeSelectElm('#variation-type-select');

    $('.corp-add-selection').click(function (e) { PrismNS.CorpusIndex.AddToSelection(e); });
    $('.corp-rem-selection').click(function (e) { PrismNS.CorpusIndex.RemoveFromSelection(e); });

    $('#corp-all-selection').click(function (e) { PrismNS.CorpusIndex.SelectAll(e); });
    $('#corp-none-selection').click(function (e) { PrismNS.CorpusIndex.UnselectAll(e); });

    //var v = $('.corp-rem-selection');
    //    $('#stopcalc').click(function (e) { PrismNS.CorpusAdmin.StopCalc(e); });
    //    $('#retok').click(function (e) { PrismNS.CorpusAdmin.Retokenise(e); });

    //    $('#test').click(function (e) { PrismNS.CorpusAdmin.Test(e); });

    //    PrismNS.CorpusAdmin.EnableControls();

    //    var corpusbusy = $('#corpusbusy').val();
    //    if (corpusbusy == 1)
    //        PrismNS.CorpusAdmin.SetTimer();

});

PrismNS.CorpusIndex.VersionNameFromElement = function (e) {
    return $(e.target).attr('data-id').substr(7);
}

PrismNS.CorpusIndex.RebuildSelection = function (e) {
    var result = new Array();

    var selectedVersions = $(".version-title-link[data-insel='1']");
    for (var i = 0; i < selectedVersions.length; i++) {
        var id = $(selectedVersions[i]).attr('data-id');
        result.push(id.substr(8));
    }
    result.sort();
    return result;
}

PrismNS.CorpusIndex.UpdateSelectionData = function (e) {
    var selection = PrismNS.CorpusIndex.RebuildSelection();

    var cookieString = '';
    var captionString = '';
    for (var i = 0; i < selection.length; i++) {
        if (i > 0) {
            cookieString += '\n';
            captionString += '\n';
        }
        cookieString += selection[i];// encodeURIComponent(selection[i]);
        captionString += selection[i];
    }

    var captionPrefix = _corpusName;
    if (selection.length > 0) {
        captionPrefix += '\n--\n';
    }
    captionString = captionPrefix + captionString;

    $('#corpus-menu-button').attr('title', captionString);

    $.cookie('prismactivecorpusselection', cookieString, { expires: 7, path: '/' });
}

PrismNS.CorpusIndex.AddToSelection = function (e) {
    var version = PrismNS.CorpusIndex.VersionNameFromElement(e);
    //alert(PrismNS.CorpusIndex.VersionNameFromElement(e));
    //$('#veradd-' + version).toggle();
    //$('#verrem-' + version).toggle();
    $("span[data-id='veradd-" + version + "']").toggle();
    $("span[data-id='verrem-" + version + "']").toggle();

    var verlink = $("[data-id='verlink-" + version + "']");
    verlink.css('color', '');
    verlink.attr('data-insel', '1');

    PrismNS.CorpusIndex.UpdateSelectionData();
    //alert(PrismNS.CorpusIndex.RebuildSelection());
}

PrismNS.CorpusIndex.SelectAll = function (e) {
    //var version = PrismNS.CorpusIndex.VersionNameFromElement(e);
    //alert(PrismNS.CorpusIndex.VersionNameFromElement(e));
    //$('#veradd-' + version).toggle();
    //$('#verrem-' + version).toggle();

    $('.corp-add-selection').hide();
    $('.corp-rem-selection').show();

    //$("span[data-id='veradd-" + version + "']").toggle();
    //$("span[data-id='verrem-" + version + "']").toggle();


//    var verlink = $("[data-id='verlink-" + version + "']");
//    verlink.css('color', '');
//    verlink.attr('data-insel', '1');

    $('.version-title-link').css('color', '');
    $('.version-title-link').attr('data-insel', '1');

    PrismNS.CorpusIndex.UpdateSelectionData();
    //alert(PrismNS.CorpusIndex.RebuildSelection());
}

PrismNS.CorpusIndex.UnselectAll = function (e) {

    $('.corp-add-selection').show();
    $('.corp-rem-selection').hide();


    $('.version-title-link').css('color', 'gray');
    $('.version-title-link').attr('data-insel', '0');

    PrismNS.CorpusIndex.UpdateSelectionData();
}



PrismNS.CorpusIndex.RemoveFromSelection = function (e) {
//    alert(PrismNS.CorpusIndex.VersionNameFromElement(e));
    var version = PrismNS.CorpusIndex.VersionNameFromElement(e);
    //alert(PrismNS.CorpusIndex.VersionNameFromElement(e));
    //$('#veradd-' + version).toggle();
    //$('#verrem-' + version).toggle();
    $("span[data-id='veradd-" + version + "']").toggle();
    $("span[data-id='verrem-" + version + "']").toggle();

    var verlink = $("[data-id='verlink-" + version + "']");
    verlink.css('color', 'gray');
    verlink.attr('data-insel', '0');

    //alert(PrismNS.CorpusIndex.RebuildSelection());
    PrismNS.CorpusIndex.UpdateSelectionData();

}

