﻿/*
Version Variation Visualisation (VVV)
http://www.delightedbeauty.org
    
Copyright (c) Kevin Flanagan, 2012.
http://www.kftrans.co.uk
  
This file is part of VVV.

VVV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VVV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VVV.  If not, see <http://www.gnu.org/licenses/>.
*/

$(function () {

    //    $('#backup').click(function (e) { Backup(e); });
    //$("#viewbutton").click(function (e) { ConfirmCorpusDeletion(true); });
    //$("#deleteConfirm").click(function (e) { ConfirmCorpusDeletion(e); });
    //$("#deleteConfirm").click(function (e) { ConfirmCorpusDeletion(e); });

    //$(".deleteConfirm").click(function () {
    //    var anchor = this;
    //    // Show the confirmation dialog
    //    $.msgBox({
    //        title: "Confirmation required",
    //        content: "Do you want to confirm?",
    //        type: "confirm",
    //        buttons: [{ value: "Yes" }, { value: "No" }],
    //        success: function (result) {
    //            if (result == "Yes") {
    //                // send the AJAX request if the user selected "Oui":
    //                $.ajax({
    //                    url: anchor.href,
    //                    type: "POST",
    //                    success: function (data) {
    //                        // When the AJAX request succeeds update the 
    //                        // corresponding element in your DOM
    //                        $("#ModalConfirmation").html(data);
    //                    }
    //                });
    //            }
    //        }
    //    });

    //    // Always cancel the default action of the link
    //    return false;
    //});


    var deleteLinkObject;
    // delete Link
    $(".delete-link").click(function () {
        deleteLinkObject = $(this);  //for future use
        $("#delete-dialog").dialog("open");
        return false; // prevents the default behaviour
    });

    //definition of the delete dialog.
    $("#delete-dialog").dialog({
        autoOpen: false, width: 400, resizable: false, modal: true, //Dialog options
        buttons: {
            "Yes": function () {
                $.post(deleteLinkObject[0].href, function (data) {  //Post to action
                    if (data != null) {
                        deleteLinkObject.closest("tr").hide("fast"); //Hide Row
                        //(optional) Display Confirmation
                    }
                    //else {
                    //    //(optional) Display Error
                    //}
                });
                $(this).dialog("close");
                location.reload();
                //$(this).load(deleteLinkObject[0].href);
                
            },
            "No": function () {
                $(this).dialog("close");
            }
        }
    });
});


function ConfirmCorpusDeletion() {

    var version = $("#versionname").val();

    var data = { CorpusName: _corpusName, VersionName: version };

    var url = _siteUrl + "CorpusStore/Delete";

    showModal(url, "Delete", PrismNS.modalEnum.okCancel, data, null, function () { UpdateExtract("editorenclosurediv", 'docextractform', GetEditorInstanceByName('doceditor'), false); });

}