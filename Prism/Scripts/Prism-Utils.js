﻿/*
Version Variation Visualisation (VVV)
http://www.delightedbeauty.org
    
Copyright (c) Kevin Flanagan, 2012.
http://www.kftrans.co.uk
  
This file is part of VVV.

VVV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VVV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VVV.  If not, see <http://www.gnu.org/licenses/>.
*/

var PrismNS = PrismNS || {};


/////////////////
//
//  PrismNS.Utils
//
//  Helper functions that don't yet belong anywhere else
//
/////////////////

PrismNS.Utils = {};

PrismNS.Utils.StringFormat = function(format) {
    var args = Array.prototype.slice.call(arguments, 1);
    return format.replace(/{(\d+)}/g, function (match, number) {
        return typeof args[number] != 'undefined'
                ? args[number]
                : match
            ;
    });
    
}

PrismNS.Utils.AddSegmentsToSelection = function (segids) {
    var sel = PrismNS.Utils.GetSegmentSelection();
    var okcount = 0;
    var cookiename = 'prismsegmentselection';
    var cookiestring = $.cookie(cookiename);
    if (!cookiestring)
        cookiestring = '';

    for (var i = 0; i < segids.length; i++) {
        if (sel['s' + segids[i]]) {
            // already in selection
            okcount++;
        }
        else {
            if (cookiestring.length > 4000)
                break; // getting too long

            if (cookiestring.length > 0)
                cookiestring += '-';

            cookiestring += segids[i];
            okcount++
        }

    }
    $.cookie(cookiename, cookiestring, { expires: 7, path: '/' });
    return okcount;
}

PrismNS.Utils.AddSegmentToSelection = function (segidarg) {

    var temp = new String(segidarg);
    var segid = parseInt(temp);

    var sel = PrismNS.Utils.GetSegmentSelection();
    if (sel['s' + segid])
        return true;

//    for (var i = 0; i < sel.length; i++) {
//        if (sel[i] == segid)
//            return true;
//    }

    var cookiename = 'prismsegmentselection';
    var cookiestring = $.cookie(cookiename);
    if (!cookiestring)
        cookiestring = '';

    if (cookiestring.length > 4000)
        return false; // getting too long

    if (cookiestring.length > 0)
        cookiestring += '-';

    cookiestring += segid;
    $.cookie(cookiename, cookiestring, { expires: 7, path: '/' });
    return true;
}

PrismNS.Utils.RemoveSegmentFromSelection = function (segidarg) {

    var temp = new String(segidarg);
    var segid = parseInt(temp);

    var sel = PrismNS.Utils.GetSegmentSelection();

    if (sel['s' + segid]) {

        var cookiename = 'prismsegmentselection';
        var cookiestring = $.cookie(cookiename);

        if (sel.count == 1) {
            cookiestring = '';
        } else {
            var temp = '-' + cookiestring + '-';
            var pos = temp.indexOf('-' + segid + '-');
            pos--;
            var tempstr = segid + '-';
            // Was it right at start?
            if (pos == -1) {
                cookiestring = cookiestring.substr( tempstr.length);
            }
            // Was it right at the end?
            else if ((pos + tempstr.length) >= cookiestring.length) {
                cookiestring = cookiestring.substr(0, pos);
            }
            else {
                cookiestring = cookiestring.substr(0, pos) + cookiestring.substr(pos + tempstr.length);
            }
        }

       
        $.cookie(cookiename, cookiestring, { expires: 7, path: '/' });

    }


}

PrismNS.Utils.ClearSegmentSelection = function () {

    var cookiename = 'prismsegmentselection';
    var cookiestring ='';

    $.cookie(cookiename, cookiestring, { expires: 7, path: '/' });

}

PrismNS.Utils.GetSegmentSelection = function () {

    var cookiename = 'prismsegmentselection';
    var cookiestring = $.cookie(cookiename);
    //var result = new Array();
    var result = new Object();
    result.count = 0;

    if (!cookiestring || cookiestring.length == 0)
        return result;

    var ids = cookiestring.split('-');
    for (var i = 0; i < ids.length; i++) {
        result['s' + ids[i]] = true;
        //result.push(parseInt(ids[i]));
    }
    result.count = ids.length;
    return result;
}


PrismNS.Utils.GetCorpusName = function () {
    if (_corpusName)
        return _corpusName;

    _corpusName = $('#corpusname').val();
    return _corpusName;
}

PrismNS.Utils.GetSegmentIDForNode = function (node) {
    var idstring = $(node).attr("data-eblasegid");
    if (idstring) {
        if (idstring.length > 0) {

            var id = parseInt(idstring);
            if (id > 0)
                return id;
        }

    }

    if (node.parentNode == null)
        return -1;

    return PrismNS.Utils.GetSegmentIDForNode(node.parentNode);

}

PrismNS.Utils.EnableButton = function(id, enabled) {
    if (enabled) {
        $('#' + id).removeAttr('disabled');
    }
    else {
        $('#' + id).attr('disabled', 'disabled');
    }
}

PrismNS.Utils.AddAttribRow = function(nextrowindexinput, membername, tableid, rowidstem) {

    var nextrowindex = parseInt($('#' + nextrowindexinput).val());

    var args = { memberName: membername, CorpusName: _corpusName, rowindex: nextrowindex };
    var url = _siteUrl + 'Corpus/GetNewAttributeRowHtml';

    CallControllerAsync(url, args, function (result) {

        if (result.Succeeded) {
        }
        else {
            alert(result.ErrorMsg);
            return;
        }

        var table = document.getElementById(tableid);
        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);
        var rowid = rowidstem + nextrowindex;
        row.setAttribute('id', rowid);

        $(row).html(result.html);

        nextrowindex++;

        document.getElementById(nextrowindexinput).value = nextrowindex;

        PrismNS.Utils.BindAttribNameSelects(rowid, membername);


    });
}

PrismNS.Utils.BindAttribNameSelects = function(rowid, membername) {

    var selector = '.attribnameselect';
    if (rowid)
        selector = '#' + rowid + ' ' + selector;

    $(selector).bind('change', function () {

        PrismNS.Utils.UpdateAttribRow(this, membername);
    });
}

PrismNS.Utils.UpdateAttribRow = function(select, membername) {
    //var nextrowid = parseInt($('#nextattribrowindex').val());

    var newAttribName = select.value;

    var row = select[0].parentNode.parentNode.parentNode.parentNode;
    var rowid = row.getAttribute('id');
    var rowindexstr = rowid.substr(rowid.indexOf('-', 0) + 1);

    var args = { memberName: membername, CorpusName: _corpusName, rowindex: rowindexstr, currName: newAttribName };
    var url = _siteUrl + 'Corpus/GetNewAttributeRowHtml';

    CallControllerAsync(url, args, function (result) {

        if (result.Succeeded) {
        }
        else {
            alert(result.ErrorMsg);
            return;
        }

        $(row).html(result.html);

        PrismNS.Utils.BindAttribNameSelects(rowid);

    });
}

PrismNS.Utils.DeleteAttrib = function(r) {

    var i = r.parentNode.parentNode.rowIndex;
    var t = r.parentNode.parentNode.parentNode.parentNode;
    t.deleteRow(i);
    //document.getElementById('attribtable').deleteRow(i);

}