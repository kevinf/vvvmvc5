﻿var PrismNS = PrismNS || {};
PrismNS.FindSegmentDialog = {};

//function AddAttribRow(nextrowindexinput, membername, tableid, rowidstem) {

//    var nextrowindex = parseInt($('#' + nextrowindexinput).val());

//    var args = { memberName: membername, CorpusName: _corpusName, rowindex: nextrowindex };
//    var url = _siteUrl + 'Corpus/GetNewAttributeRowHtml';

//    CallControllerAsync(url, args, function (result) {

//        if (result.Succeeded) {
//        }
//        else {
//            alert(result.ErrorMsg);
//            return;
//        }

//        var table = document.getElementById(tableid);
//        var rowCount = table.rows.length;
//        var row = table.insertRow(rowCount);
//        var rowid = rowidstem + nextrowindex;
//        row.setAttribute('id', rowid);

//        $(row).html(result.html);

//        nextrowindex++;

//        document.getElementById(nextrowindexinput).value = nextrowindex;

//        BindAttribNameSelects(rowid, membername);


//    });
//}

//function BindAttribNameSelects(rowid, membername) {

//    var selector = '.attribnameselect';
//    if (rowid)
//        selector = '#' + rowid + ' ' + selector;

//    $(selector).bind('change', function () {

//        UpdateAttribRow(this, membername);
//    });
//}

//function UpdateAttribRow(select, membername) {
//    //var nextrowid = parseInt($('#nextattribrowindex').val());

//    var newAttribName = select.value;

//    var row = select[0].parentNode.parentNode.parentNode.parentNode;
//    var rowid = row.getAttribute('id');
//    var rowindexstr = rowid.substr(rowid.indexOf('-', 0) + 1);

//    var args = { memberName: membername, CorpusName: _corpusName, rowindex: rowindexstr, currName: newAttribName };
//    var url = _siteUrl + 'Corpus/GetNewAttributeRowHtml';

//    CallControllerAsync(url, args, function (result) {

//        if (result.Succeeded) {
//        }
//        else {
//            alert(result.ErrorMsg);
//            return;
//        }

//        $(row).html(result.html);

//        BindAttribNameSelects(rowid);

//    });
//}

//function deleteattrib(r) {

//    var i = r.parentNode.parentNode.rowIndex;
//    var t = r.parentNode.parentNode.parentNode.parentNode;
//    t.deleteRow(i);
//    //document.getElementById('attribtable').deleteRow(i);

//}

PrismNS.FindSegmentDialog.AddFilterAttrib = function () {

    //alert('adsd');

    PrismNS.Utils.AddAttribRow('nextattribfilterrowindex', 'AttributeFilters', 'attribfiltertable', 'attribfilterrow-'); 

}

PrismNS.FindSegmentDialog.CheckAll = function () {
    var c = $('#findsegcheckall');
    if (c.is(':checked'))
        $('.findsegcheckbox').attr('checked', 'checked');
    else
        $('.findsegcheckbox').removeAttr('checked');
}

PrismNS.FindSegmentDialog.GetFloorOrCeilingVal = function (id) {
    var input = $('#' + id);
    var result = input.val();
    //alert(result);
    if (result.length > 0) {
        result = parseFloat(result);
        if (isNaN(result)) {
            //alert('nn');
            input.val('');
            result = -1;
        }
        else {
            input.val(result);
        }
    }
    else
        result = -1;
    //alert(result);
    return result;

}

PrismNS.FindSegmentDialog.Find = function () {

    var queryString = $('#modalForm').formSerialize();

    var vivfloor = PrismNS.FindSegmentDialog.GetFloorOrCeilingVal('segmentsearchvivfloor');
    var vivceiling = PrismNS.FindSegmentDialog.GetFloorOrCeilingVal('segmentsearchvivceiling');
    var eddyfloor = PrismNS.FindSegmentDialog.GetFloorOrCeilingVal('segmentsearcheddyfloor');
    var eddyceiling = PrismNS.FindSegmentDialog.GetFloorOrCeilingVal('segmentsearcheddyceiling');
    var $vs = $('#variation-type-select');

    var url = _siteUrl + 'Visualise/GetFindSegmentsData' + '?' + queryString;

    var args = { CorpusName: PrismNS.Utils.GetCorpusName(), SearchText: $('#segmentsearchtext').val(),
        SearchBaseText: $('#segmentsearchbasetext').is(":checked"),
        SearchVersion: $('#segmentsearchversion').is(":checked"),
        VersionName: $('#versiontosearch').val(),
        VivFloor: vivfloor,
        VivCeiling: vivceiling,
        EddyFloor: eddyfloor,
        EddyCeiling: eddyceiling,
        metricTypeVal: parseInt($vs.val())
    };

    //alert(vivfloor);
    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                $('#findsegmentresults').html(result.TableHtml);
                $('#findsegcheckall').click(function (e) { PrismNS.FindSegmentDialog.CheckAll(); });
                return;

            }
            alert(PrismNS.Strings.Search_failed + ': ' + result.ErrorMsg);
        }
    });

}