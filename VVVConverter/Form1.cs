﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace VVVConverter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ImportFileBrowseButton_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();

                //openFileDialog1.InitialDirectory = "c:\\";
                openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    this.ImportFileTextBox.Text = openFileDialog1.FileName;
                    EnableControls();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void EnableControls()
        {
            this.ConvertButton.Enabled = !string.IsNullOrWhiteSpace(ImportFileTextBox.Text) && !string.IsNullOrWhiteSpace(CorpusNameTextBox.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.CorpusNameTextBox.Text = Properties.Settings.Default.CorpusName;
            this.ImportFileTextBox.Text = Properties.Settings.Default.InputFile;
            EnableControls();
            try
            {
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ConvertButton_Click(object sender, EventArgs e)
        {
            try
            {
                Properties.Settings.Default.CorpusName = this.CorpusNameTextBox.Text;
                Properties.Settings.Default.InputFile = this.ImportFileTextBox.Text;
                Properties.Settings.Default.Save();
                //Helpers.InitTempDb();
                var converter = new TabDelimited();
                converter.CreateCorpus(this.ImportFileTextBox.Text, this.CorpusNameTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ImportFileTextBox_TextChanged(object sender, EventArgs e)
        {
            EnableControls();
        }

        private void CorpusNameTextBox_TextChanged(object sender, EventArgs e)
        {
            EnableControls();
        }
    }
}
