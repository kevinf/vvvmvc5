﻿namespace VVVConverter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.CorpusNameTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ConvertButton = new System.Windows.Forms.Button();
            this.ImportFileBrowseButton = new System.Windows.Forms.Button();
            this.ImportFileTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(0, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(513, 229);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.CorpusNameTextBox);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.ConvertButton);
            this.tabPage1.Controls.Add(this.ImportFileBrowseButton);
            this.tabPage1.Controls.Add(this.ImportFileTextBox);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(505, 203);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Tab-delimited";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // CorpusNameTextBox
            // 
            this.CorpusNameTextBox.Location = new System.Drawing.Point(83, 54);
            this.CorpusNameTextBox.Name = "CorpusNameTextBox";
            this.CorpusNameTextBox.Size = new System.Drawing.Size(334, 20);
            this.CorpusNameTextBox.TabIndex = 5;
            this.CorpusNameTextBox.TextChanged += new System.EventHandler(this.CorpusNameTextBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Corpus name:";
            // 
            // ConvertButton
            // 
            this.ConvertButton.Location = new System.Drawing.Point(6, 163);
            this.ConvertButton.Name = "ConvertButton";
            this.ConvertButton.Size = new System.Drawing.Size(75, 23);
            this.ConvertButton.TabIndex = 3;
            this.ConvertButton.Text = "Convert";
            this.ConvertButton.UseVisualStyleBackColor = true;
            this.ConvertButton.Click += new System.EventHandler(this.ConvertButton_Click);
            // 
            // ImportFileBrowseButton
            // 
            this.ImportFileBrowseButton.Location = new System.Drawing.Point(424, 22);
            this.ImportFileBrowseButton.Name = "ImportFileBrowseButton";
            this.ImportFileBrowseButton.Size = new System.Drawing.Size(75, 23);
            this.ImportFileBrowseButton.TabIndex = 2;
            this.ImportFileBrowseButton.Text = "Browse...";
            this.ImportFileBrowseButton.UseVisualStyleBackColor = true;
            this.ImportFileBrowseButton.Click += new System.EventHandler(this.ImportFileBrowseButton_Click);
            // 
            // ImportFileTextBox
            // 
            this.ImportFileTextBox.Location = new System.Drawing.Point(83, 22);
            this.ImportFileTextBox.Name = "ImportFileTextBox";
            this.ImportFileTextBox.Size = new System.Drawing.Size(334, 20);
            this.ImportFileTextBox.TabIndex = 1;
            this.ImportFileTextBox.TextChanged += new System.EventHandler(this.ImportFileTextBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Import file:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 234);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VVV Converter";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button ConvertButton;
        private System.Windows.Forms.Button ImportFileBrowseButton;
        private System.Windows.Forms.TextBox ImportFileTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CorpusNameTextBox;
        private System.Windows.Forms.Label label2;
    }
}

