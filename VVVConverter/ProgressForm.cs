﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace VVVConverter
{
    public partial class ProgressForm : Form
    {
        public delegate bool DoBgWorkDelg(BackgroundWorker bg);

        public DoBgWorkDelg DoBgWorkImpl;

        public bool Completed;

        public ProgressForm()
        {
            InitializeComponent();

            progressBar1.Step = 1;
            progressBar1.Maximum = 100;
            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bgWorker = sender as BackgroundWorker;

            Completed = DoBgWorkImpl.Invoke(bgWorker);

            if (!Completed)
                e.Cancel = true;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.IsBusy)
            {
                statusLabel.Text = "Cancelling...";
                backgroundWorker1.CancelAsync();
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            if (e.UserState != null)
                statusLabel.Text = e.UserState.ToString();
            else
                statusLabel.Text = e.ProgressPercentage + "%";
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                MessageBox.Show("Cancelled");
            }
            else
            {
                //MessageBox.Show("Completed");
            }
            Close();

        }

        private void ProgressForm_Shown(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();
        }
    }
}
