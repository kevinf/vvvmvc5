﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using EblaImpl;
//using HtmlAgilityPack;
using EblaAPI;
using System.Threading;
using System.ComponentModel;

namespace VVVConverter
{
    class TabDelimited
    {
        string[] _lines;
        int _colCount;
        //List<string> _versionNames = new List<string>();

        List<EblaAPI.DocumentMetadata> _metadata = new List<EblaAPI.DocumentMetadata>();
        List<XmlDocument> _docs = new List<XmlDocument>();
        List<XmlNode> _bodyNodes = new List<XmlNode>();
        SortedDictionary<string, string> _cultures;

        string _filename, _corpName;

        public TabDelimited()
        {
            _cultures = Helpers.GetCorpusStore().GetCultures();

        }

        public void CreateCorpus(string filename, string corpName)
        {
            _filename = filename;
            _corpName = corpName;
            var pf = new ProgressForm();
            pf.Text = "Conversion progress";
            pf.DoBgWorkImpl = this.CreateCorpusWork;

            pf.ShowDialog();

            if (!pf.Completed)
                return;
        }

        bool CreateCorpusWork(BackgroundWorker bg)
        {
            bg.ReportProgress(0, "Creating temporary database...");
            Helpers.InitTempDb();
            if (bg.CancellationPending)
                return false;
//#if DEBUG
//            var workingpath = Path.GetDirectoryName(_filename);
//            workingpath = Path.Combine(workingpath, "eblatemp");

//            if (!Directory.Exists(workingpath))
//                Directory.CreateDirectory(workingpath);
//#endif

            bg.ReportProgress(0, "Reading input file...");
            _lines = File.ReadAllLines(_filename, Encoding.UTF8);
            if (bg.CancellationPending)
                return false;

            
            bg.ReportProgress(0, "Parsing headers...");
            ParseHeaderLines();

            bg.ReportProgress(0, "Creating new corpus...");
            var store = Helpers.GetCorpusStore();

            if (store.GetCorpusList().FirstOrDefault(x => x == _corpName) != null)
                store.DeleteCorpus(_corpName);


            store.CreateCorpus(_corpName, string.Empty, _metadata[0].LanguageCode);

            var corp = Helpers.GetCorpus(_corpName);

            //var docs = new List<XmlDocument>();

            foreach (var md in _metadata)
            {
                var xmlDoc = EblaHelpers.CreateXmlDocument("html");
                XmlNode headNode = xmlDoc.CreateElement("head");
                xmlDoc.DocumentElement.AppendChild(headNode);

                _docs.Add(xmlDoc);

                var attrib = xmlDoc.CreateAttribute("data-ebla-lang");
                attrib.Value = md.LanguageCode;
                headNode.Attributes.Append(attrib);

                XmlNode xmlBodyNode = xmlDoc.CreateElement("body");
                xmlDoc.DocumentElement.AppendChild(xmlBodyNode);

                _bodyNodes.Add(xmlBodyNode);

                attrib = xmlDoc.CreateAttribute("data-ebla-authtran");
                attrib.Value = md.AuthorTranslator;
                headNode.Attributes.Append(attrib);

                attrib = xmlDoc.CreateAttribute("data-ebla-copyright");
                attrib.Value = md.CopyrightInfo;
                headNode.Attributes.Append(attrib);

                attrib = xmlDoc.CreateAttribute("data-ebla-description");
                attrib.Value = md.Description;
                headNode.Attributes.Append(attrib);

                attrib = xmlDoc.CreateAttribute("data-ebla-genre");
                attrib.Value = md.Genre;
                headNode.Attributes.Append(attrib);

                XmlAttribute infoAttrib = xmlDoc.CreateAttribute("data-ebla-info");
                infoAttrib.Value = md.Information;
                headNode.Attributes.Append(infoAttrib);

                if (md.ReferenceDate.HasValue)
                {
                    attrib = xmlDoc.CreateAttribute("data-ebla-date");
                    attrib.Value = md.ReferenceDate.ToString();
                    headNode.Attributes.Append(attrib);
                }

            }

            bg.ReportProgress(0, "Processing file...");
            int segid = 1;
            string previousText = null;
            for (int lineno = (int)HeaderLines.LastHeaderLine; lineno < _lines.Length; lineno++)
            {
                var cells = SplitOnTab(_lines[lineno]);
                if (cells.Count != _colCount)
                    throw new Exception("Inconsistent number of columns at line " + lineno + "; found " + cells.Count + ", expected " + _colCount);


                for (int i = 0; i < _colCount; i++)
                {
                    var xmlDoc = _docs[i];
                    var xmlBodyNode = _bodyNodes[i];

                    string text = cells[i]; // TODO - allow markup or at least conver CRLF to <br/>

                    // did previous text end in whitespace and are we about to add more text?
                    if (!string.IsNullOrEmpty(previousText) && text.Length > 0)
                    {
                        if (!char.IsWhiteSpace(text[0]) && !char.IsWhiteSpace(previousText[previousText.Length - 1]))
                        {
                            var whitespace = xmlDoc.CreateTextNode("XNLX");
                            xmlBodyNode.AppendChild(whitespace);
                        }
                    }

                    if (text.Length > 0)
                        previousText = text;
                    
                    XmlText xmlText = xmlDoc.CreateTextNode(text);
                    xmlBodyNode.AppendChild(xmlText);


                    var attribs = new List<XmlAttribute>();
                    var attrib = xmlDoc.CreateAttribute("data-eblasegid");
                    attrib.Value = segid.ToString();
                    attribs.Add(attrib);
                    attrib = xmlDoc.CreateAttribute("data-eblatype");
                    attrib.Value = "startmarker";
                    attribs.Add(attrib);
                    if (i == 0)
                    {
                        attrib = xmlDoc.CreateAttribute("data-userattrib-label");
                        attrib.Value = segid.ToString();
                        attribs.Add(attrib);
                    }
                    else
                    {
                        attrib = xmlDoc.CreateAttribute("data-userattrib-alignlabel");
                        attrib.Value = segid.ToString();
                        attribs.Add(attrib);
                    }
                    var pos = new XmlDocPos();
                    pos.textContent = xmlText;
                    pos.offset = 0;
                    Document.InsertStartSegElm(pos, xmlDoc, attribs, false);
                    attribs.Clear();
                    attrib = xmlDoc.CreateAttribute("data-eblasegid");
                    attrib.Value = segid.ToString();
                    attribs.Add(attrib);
                    attrib = xmlDoc.CreateAttribute("data-eblatype");
                    attrib.Value = "endmarker";
                    attribs.Add(attrib);
                    pos.offset = xmlText.InnerText.Length;
                    Document.InsertEndSegElm(pos, xmlDoc, attribs, false);


                } // for (each col)

                segid++;

                bg.ReportProgress((int) (segid * 100.0/ ( _lines.Length + _colCount)), "Processing file...");


            } // for (each line)

            HtmlErrors errors;



            for (int i = 0; i < _colCount; i++)
            {
                var xmlDoc = _docs[i];


                bg.ReportProgress((int)((_lines.Length * 100.0 + i) / (_lines.Length + _colCount)), "Storing document " + (i + 1));

                string html = EblaHelpers.XmlDocumentToString(xmlDoc);

                //html = html.Replace("XNLX", " ");
                html = html.Replace("XNLX", "&nbsp;");

                int ihtml = html.IndexOf("<html");
                html = html.Substring(ihtml);

                if (i == 0)
                {
                    var doc = Helpers.GetBaseText(_corpName);
                    // base text
                    errors = corp.UploadBaseText(html);
//#if DEBUG
//                    string workingfile = Path.Combine(workingpath, "basetext.html");
//                    File.Delete(workingfile);
//                    File.AppendAllText(workingfile, html);
//#endif
                }
                else
                {
                    corp.CreateVersion(_metadata[i].NameIfVersion, _metadata[i]);

                    var doc = Helpers.GetVersion(_corpName, _metadata[i].NameIfVersion);
                    Console.WriteLine("Created version " + _metadata[i].NameIfVersion + " ...");
                    errors = corp.UploadVersion(_metadata[i].NameIfVersion, html);
//#if DEBUG
//                    string workingfile = Path.Combine(workingpath, "version" + i + ".html");
//                    File.Delete(workingfile);
//                    File.AppendAllText(workingfile, html);
//#endif
                }


            }

            var baseText = Helpers.GetBaseText(_corpName);

            string path = System.IO.Path.GetDirectoryName(_filename);
            string fname = _corpName + "-export";
            fname = PrismLib.PrismExport.SanitiseFilename(fname);
            fname += ".xml";
            fname = System.IO.Path.Combine(path, fname);

            var settings = new XmlWriterSettings();
            settings.Indent = true;
            
            var writer = XmlWriter.Create(fname, settings);

            //bg.ReportProgress((int)((_lines.Length * 1.0 + i) / (_lines.Length + _colCount)), "Storing document " + (i + 1));
            bg.ReportProgress(100, "Exporting...");


            PrismLib.PrismExport.ExportCorpus(writer, corp, baseText, corp.GetVersionList(), _corpName, VariationMetricTypes.metricA, Helpers.GetVersion, Helpers.GetAlignmentSet);

            return true;
        }

        enum HeaderLines
        {
            VersionName = 1,
            Description,
            LanguageCode,
            Info,
            AuthTrans,
            Genre,
            Copyright,
            RefDate,
            LastHeaderLine = RefDate
        }


        List<string> SplitOnTab(string s)
        {
            var l = new List<string>();
            var temp = s.Split(new char[] { '\t' }, StringSplitOptions.None); // we need empty entries
            foreach (var t in temp)
                l.Add(t.Trim());
            return l;
        }

        void ParseHeaderLines()
        {
            if (_lines.Length <= (int) HeaderLines.LastHeaderLine)
                throw new Exception("Input file should have " + ((int)HeaderLines.LastHeaderLine) + " header lines plus at least one content line");

            var cells = SplitOnTab(_lines[0]);
            if (cells.Count < 2)
                throw new Exception("Input file should have at least 2 columns in every line");

            if (!string.IsNullOrEmpty(cells[0]))
                throw new Exception("First column of first row should be left empty - remaining columns must contain version names");

            _colCount = cells.Count;
            _metadata = new List<EblaAPI.DocumentMetadata>();
            foreach (var c in cells)
                _metadata.Add(new DocumentMetadata());
            var headerLineTypes = Enum.GetValues(typeof(HeaderLines)).Cast<HeaderLines>();

            foreach (var headerLineType in headerLineTypes)
            {
                cells = SplitOnTab(_lines[(int) headerLineType - 1]);
                if (cells.Count != _colCount)
                    throw new Exception(cells.Count + " columns found at line " + ((int)headerLineType) + "; expected " + _colCount);
                switch (headerLineType)
                {
                    case HeaderLines.VersionName:
                        GetVersionNames(cells);
                        break;
                    case HeaderLines.AuthTrans:
                        GetAuthTrans(cells);
                        break;
                    case HeaderLines.Copyright:
                        GetCopyright(cells);
                        break;
                    case HeaderLines.Description:
                        GetDescription(cells);
                        break;
                    case HeaderLines.Genre:
                        GetGenre(cells);
                        break;
                    case HeaderLines.Info:
                        GetInfo(cells);
                        break;
                    case HeaderLines.LanguageCode:
                        GetLangCode(cells);
                        break;
                    case HeaderLines.RefDate:
                        GetRefDate(cells);
                        break;
                }

            }
        }

        void GetVersionNames(List<string> cells)
        {
            for (int i = 1; i < _colCount; i++)
            {
                if (string.IsNullOrWhiteSpace(cells[i]))
                    throw new Exception("Empty version name at column " + i);
                _metadata[i].NameIfVersion = cells[i];
            }

        }

        void GetRefDate(List<string> cells)
        {
            for (int i = 0; i < _colCount; i++)
            {
                if (!string.IsNullOrEmpty(cells[i]))
                {
                    int d;
                    if (int.TryParse(cells[i], out d))
                        _metadata[i].ReferenceDate = d;
                    else
                    {
                        if (!string.IsNullOrEmpty(_metadata[i].Information))
                            _metadata[i].Information += Environment.NewLine;
                        _metadata[i].Information += "Publication date: " + cells[i];
                    }
                }
                
            }
        }

        void GetLangCode(List<string> cells)
        {
            for (int i = 0; i < _colCount; i++)
            {
                if (!_cultures.Values.Contains(cells[i]))
                    throw new Exception("Invalid language code in column " + i + ": " + cells[i]);
                _metadata[i].LanguageCode = cells[i];
            }
        }

        void GetInfo(List<string> cells)
        {
            for (int i = 0; i < _colCount; i++)
            {
                _metadata[i].Information = cells[i];
            }
        }

        void GetGenre(List<string> cells)
        {
            for (int i = 0; i < _colCount; i++)
            {
                _metadata[i].Genre = cells[i];
            }
        }

        void GetAuthTrans(List<string> cells)
        {
            for (int i = 0; i < _colCount; i++)
            {
                _metadata[i].AuthorTranslator = cells[i];
            }
        }

        void GetCopyright(List<string> cells)
        {
            for (int i = 0; i < _colCount; i++)
            {
                _metadata[i].CopyrightInfo = cells[i];
            }
        }

        void GetDescription(List<string> cells)
        {
            for (int i = 0; i < _colCount; i++)
            {
                _metadata[i].Description = cells[i];
            }
        }

    }
}
