﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VVVConverter
{
    class EblaSupport
    {
        static object _cxn;
        static object _rqdata;

        internal static object GetConnection()
        {
            return _cxn;
        }

        internal static void SetConnection(object conn)
        {
            _cxn = conn;
        }

        internal static object GetRequestData()
        {
            return _rqdata;
        }

        internal static void SetRequestData(object data)
        {
            _rqdata = data;
        }

    }
}
