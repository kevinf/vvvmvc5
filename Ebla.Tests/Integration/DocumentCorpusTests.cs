﻿/*
Version Variation Visualisation (VVV)
http://www.delightedbeauty.org
    
Copyright (c) Kevin Flanagan, 2012.
http://www.kftrans.co.uk
  
This file is part of VVV.

VVV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VVV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VVV.  If not, see <http://www.gnu.org/licenses/>.
*/
using System.Configuration;
using System.Linq;
using EblaImpl;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ebla.Tests.Integration
{
    /// <summary>
    /// Integration tests between Corpus and its documents.
    /// </summary>
    [TestClass]
    public class DocumentCorpusTests
    {
        private const string BaseDocument = @"<p>""Yes, M'm.""</p><p>""And has the fruit come?""</p><p>""Yes, M'm. Everything's come.""</p><p>""Bring the fruit up to the dining-room, will you? I'll arrange it before I go upstairs.""</p><p>It was dusky in the dining-room and quite chilly. But all the same Bertha threw off her coat; she could not bear the tight clasp of it another moment, and the cold air fell on her arms.</p>";
        private const string Version1Document = @"<p>-- Voltou, sim senhora.</p><p>-- E as frutas vieram?</p><p>-- Vieram, sim senhora. Veio tudo.</p><p>-- Traze as frutas para cá, sim? Quero arranjá-las antes de subir.</p><p>Fazia lusco-fusco na sala de jantar e estava bastante fresco. Mas mesmo assim Berta tirou o casaco; não podia suportar por mais tempo sua pressão; o ar frio caiu-lhe sobre os braços.</p>";
        private const string HtmlContent = "<html><head></head><body>{0}</body></html>";

        [TestInitialize()]
        public void DocumentCorpusInitialize()
        {
            Helpers.CreateTestCorpus();
            var doc = EblaFactory.GetDocument(ConfigurationManager.AppSettings);
            doc.OpenBaseText(Helpers.GetTestCorpusName2(), Helpers.GetTestUsername(), Helpers.GetTestPassword());
            doc.DeleteAllSegmentDefinitions();
        }

        [TestMethod]
        public void ShouldGetDocument()
        {
            var doc = EblaFactory.GetDocument(ConfigurationManager.AppSettings);
            Assert.IsNotNull(doc);
        }

        [TestMethod]
        public void ShouldNotOpenBaseText()
        {
            var doc = EblaFactory.GetDocument(ConfigurationManager.AppSettings);
            Assert.IsFalse(doc.OpenBaseText(Helpers.GetTestCorpusName2(), "wrong", "wrong"));
        }

        [TestMethod]
        public void ShouldOpenBaseText()
        {
            var doc = EblaFactory.GetDocument(ConfigurationManager.AppSettings);
            Assert.IsTrue(doc.OpenBaseText(Helpers.GetTestCorpusName2(), Helpers.GetTestUsername(), Helpers.GetTestPassword()));
        }

        [TestMethod]
        public void ShouldReturnCorpus()
        {
            var corpus = EblaFactory.GetCorpus(ConfigurationManager.AppSettings);
            Assert.IsNotNull(corpus);
        }

        [TestMethod]
        public void ShouldNotOpenCorpus()
        {
            var corpus = EblaFactory.GetCorpus(ConfigurationManager.AppSettings);
            Assert.IsFalse(corpus.Open(Helpers.GetTestCorpusName2(), "wrong", "wrong"));
        }

        [TestMethod]
        public void ShouldOpenCorpus()
        {
            var corpus = EblaFactory.GetCorpus(ConfigurationManager.AppSettings);
            Assert.IsTrue(corpus.Open(Helpers.GetTestCorpusName2(), Helpers.GetTestUsername(), Helpers.GetTestPassword()));
        }

        [TestMethod]
        public void ShouldGetVersionText()
        {
            var doc = EblaFactory.GetDocument(ConfigurationManager.AppSettings);

            Assert.IsTrue(doc.OpenVersion(Helpers.GetTestCorpusName2(), Helpers.GetVersionTextName(), Helpers.GetTestUsername(), Helpers.GetTestPassword()), "You do not have permission to access the document.");
        }

        [TestMethod]
        public void ShouldUpdateBaseText()
        {
            var corpus = EblaFactory.GetCorpus(ConfigurationManager.AppSettings);
            corpus.Open(Helpers.GetTestCorpusName2(), Helpers.GetTestUsername(), Helpers.GetTestPassword());
            var errors = corpus.UpdateBaseText(string.Format(HtmlContent,BaseDocument));
            Assert.IsFalse(errors.HtmlParseErrors.Any());
        }

        [TestMethod]
        public void ShouldUploadVersion()
        {
            var documentName = "Portuguese version";
            var corpus = EblaFactory.GetCorpus(ConfigurationManager.AppSettings);
            corpus.Open(Helpers.GetTestCorpusName2(), Helpers.GetTestUsername(), Helpers.GetTestPassword());
            var errors = corpus.UploadVersion(documentName, string.Format(HtmlContent, BaseDocument));
            Assert.IsFalse(errors.HtmlParseErrors.Any());
        }

        [TestMethod]
        public void ShouldReturnAutoSegmentationAroundTag()
        {
            var doc = EblaFactory.GetDocument(ConfigurationManager.AppSettings);
            doc.OpenBaseText(Helpers.GetTestCorpusName2(), Helpers.GetTestUsername(), Helpers.GetTestPassword());
            doc.AutoSegmentationAroundTag("p");
        }

        [TestMethod]
        public void ShouldReturnAutoSegmentationAfterTag()
        {
            
        }

        [TestMethod]
        public void ShouldReturnNextTextNode()
        {
        }

       
    }
}
