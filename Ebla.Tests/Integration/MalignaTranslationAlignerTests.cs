﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.

    This class was based on the following articles and 2006-2015 Jarek Lipski - 
    Maligna (v3.0.1-SNAPSHOT) source code (https://github.com/loomchild/maligna):

    [1] Peter F. Brown, Jennifer C. Lai, Robert L. Mercer (1991), 
    Aligning sentences in parallel corpora, Proceedings of the 29th annual meeting on Association for Computational Linguistics.
    [2] Jassem, K., & Lipski, J. (2008). A new tool for the bilingual text aligning at the sentence level. 
    Intelligent Information Systems, 279-286.
    [3] Moore, R. C. (2002, October). Fast and accurate sentence alignment of bilingual corpora. 
    In Conference of the Association for Machine Translation in the Americas (pp. 135-144). Springer Berlin Heidelberg.
 */
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EblaImpl;
using System.Configuration;
using System.Linq;
using EblaImpl.Aligner;
using EblaImpl.Calculator;
using EblaImpl.Translation;

namespace Ebla.Tests.Integration
{
    [TestClass]
    public class MalignaTranslationAlignerTests
    {
        private const string TotalTokensAttributeName = ":totaltokens";
        private const string TokensAttributeName = ":tokens";
        private const string WidsAttributeName = ":wids";

        public TestContext TestContext { get; set; }

        [TestMethod]
        public void ShouldAlignSegmentsWithGaleAndChurchMacro()
        {

            var alignmentList = new java.util.ArrayList();
            var alignment = new net.loomchild.maligna.coretypes.Alignment();
            alignment.addSourceSegment("Three Witches, the Weïrd Sisters");
            alignment.addSourceSegment("DUNCAN, king of Scotland");
            alignment.addSourceSegment("MALCOLM, his elder son");
            alignment.addSourceSegment("DONALBAIN, Duncan’s younger son");
            alignment.addSourceSegment("MACBETH, thane of Glamis");
            alignment.addSourceSegment("LADY MACBETH");
            alignment.addSourceSegment("SEYTON, attendant to Macbeth");
            alignment.addSourceSegment("Three Murderers in Macbeth’s service");
            alignment.addSourceSegment("Both attending upon Lady Macbeth:");
            alignment.addSourceSegment("A Doctor");
            alignment.addSourceSegment("A Gentlewoman");
            alignment.addSourceSegment("A Porter");

            alignment.addTargetSegment("ДУНКАН, король Шотландии");
            alignment.addTargetSegment("МАЛЬКОЛЬ, старший сын Дункана");
            alignment.addTargetSegment("ДОНАЛЬБАЙН, младший сын Дункана");
            alignment.addTargetSegment("МАКБЕТ, военачальник королевской армии");
            alignment.addTargetSegment("БАНКО, военачальник королевской армии");
            alignment.addTargetSegment("МАКДУФ, военачальник королевской армии");
            alignment.addTargetSegment("ЛЕНОКС, военачальник королевской армии");
            alignment.addTargetSegment("РОСС, шотландский вельможа");
            alignment.addTargetSegment("МЕНТИТ, шотландский вельможа");
            alignment.addTargetSegment("АНГУС, шотландский вельможа");
            alignment.addTargetSegment("КЕТНЕС, шотландский вельможа");
            alignment.addTargetSegment("ФЛИНС, сын Банко");

            alignmentList.add(alignment);

            var macro = new net.loomchild.maligna.filter.macro.GaleAndChurchMacro();
            java.util.List result = macro.apply(alignmentList);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ShouldAlignFileSegments()
        {
            var alignmentList = new java.util.ArrayList();
            var malignaAlignment = new net.loomchild.maligna.coretypes.Alignment();

            var fileSource = new System.IO.StreamReader(Helpers.GetTestDataDir(TestContext) + "\\boris-en.html");
            string line;
            while ((line = fileSource.ReadLine()) != null)
            {
                malignaAlignment.addSourceSegment(line);
            }
            fileSource.Close();

            var fileTarget = new System.IO.StreamReader(Helpers.GetTestDataDir(TestContext) + "\\boris-ru.html");
            while ((line = fileTarget.ReadLine()) != null)
            {
                malignaAlignment.addTargetSegment(line);
            }
            fileTarget.Close();

            alignmentList.add(malignaAlignment);

            var macro = new net.loomchild.maligna.filter.macro.MooreMacro();
            java.util.List result = macro.apply(alignmentList);
            System.Console.Write(result.size());
            for (int i = 0; i < result.size(); i++)
            {
                System.Console.Write("--------------------------");
                net.loomchild.maligna.coretypes.Alignment aa = (net.loomchild.maligna.coretypes.Alignment)result.get(i);
                System.Console.Write(aa.getSourceSegmentList().size());
                System.Console.Write(" - ");
                if (aa.getSourceSegmentList().size() > 0) System.Console.Write(aa.getSourceSegmentList().get(0));
                System.Console.Write(" - ");
                System.Console.Write(aa.getTargetSegmentList().size());
                System.Console.Write(" - ");
                if (aa.getTargetSegmentList().size() > 0) System.Console.Write(aa.getTargetSegmentList().get(0));
            }
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ShouldAlignFileSegmentsWithGaleAndChurchMacro()
        {
            var alignmentList = new java.util.ArrayList();
            var malignaAlignment = new net.loomchild.maligna.coretypes.Alignment();

            var fileSource = new System.IO.StreamReader(Helpers.GetTestDataDir(TestContext) + "\\boris-en.html");
            string line;
            while ((line = fileSource.ReadLine()) != null)
            {
                malignaAlignment.addSourceSegment(line);
            }
            fileSource.Close();

            var fileTarget = new System.IO.StreamReader(Helpers.GetTestDataDir(TestContext) + "\\boris-ru.html");
            while ((line = fileTarget.ReadLine()) != null)
            {
                malignaAlignment.addTargetSegment(line);
            }
            fileTarget.Close();

            alignmentList.add(malignaAlignment);

            var macro = new net.loomchild.maligna.filter.macro.GaleAndChurchMacro();
            java.util.List result = macro.apply(alignmentList);
            System.Console.Write(result.size());
            for (int i = 0; i < result.size(); i++)
            {
                System.Console.Write("--------------------------");
                net.loomchild.maligna.coretypes.Alignment aa = (net.loomchild.maligna.coretypes.Alignment)result.get(i);
                System.Console.Write(aa.getSourceSegmentList().size());
                System.Console.Write(" - ");
                if (aa.getSourceSegmentList().size() > 0) System.Console.Write(aa.getSourceSegmentList().get(0));
                System.Console.Write(" - ");
                System.Console.Write(aa.getTargetSegmentList().size());
                System.Console.Write(" - ");
                if (aa.getTargetSegmentList().size() > 0) System.Console.Write(aa.getTargetSegmentList().get(0));
            }
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ShouldApplyAutoAlignmentAndCreateEblaAlignment()
        {
            var alignment = new MalignaTranslationAligner(ConfigurationManager.AppSettings);
            var alignments = alignment.ApplyAutoAlignmentAndCreateEblaAlignment(Helpers.GetTestCorpusName2(), Helpers.GetVersionTextName(),
                Helpers.GetTestUsername(), Helpers.GetTestPassword());
            Assert.IsNotNull(alignments);
        }

        [TestMethod]
        public void ShouldApplyAutoAlignmentAndCreateEblaAlignmentWithGaleAndChurchMacro()
        {
            var alignment = new MalignaTranslationAligner(ConfigurationManager.AppSettings);
            var alignments = alignment.ApplyAutoAlignmentAndCreateEblaAlignmentWithGaleAndChurchMacro(Helpers.GetTestCorpusName2(), Helpers.GetVersionTextName(),
                Helpers.GetTestUsername(), Helpers.GetTestPassword());
            Assert.IsNotNull(alignments);
        }


        [TestMethod]
        public void ShouldGetSegmentTokens()
        {
            var segment = new SegmentBasedAlignment(ConfigurationManager.AppSettings);
            var result = segment.GetSegmentTokens(34, TokensAttributeName);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ShouldGetSegmentTokensWidsAttributeName()
        {
            var segment = new SegmentBasedAlignment(ConfigurationManager.AppSettings);
            var result = segment.GetSegmentTokens(214, WidsAttributeName);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ShouldCalculateTokensProbabilities()
        {
            var sentenceAlignment = new SegmentBasedAlignment(ConfigurationManager.AppSettings);
            var segmentDefinitions = sentenceAlignment.GetSegmentTokens(1619, TotalTokensAttributeName);

            var segmentCalculator = new SegmentCalculator();
            var result = segmentCalculator.CalculateProbabilities(segmentDefinitions, null);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ShouldCreateAlignmentVocabulary()
        {
            var sentenceAlignment = new SegmentBasedAlignment(ConfigurationManager.AppSettings);
            var segmentDefinitions = sentenceAlignment.GetSegmentTokens(1619, TokensAttributeName);

            var translationUtil = new TranslationUtil();
            var result = translationUtil.CreateAlignmentVocabulary(segmentDefinitions.ToArray());
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ShouldCreateAlignmentVocabularyWithoutRareWords()
        {
            var sentenceAlignment = new SegmentBasedAlignment(ConfigurationManager.AppSettings);
            var segmentDefinitions = sentenceAlignment.GetSegmentTokens(1619, TokensAttributeName);

            var translationUtil = new TranslationUtil();
            var result = translationUtil.CreateAlignmentVocabularyWithoutRareWords(segmentDefinitions.ToArray(), 2);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ShouldCalculateWordTranslationProbabilities()
        {
            var sentenceAlignment = new SegmentBasedAlignment(ConfigurationManager.AppSettings);
            var segmentDefinitions = sentenceAlignment.GetSegmentTokens(1619, TokensAttributeName);
            var wids = sentenceAlignment.GetSegmentTokens(1619, WidsAttributeName);

            var translationUtil = new TranslationUtil();
            var alignmentVocabulary = translationUtil.CreateAlignmentVocabulary(segmentDefinitions.ToArray());

            var wordCalculator = new WordCalculator();

            //var result = wordCalculator.CalculateProbabilities(wids, alignmentVocabulary);
            //TODO Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ShouldCalculateWordTranslationProbabilitiesWithoutRareWords()
        {
            var sentenceAlignment = new SegmentBasedAlignment(ConfigurationManager.AppSettings);
            var segmentDefinitions = sentenceAlignment.GetSegmentTokens(1619, TokensAttributeName);
            var wids = sentenceAlignment.GetSegmentTokens(1619, WidsAttributeName);

            var translationUtil = new TranslationUtil();
            var alignmentVocabulary = translationUtil.CreateAlignmentVocabularyWithoutRareWords(segmentDefinitions.ToArray(), 2);

            var widsWithoutRareWords = translationUtil.CreateSegmentWidListWithoutRareWords(wids, alignmentVocabulary);

            var wordCalculator = new WordCalculator();
            var result = wordCalculator.CalculateProbabilities(widsWithoutRareWords, alignmentVocabulary);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ShouldCreateSegmentWidList()
        {
            var sentenceAlignment = new SegmentBasedAlignment(ConfigurationManager.AppSettings);
            var segmentDefinitions = sentenceAlignment.GetSegmentTokens(1619, TokensAttributeName);
            var segmentWids = sentenceAlignment.GetSegmentTokens(1619, WidsAttributeName);

            var translationUtil = new TranslationUtil();
            var alignmentVocabulary = translationUtil.CreateAlignmentVocabulary(segmentDefinitions.ToArray());

            var result = translationUtil.CreateSegmentWidList(segmentWids, alignmentVocabulary, true);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ShouldCreateSegmentWidListWithoutRareWords()
        {
            var sentenceAlignment = new SegmentBasedAlignment(ConfigurationManager.AppSettings);
            var segmentDefinitions = sentenceAlignment.GetSegmentTokens(1619, TokensAttributeName);
            var segmentWids = sentenceAlignment.GetSegmentTokens(1619, WidsAttributeName);

            var translationUtil = new TranslationUtil();
            var alignmentVocabulary = translationUtil.CreateAlignmentVocabularyWithoutRareWords(segmentDefinitions.ToArray(), 2);

            var result = translationUtil.CreateSegmentWidListWithoutRareWords(segmentWids, alignmentVocabulary, true);

            Assert.IsNotNull(result);
        }
    }
}
