﻿/*
Version Variation Visualisation (VVV)
http://www.delightedbeauty.org
    
Copyright (c) Kevin Flanagan, 2012.
http://www.kftrans.co.uk
  
This file is part of VVV.

VVV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VVV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VVV.  If not, see <http://www.gnu.org/licenses/>.
*/
using EblaAPI;
using EblaImpl;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Ebla.Tests.Integration
{
    [TestClass]
    public class AlignmentSetTests
    {
        [TestInitialize]
        public void AlignmentSetInitialize()
        {
            
        }
             
        [TestMethod]
        public void ShouldGetAlignmentSet()
        {
            var alignmentSet = EblaFactory.GetAlignmentSet(ConfigurationManager.AppSettings);

            Assert.IsNotNull(alignmentSet, "You do not have permission to access the alignment set.");
        }

        [TestMethod]
        public void ShouldGetAlignmentSetOpen()
        {
            var alignmentSet = EblaFactory.GetAlignmentSet(ConfigurationManager.AppSettings);

            Assert.IsTrue(alignmentSet.Open(Helpers.GetTestCorpusName2(), Helpers.GetVersionTextName(), Helpers.GetTestUsername(), Helpers.GetTestPassword()), "You do not have permission to access the alignment set.");
        }

        [TestMethod]
        public void ShouldGetAlignmentSetOpenGetBaseTextAndVersion()
        {
            var alignmentSet = EblaFactory.GetAlignmentSet(ConfigurationManager.AppSettings);
            alignmentSet.Open(Helpers.GetTestCorpusName2(), Helpers.GetVersionTextName(), Helpers.GetTestUsername(), Helpers.GetTestPassword());

            var basetextDoc = Helpers.GetBaseText(Helpers.GetTestCorpusName2());
            var versionDoc = Helpers.GetVersionText(Helpers.GetTestCorpusName2(), Helpers.GetVersionTextName());

            Assert.IsTrue(alignmentSet != null && basetextDoc != null && versionDoc != null, "Problem retrieving alignment and documents.");
        }


        [TestMethod]
        public void ShouldFindSegmentDefinitionsFromBaseText()
        {
            var alignmentSet = EblaFactory.GetAlignmentSet(ConfigurationManager.AppSettings);
            alignmentSet.Open(Helpers.GetTestCorpusName2(), Helpers.GetVersionTextName(), Helpers.GetTestUsername(), Helpers.GetTestPassword());

            var document = (Document)Helpers.GetBaseText(Helpers.GetTestCorpusName2());
            var cbFilter = new List<SegmentContentBehaviour>() { SegmentContentBehaviour.normal };

            var segs = document.FindSegmentDefinitions(0, document.Length(), false, false, null, cbFilter.ToArray());

            Assert.IsTrue(segs.Any(), "Problem retrieving segments.");
        }

        [TestMethod]
        public void ShouldFindSegmentDefinitionsFromVersion()
        {
            var alignmentSet = EblaFactory.GetAlignmentSet(ConfigurationManager.AppSettings);
            alignmentSet.Open(Helpers.GetTestCorpusName2(), Helpers.GetVersionTextName(), Helpers.GetTestUsername(), Helpers.GetTestPassword());

            var document = (Document)Helpers.GetVersionText(Helpers.GetTestCorpusName2(), Helpers.GetVersionTextName());
            var cbFilter = new List<SegmentContentBehaviour>() { SegmentContentBehaviour.normal };

            var segs = document.FindSegmentDefinitions(0, document.Length(), false, false, null, cbFilter.ToArray());

            Assert.IsTrue(segs.Any(), "Problem retrieving segments.");
        }
    }
}
