﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using EblaImpl.Extractor;
using System.IO;
using EblaAPI;
using TikaOnDotNet.TextExtraction;
using org.apache.tika.metadata;

namespace Ebla.Tests.Unit
{
    [TestClass]
    public class TikaExtractorTests
    {
        private TextExtractor _cut;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public virtual void SetUp()
        {
            _cut = new TextExtractor();
        }

        [TestMethod]
        public void ShouldExtractFromDoc()
        {
            var path = Helpers.GetTestDataDir(TestContext) + "\\example.doc";
            var textExtractionResult = _cut.Extract(path);

            Assert.IsNotNull(textExtractionResult.Text, "The extraction was not successful.");
        }

        [TestMethod]
        public void ShouldConvertEmbeddedDocToHtml()
        {
            var path = Helpers.GetTestDataDir(TestContext) + "\\example.doc";
            byte[] bytes = File.ReadAllBytes(path);

            var tikaExtractor = new TikaExtractor();
            var result = tikaExtractor.ConvertFileToHtmlWithEmbeddedImages(bytes);

            File.WriteAllBytes(Helpers.GetTestDataDir(TestContext) + "\\result.html", result);

            Assert.IsNotNull(result, "The conversion was not successful.");
        }

        [TestMethod]
        public void ShouldConvertDocToHtml()
        {
            var path = Helpers.GetTestDataDir(TestContext) + "\\example.doc";
            var bytes = File.ReadAllBytes(path);

            var tikaExtractor = new TikaExtractor();
            var result = tikaExtractor.ConvertAnyFileToHtml(bytes);

            File.WriteAllBytes(Helpers.GetTestDataDir(TestContext) + "\\result.html", result);

            Assert.IsNotNull(result, "The conversion was not successful.");
        }

        [TestMethod]
        public void ShouldConvertAnyFileToHtmlPdf()
        {
            var path = Helpers.GetTestDataDir(TestContext) + "\\example.pdf";
            var bytes = File.ReadAllBytes(path);

            var tikaExtractor = new TikaExtractor();
            var result = tikaExtractor.ConvertAnyFileToHtml(bytes);

            File.WriteAllBytes(Helpers.GetTestDataDir(TestContext) + "\\result.html", result);

            Assert.IsNotNull(result, "The conversion was not successful.");
        }

        [TestMethod]
        public void ShouldConvertFileToHtmlPdf()
        {
            var path = Helpers.GetTestDataDir(TestContext) + "\\example.pdf";
            var bytes = File.ReadAllBytes(path);

            var tikaExtractor = new TikaExtractor();
            var metadata = new DocumentMetadata();
            var result = tikaExtractor.ConvertFileToHtml(bytes, null, out metadata);

            File.WriteAllBytes(Helpers.GetTestDataDir(TestContext) + "\\result.html", System.Text.Encoding.UTF8.GetBytes(result));

            Assert.IsNotNull(result, "The conversion was not successful.");
        }

        [TestMethod]
        public void ShouldConvertFileToHtmlDoc()
        {
            var path = Helpers.GetTestDataDir(TestContext) + "\\example.doc";
            var bytes = File.ReadAllBytes(path);

            var tikaExtractor = new TikaExtractor();
            var metadata = new DocumentMetadata();
            var result = tikaExtractor.ConvertFileToHtml(bytes, null, out metadata);

            File.WriteAllBytes(Helpers.GetTestDataDir(TestContext) + "\\result.html", System.Text.Encoding.UTF8.GetBytes(result));

            Assert.IsNotNull(result, "The conversion was not successful.");
        }

        [TestMethod]
        public void ShouldConvertFileToHtmlDoc2()
        {
            var path = Helpers.GetTestDataDir(TestContext) + "\\example.doc";
            var bytes = File.ReadAllBytes(path);

            var tikaExtractor = new TikaExtractor();
            var metadata = new DocumentMetadata();
            var result = tikaExtractor.ConvertFileToHtml(bytes, null);

            File.WriteAllBytes(Helpers.GetTestDataDir(TestContext) + "\\result.html", System.Text.Encoding.UTF8.GetBytes(result.Content));

            Assert.IsNotNull(result, "The conversion was not successful.");
        }

        [TestMethod]
        public void ShouldConvertFileToHtmlDocMetadata()
        {
            var path = Helpers.GetTestDataDir(TestContext) + "\\example.doc";
            var bytes = File.ReadAllBytes(path);

            var tikaExtractor = new TikaExtractor();
            var metadata = new DocumentMetadata();
            var result = tikaExtractor.ConvertFileToHtml(bytes, null, out metadata);

            File.WriteAllBytes(Helpers.GetTestDataDir(TestContext) + "\\result.html", System.Text.Encoding.UTF8.GetBytes(result));

            Assert.IsNotNull(result, "The conversion was not successful.");
        }

        [TestMethod]
        public void ShouldConvertAnyFileToHtml()
        {
            var path = Helpers.GetTestDataDir(TestContext) + "\\example.html";
            var bytes = File.ReadAllBytes(path);

            var tikaExtractor = new TikaExtractor();
            var result = tikaExtractor.ConvertAnyFileToHtml(bytes);

            File.WriteAllBytes(Helpers.GetTestDataDir(TestContext) + "\\result.html", result);

            Assert.IsNotNull(result, "The conversion was not successful.");
        }



    }
}
